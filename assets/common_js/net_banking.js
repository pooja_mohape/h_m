$(document).ready(function() {
     
     var tconfig = {
       "processing": true,
       "serverSide": true,
       "ajax": {
           "url": base_url + "back/netbanking/get_netbanking_data",
           "type": "POST",
           "data": "json"
       },
       "iDisplayLength": 10,
       "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
       //        "paginate": true,
       "paging": true,
       "searching": true,
       "order": [[1, "desc"]],
       "aoColumnDefs": [
                   {
                       "targets": [1],
                       "visible": false,
                       "searchable": false             

                   },
                   // {
                   //  "bSortable":false,
                   //  "aTargets":[4]
                   // }
               ],

                      "fnRowCallback": function(nRow, aData, iDisplayIndex) {
          var info = $(this).DataTable().page.info();  
          $("td:first", nRow).html(info.start + iDisplayIndex + 1);      
          return nRow;
      },
   };    
   var oTable = $('#mail_display_table').dataTable(tconfig);
       
   $(document).off('click', '.net_banking_btn').on('click', '.net_banking_btn',function () {
       $('#net_banking_modal').modal('show');
   });
 
});
