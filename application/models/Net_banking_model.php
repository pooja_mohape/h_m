<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Net_banking_model extends MY_Model {

    protected $table = 'net-banking-details';
    var $fields = array("id","society_id","acc_name","acc_no","bank_name","ifsc_code","branch","created_at");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
    }

    public function editNetbanking($id){
    	$this->db->select('*');
    	$this->db->from('net-banking-details');
    	$this->db->where('id',$id);
    	$res = $this->db->get()->result();
    	return $res;
    }

    public function updateNetbanking($id,$ndata){
    	$this->db->where('id',$id);
    	$this->db->update('net-banking-details',$ndata);
    	return true;
    }

    public function delete_netbanking($id){
    	$this->db->where('id',$id);
    	$this->db->delete('net-banking-details');
    	return true;
    }
    
}
