<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wallet_transaction_model extends MY_Model {

    protected $table = 'wallet_transaction';
    var $fields = array("id","w_id","user_id","outstanding_amount_before","amount","outstanding_amount_after","bill_payment_id","created_by", "created_at");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
    }

}
