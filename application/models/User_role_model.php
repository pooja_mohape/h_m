<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class User_role_model extends MY_Model {
    
    var $table  = 'user_roles';
    var $fields = array("id","role_name", "status", "is_deleted", "created_date");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

    public function getRolename($id)
    {

    	$this->db->select('role_name');
    	$this->db->from('user_roles');
    	$this->db->where('id',$id);
    	$res = $this->db->get();
    	
    	return $res->result_array();
    }
    public function searchByRoleId($roleid)
    {
      return  $this->db->get_where('users',array('role_id'=>$roleid,'society_id'=>$this->session->userdata('society_id')))->result();
    }

    public function userReportByIds($filter_by,$filter_list)
    {
        // return $this->db->get_where('users',array('role_id'=>$id1,'id'=>$id2))->result();
        $role_id = $this->session->userdata('role_id');
      $ses_scid = $this->session->userdata('society_id');
      $this->db->select('u.*,ur.role_name,ur.id,sm.name as society_name,h.wing,h.block');
      $this->db->from('users u');
      $this->db->join('user_roles ur','u.role_id=ur.id','left');
      $this->db->join('society_master sm','u.society_id=sm.id','left');
      $this->db->join('house_master h','u.house_id=h.id','left'); 

       if($filter_by == 'All') {
         
      } else {

        $this->db->where('ur.id',$filter_by);
        $this->db->where('u.id',$filter_list);
        
      }
      if($role_id==SUPERADMIN) {

      } else {
        $this->db->where('society_id',$ses_scid);
      }
        $this->db->where('u.is_deleted','N');
           $res = $this->db->get()->result();
          return $res;
      }
      public function getRoleUsers()
      {
        //return $this->db->get('user_roles')->result();
        if($this->session->userdata('role_id')==SUPERADMIN)
        {
            return $this->db->get('user_roles')->result();
        }
        if($this->session->userdata('role_id')==SOCIETY_SUPERUSER)
        {
          
            $this->db->where('id != ', 2);
            $this->db->where('id != ', 1);
            $query = $this->db->get('user_roles')->result(); 
            return $query;
        }
        if($this->session->userdata('role_id')==SOCIETY_ADMIN)
        {
            $this->db->where('id', 4);
            $query = $this->db->get('user_roles')->result(); 
            return $query;
        }
      }
    
    
}
