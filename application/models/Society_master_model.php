<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Society_master_model extends MY_Model {

    protected $table = 'society_master';
    var $fields = array("id", "code","name","no_of_house","address","state","city","pincode","image","created_by","created_date","updated_by","updated_date");

    public function selectState() {
    		$this->db->select('stState,intStateId');
    		$this->db->from('state_master');
	     $res =	$this->db->get();
	     if($res->num_rows()>'0') {
	     	$res = $res->result();
	     	return $res;
	     }
    }
    public function selectCity($id) {
    		$this->db->select('stCity,intCityId');
    		$this->db->from('city_master');
    	    $this->db->where('intStateId',$id);
	        
	        $response =	$this->db->get()->result();
	        return $response;
    }
    public function getCity($state_id) 
    {
    	$res = $this->db->where('intStateId', $state_id)->get('city_master')->result();
    	return $res;
    }
    public function addSociety($society_data) {
 

		$this->db->insert('society_master',$society_data);	
    }
    public function addSocietyUser($society_user) {

    	$this->db->insert('users',$society_user);
		return true;
    }
    public function showSociety($id) {

    	$this->db->select('id,code,name,no_of_house,address,stState as state,stCity as city,pincode,image as society_pic,created_by,DATE_FORMAT(created_date,"%d/%m/%Y") as created_date');
			$this->db->from('society_master sm');
			$this->db->join('city_master cm','sm.city=cm.intCityId');
			$this->db->where('sm.id',$id);
			$res  =	$this->db->get()->result();
			return $res;
    }
    public function getData($id) {

	    	$this->db->select('id,code,name,no_of_house,address,stState as state,intStateId,stCity as city,intCityId,pincode,image');
	    	$this->db->from('society_master sm');
			$this->db->join('city_master cm','sm.city=cm.intCityId');
	    	$this->db->where('id',$id);
			$res  =	$this->db->get()->result();
			// show($res,1);
			return $res;
    }
    public function updateSociety($society_data,$society_id) {

		$this->db->where('id',$society_id);
		$this->db->update('society_master',$society_data);	
		return true;
}
	public function allSociety() {
		$sess_id = $this->session->userdata('role_id');
		$society_id = $this->session->userdata('society_id');

		$this->db->select('id,code,name,no_of_house,address,stState as state,stCity as city,pincode,image,created_by,DATE_FORMAT(created_date,"%d/%m/%Y") as created_date');
		$this->db->from('society_master sm');
		$this->db->join('city_master cm','sm.city=cm.intCityId');
		$this->db->order_by('sm.id','DESC');
		if($sess_id ==SUPERADMIN) {

		} else {

			$this->db->where('sm.id',$society_id);
		}
			$this->db->where('sm.is_deleted','N');
			$res  =	$this->db->get()->result();
		return $res;
	}
	public function deleteSociety($id) {

		$this->db->set('is_deleted','Y');
		$this->db->where('id',$id);
      	$this->db->update('society_master');
      	return true;
	}
	public function get_society_count()
   {
       $CY = date('Y');
       $year_count = array();
       $month_array = array($CY.'-01',$CY.'-02',$CY.'-03',$CY.'-04',$CY.'-05',$CY.'-06',$CY.'-07',$CY.'-08',$CY.'-09',$CY.'-10',$CY.'-11',$CY.'-12');
       foreach ($month_array as $value) {
              $this->db->select('count(id) as society_count');
              $this->db->from($this->table);
              $this->db->where('DATE_FORMAT(created_date,"%Y-%m")',$value);              $result = $this->db->get()->result();              $year_count[$value] = $result[0]->society_count;
             
       }
       return $year_count;
   }
    public function society_email_exit($val){
   	$this->db->where('email',$val);
   	return $this->db->get('users');
   }
   
}