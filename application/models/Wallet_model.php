<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wallet_model extends MY_Model {

    protected $table = 'wallet';
    var $fields = array("id", "user_id","current_outstanding","last_transaction_id","updated_at","created_at");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
    }

}
