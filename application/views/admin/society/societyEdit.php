  <?php if($this->session->flashdata('message')){?>
          <div align="center" class="alert alert-success">      
            <?php echo $this->session->flashdata('message')?>
          </div>
        <?php } ?>
<section class="content">
      <div class="row">
        <!-- left column -->
      <form method="post" action="<?= base_url()?>back/registration/updatesociety" enctype="multipart/form-data" id="society_form">
         <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Society</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              <div class="box-body">
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="society_name">Society Name *</label>
                    <div class="col-sm-5">
                    <input type="text" class="form-control" name="society_name" id="society_name" value="<?= $society[0]->name; ?>">
                  </div>
                </div>
                 <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="sstate">Select State *</label>
                  <div class="col-sm-5">
                    <select name="state" class="form-control" id="state" value="<?= $state->stState?>">
                      <option value="<?= $society[0]->intStateId;?>"><?= $society[0]->state;?></option>
                      <?php foreach($state as $state){?>
                      <option value="<?= $state->intStateId?>"><?= $state->stState; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="house">Select City *</label>
                    <div class="col-sm-5">
                     <select name="city" class="form-control" id="city" style="max-width: 61%">
                      <option value="<?= $society[0]->intCityId;?>"><?= $society[0]->city;?></option>
                      <?php foreach($city as $city){?>
                      <option value="<?= $city->intCityId?>"><?= $city->stCity; ?></option>
                      <?php } ?>
                    </select> 
                    </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="address">Address *</label>
                  <div class="col-sm-5">
                   <input name="address" class="form-control" id="address" value="<?= $society[0]->address; ?>" />
                 </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="pincode">Pincode *</label>
                  <div class="col-sm-5">
                    <input type="text" name="pincode" class="form-control" value="<?= $society[0]->pincode; ?>"/>
                  </div>
                </div>
                 <div class="form-group row">
                      <label class="col-sm-3 col-form-label col-sm-offset-1" for="total_house">Total House In The Society *</label>
                      <div class="col-sm-5">
                      <input type="number" class="form-control" id="total_house" name="no_of_house" value="<?= $society[0]->no_of_house?>">
                    </div>
                 </div>
                 <div class="form-group row">
                        <label class="col-sm-3 col-form-label col-sm-offset-1" for="societypic">Society Image</label>
                        <div class="col-sm-5">
                          <input type="file" size="20" name="societypic" id="societypic" value="<?= $society[0]->image?>" />
                        <img src="<?= base_url().'upload/societyImages/'.$society[0]->image; ?>" width="150px;" alt="Society Image" id="editImage">
                        <input type="hidden" value="<?= $society[0]->image;?>" name="sc_img"/>
                           <p class="help-block">Your Society Picture.</p>
                           <input type="hidden" name="society_id" value="<?= $society[0]->id; ?>" />
                      </div>
                 </div>
                    <!--  <div class="box-footer" style="padding-left: 80px;">
                      <input type="hidden" name="society_id" value="<?= $society[0]->id; ?>" />
                        <button type="submit" class="btn btn-primary">Update Society</button>
                    </div> -->
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="submit"></label>
                    <div class="col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Society</button>&nbsp;&nbsp;&nbsp;
                        <a href="<?= base_url()?>back/registration/allsociety" class="btn btn-danger"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</a>
                      </div>
                  </div>
              </div>
            </div>
          </div>
      </form>
  </div>
</section>
 <script type="text/javascript">
  $(document).ready(function() {
  	$('#state').trigger('change');
    $('#state').on('change',function(){
      var val = $('#state').val();
      $("#city").empty();
        $.ajax({
          url: '<?php echo base_url();?>back/registration/selectCity',
          type:'POST',
          data:{val:val},
          success:function(data)
          {
            $("#city").append(data); 
          }
        });
    });
  });
</script>