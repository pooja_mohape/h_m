<section class="content">
      <div class="row">
        <!-- left column -->
      <form method="post" action="<?= base_url()?>back/registration/addsociety" enctype="multipart/form-data" id="society_form">
         <div class="col-sm-12">
          <!-- general form elements -->
          <div class="box box-primary">
            
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-header with-border">
              <h2 class="box-title">Create Society</h2>
            </div>

              <div class="box-body">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="society_name">Society Name *</label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" name="society_name" id="society_name" placeholder="Enter Society Name" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="society_name">Email *</label>
                    <div class="col-sm-5">
                      <input type="email" class="form-control" name="email" id="email" placeholder="Enter Email" />
                       <div id="society_email_exist"></div>
                    </div>
          
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="society_name">Mobile *</label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Enter Mobile Number" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="sstate">State Name *</label>
                    <div class="col-sm-5">
                      <select name="state" class="form-control" id="state">
                        <option value="">Select State</option>
                        option
                         <?php foreach($state as $state){?>
                          <option value="<?= $state->intStateId?>"><?= $state->stState; ?></option>
                         <?php } ?>
                      </select>
                    </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="house">City Name *</label>
                    <div class="col-sm-5">
                      <select name="city" class="form-control" id="city">
                        <option value="">Select City</option>
                        option
                      </select> 
                    </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="address">Address *</label>
                  <div class="col-sm-5">
                   <input name="address" class="form-control" id="address" placeholder="Enter Address of Society" />
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="pincode">Pincode *</label>
                    <div class="col-sm-5">
                     <input type="text" name="pincode" class="form-control" placeholder="Enter Pincode"/>
                    </div>
                </div>
                 <div class="form-group row">
                      <label class="col-sm-3 col-form-label col-sm-offset-1" for="total_house">Total House In The Society *</label>
                      <div class="col-sm-5">
                      <input type="number" class="form-control" id="total_house" name="no_of_house" placeholder="Enter number Of House" />
                    </div>
                 </div>
                  <div class="form-group row">
                      <label class="col-sm-3 col-form-label col-sm-offset-1" for="username">Username for The Society *</label>
                      <div class="col-sm-5">
                      <input type="text" class="form-control" id="username" name="username" placeholder="Enter Username for The Society" />
                      <p id="existing_user" style="color: red"></p>
                    </div>
                 </div>
                  <div class="form-group row">
                      <label class="col-sm-3 col-form-label col-sm-offset-1" for="password">Password for Society *</label>
                      <div class="col-sm-5">
                      <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" />
                    </div>
                 </div>
                 <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="societypic">Society Image</label>
                    <div class="col-sm-5">
                     <input type="file" size="20" name="societypic" id="societypic"/>
                     <p class="help-block">Your Society Picture.(only png,jpg,jpeg,gif)</p>
                  </div>
                 </div>
                     <div class="form-group row">
                      <label class="col-sm-3 col-form-label col-sm-offset-1" for="total_house"></label>
                      <div class="col-sm-3">
                         <button type="submit" class="btn btn-primary">Create Society</button>&nbsp;&nbsp;
                          <a href="javascript:window.history.go(-1);" class="btn btn-danger"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</a>
                    </div>
                 </div>
              </div>
            </div>
          </div>
      </form>
  </div>
</section>
 <script type="text/javascript">
  $(document).ready(function() {
    $('#state').on('change',function(){
      var val = $('#state').val();
      $("#city").empty();
        $.ajax({
          url: '<?php echo base_url();?>back/registration/selectCity',
          type:'POST',
          data:{val:val},
          success:function(data)
          {
            $("#city").append(data); 
          }
        });
    });
    $('#state').trigger('change');
  });

</script>
<script type="text/javascript">
  $(document).ready(function() {
     
      // $.validator.addMethod("mobileno_exists", function (value, element) {
      //       var flag;

      //       $.ajax({
      //           url: base_url + 'back/registration/is_mobile_no_registered',
      //           data: {mobile: value},
      //           async: false,
      //           type: 'POST',
      //           success: function (r) {
      //               flag = (r === 'true') ? false : true;
      //           }
      //       });
      //       return flag;
      //   }, 'Mobile no. already exsist with us');
    /******** mobile validation start here ********/
    /******** email validation start here ********/
     // $.validator.addMethod("email_exists", function (value, element) {
     //        var flag;
     //        $.ajax({
     //            url: base_url + 'back/registration/is_email_registered',
     //            data: {email: value},
     //            async: false,
     //            type: 'POST',
     //            success: function (r) {
     //                flag = (r === 'true') ? false : true;
     //            }
     //        });

     //        return flag;
     //    }, 'Email id already exsist with us');

      $.validator.addMethod("username_exists", function (value, element) {
            var flag;
            $.ajax({
                url: base_url + 'back/registration/is_username_registered',
                data: {username: value},
                async: false,
                type: 'POST',
                success: function (r) {
                    flag = (r === 'true') ? false : true;
                }
            });

            return flag;
        }, 'Username already exsist with us');
  });
</script>