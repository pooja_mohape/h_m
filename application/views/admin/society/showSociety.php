<section class="content">
   <div class="row">
    <div class="col-md-12">
        <div class="col-md-6">
           <h3 style="">Society Details</h3>
        </div>
         <div class="col-md-6">
           <a href="javascript:window.history.go(-1);" class="btn btn-danger pull-right"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</a>
        </div>
    </div>
  </div>
      <div class="row">
         <div class="col-md-6">
                <div class="box box-primary">
                  <div class="box-header with-border">
                  </div>
                  <div class="box-body">
                   <img src="<?= base_url().'upload/societyImages/'.$society[0]->society_pic; ?>" class="img-responsive center-block" width="300px;" height="300px;"/>
                   <p class="text-center" style="padding:5px;"><strong><?= ucfirst($society[0]->name).' Society';?></strong></p>
                </div>
              </div>
            </div>
         <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">View Society</h3>
            </div>
              <div class="box-body">
                <table class="table table-responsive table-condensed table-hover table-bordered">
                  <!-- <caption>Personal Details</caption> -->
                  <thead></thead>
                  <tbody>
                    <tr>
                        <td>Socitey Name</td>
                        <td><?= $society[0]->name; ?></td>
                    </tr>
                     <tr>
                          <td>No Of House</td>
                          <td><?= $society[0]->no_of_house; ?></td>
                      </tr>
                      <tr>
                          <td>State</td>
                          <td><?= $society[0]->state; ?></td>
                      </tr>
                     <tr>
                        <td>City</td>
                        <td><?= $society[0]->city; ?></td>
                    </tr>
                     <tr>
                        <td>Address</td>
                        <td><?= $society[0]->address; ?></td>
                    </tr>
                     <tr>
                        <td>Pincode</td>
                        <td><?= $society[0]->pincode; ?></td>
                    </tr>
                    <tr>
                        <td>Created Date</td>
                        <td><?= $society[0]->created_date; ?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
             <div class="clearfix" style="height: 10px;clear: both;"></div>
                       <div class="form-group">
                                <label class="col-lg-3 control-label" for="commission from"></label>
                                <div class="col-lg-6">
                                </div>
                            </div>
          </div>
  </div>
</section>