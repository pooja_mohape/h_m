<script src="<?php echo base_url()?>assets/common_js/net_banking.js"></script>

<section class="content">
    <div id="mail_template_wrapper">
        <?php
        $this->load->view(NET_BANKING_AJAX);
        ?>
    </div>

    <?php $this->load->view(NET_BANKING_POPUP);?>
</section>
