<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of menu
 * 
 * @author jay butere<jayvant@stzsoft.com>
 * 
 * Revision History
 * 
 * 
 */
class Societycharges extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('society_house_charges_model','parking_charges_model'));
    }
    public function index()
    {
        $data = array();
        $data['house_type'] = $this->society_house_charges_model->where('society_id',$this->session->userdata('society_id'))->as_array()->find_all();
        $data['parking_charges'] = $this->parking_charges_model->where('society_id',$this->session->userdata('society_id'))->as_array()->find_all();
        load_back_view('admin/society_charges/add_charges',$data);
    }
    public function add_charges()
    {
        $input = $this->input->post();
        if($input)
        {
            if($input['house_type_check'] || $input['parking_check'])
            {
                if($input['house_type_check'] && $input['parking_check'])
                {
                       $house_type = $input['house_type'];
                       
                       for($i = 0;$i < count($input['house_type']); $i++) {
                        $insert_house_details = array();
                       
                        $insert_house_details['society_id'] = $this->session->userdata('society_id');
                        $insert_house_details['house_type'] = $input['house_type'][$i];
                        $insert_house_details['maintenance_charges'] = $input['main_amt'][$i];
                        $insert_house_details['lease_amt'] = $input['lease_amt'][$i];
                        $insert_house_details['created_by'] = $this->session->userdata('user_id');

                        $inserted_id = $this->society_house_charges_model->insert($insert_house_details);
                      } 
                      for($i = 0;$i < count($input['parking_type']); $i++) {
                        $insert_park_details = array();
                       
                        $insert_park_details['society_id'] = $this->session->userdata('society_id');
                        $insert_park_details['vehicle_type'] = $input['parking_type'][$i];
                        $insert_park_details['parking_amt'] = $input['park_amt'][$i];

                        $inserted_id = $this->parking_charges_model->insert($insert_park_details);
                      } 
                        $this->session->set_flashdata('msg', 'Thank You..Information submitted successfully');
                        $this->session->set_flashdata('msg_type', 'success');
                        redirect(base_url().'back/Societycharges');


                }else if ($input['house_type_check']) {
                   
                   for($i = 0;$i < count($input['house_type']); $i++) {
                        $insert_house_details = array();
                       
                        $insert_house_details['society_id'] = $this->session->userdata('society_id');
                        $insert_house_details['house_type'] = $input['house_type'][$i];
                        $insert_house_details['maintenance_charges'] = $input['main_amt'][$i];
                        $insert_house_details['lease_amt'] = $input['lease_amt'][$i];
                        $insert_house_details['created_by'] = $this->session->userdata('user_id');

                        $inserted_id = $this->society_house_charges_model->insert($insert_house_details);
                      } 
                       $this->session->set_flashdata('msg', 'Thank You..Information submitted successfully');
                        $this->session->set_flashdata('msg_type', 'success');
                        redirect(base_url().'back/Societycharges');

                }elseif ($input['parking_check']) {
                  
                   for($i = 0;$i < count($input['parking_type']); $i++) {
                        $insert_park_details = array();
                       
                        $insert_park_details['society_id'] = $this->session->userdata('society_id');
                        $insert_park_details['vehicle_type'] = $input['parking_type'][$i];
                        $insert_park_details['parking_amt'] = $input['park_amt'][$i];

                        $inserted_id = $this->parking_charges_model->insert($insert_park_details);
                      } 
                        $this->session->set_flashdata('msg', 'Thank You..Information submitted successfully');
                        $this->session->set_flashdata('msg_type', 'success');
                        redirect(base_url().'back/Societycharges');

                }else
                {
                  
                }
            }
            else
            {
               $this->session->set_flashdata('msg', 'select atleast one checkbox');
                $this->session->set_flashdata('msg_type', 'success');
                redirect(base_url().'back/Societycharges');
            }
        }
        else
        {
            $this->session->set_flashdata('msg', 'Please provide valid input');
            $this->session->set_flashdata('msg_type', 'success');
            redirect(base_url().'back/Societycharges');
        }

    }
    public function edit_house_charge($id)
    {
      show($id,1);
    }
    public function delete_charge($id)
    {
      show($id,1);
    }
    public function edit_park_charges($id)
    {
      show($id,1);
    }
    public function delete_park($id)
    {
      show($id,1);
    }
}

//end of menu class