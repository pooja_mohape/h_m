<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                           <div class="col-lg-7"> 
                                <h3 style="text-align: center;">
                                    Update House Details
                                     <hr>
                                </h3>
                            </div>
                        </div>
                      <div class="box-body">
                         
                  <form id="payment_request" name="payment_request" method="post" action="<?php echo base_url();?>back/societycharges/update/">
                    <div class="form-group">
                     
                     <label class="col-lg-2 control-label">House Type</label>
                      <div class="col-lg-4"> 
                      <input type="text" class="form-control" name="house_type" id="house_type" value="<?php echo $house_type[0]['house_type'];?>" readonly>
                    </div>
                  <div class="clearfix" style="height: 10px;clear: both;"></div>
                   <div class="form-group">
                      <label class="col-lg-2 control-label">Maintenance Amount</label>
                       <div class="col-lg-4"> 
                    <input type="text" class="form-control" name="maintenence_amount" id="maintenence_amount" value="<?php echo $house_type[0]['maintenance_charges'];?>">
                  </div>
                </div>
                 <input type="hidden" name="id" value="<?php echo $house_type[0]['id'];?>">
                 <div class="clearfix" style="height: 10px;clear: both;"></div>
                   <div class="form-group">
                      <label class="col-lg-2 control-label">Lease Amount</label>
                       <div class="col-lg-4"> 
                    <input type="text" class="form-control" name="lease_amount" id="lease_amount" value="<?php echo $house_type[0]['lease_amt'];?>">
                  </div>
                </div>
                       <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                      <label class="col-lg-2 control-label"></label>
                       <div class="col-lg-4"> 
                        <center>
                            <button type="submit" id="submit" class="btn btn-primary">Update</button>&nbsp;&nbsp; 
                              <a class="btn btn-danger" href="<?php echo  base_url().'back/societycharges'?>" type="button">Back</a>
                              </center> 
                          
                  </div>
                  </div>
                </div>
                       <div class="clearfix" style="height: 10px;clear: both;"></div>
                <!--    <div class="col-lg-offset-3">
                  <button type="submit" id="submit" class="btn btn-primary pull-left">Save</button>
                  </div>
                      <div class="form-group">
                          <div class="col-lg-offset-4">
                              <a class="btn btn-primary" href="<?php echo  base_url().'back/parking'?>" type="button">Back</a> 
                          </div>
                      </div> -->

                                
                          </form>
                    <!--</div> /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<script>
  $( function() {
       $( "#transaction_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
  } );
</script>