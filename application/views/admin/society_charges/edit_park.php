<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                           <div class="col-lg-7"> 
                                <h3 style="text-align: center;">
                                    Update Parking Details
                                     <hr>
                                </h3>
                            </div>
                        </div>
                      <div class="box-body">
                         
                  <form id="payment_request" name="payment_request" method="post" action="<?php echo base_url();?>back/societycharges/update_park/">
                    <div class="form-group">
                     
                     <label class="col-lg-2 control-label">Vehicle_Type</label>
                      <div class="col-lg-4"> 
                      <input type="text" class="form-control" name="vehicle_type" id="vehicle_type" value="<?php echo $park_charge[0]['vehicle_type'];?>" readonly>
                    </div>
                  <div class="clearfix" style="height: 10px;clear: both;"></div>
                   <div class="form-group">
                      <label class="col-lg-2 control-label">Parking Amount</label>
                       <div class="col-lg-4"> 
                    <input type="text" class="form-control" name="parking amount" id="parking amount" value="<?php echo $park_charge[0]['parking_amt'];?>">
                  </div>
                </div>
                 <input type="hidden" name="id" value="<?php echo $park_charge[0]['id'];?>">

                       <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                      <label class="col-lg-2 control-label"></label>
                       <div class="col-lg-4"> 
                        <center>
                            <button type="submit" id="submit" class="btn btn-primary">Update</button>&nbsp;&nbsp; 
                              <a class="btn btn-danger" href="<?php echo  base_url().'back/societycharges'?>" type="button">Back</a>
                              </center> 
                          
                  </div>
                  </div>
                </div>
                       <div class="clearfix" style="height: 10px;clear: both;"></div>
                                
                          </form>
                    <!--</div> /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<script>
  $( function() {
       $( "#transaction_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
  } );
</script>