 <link rel="stylesheet" href="<?php echo base_url().ASSETS_BACK_THEME; ?>plugins/iCheck/flat/red.css">
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h3>
        Edit Permissions
        <small>Edit Permissions for roles</small>
      </h3>
    </section>

    <!-- Main content -->
    <section class="content edit_permissions">
      <div class="row">
      <div class="col-xs-12">     
            <div class="alertbox"></div>
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Role : <?php echo $role_info['role_name']; ?></h3>            
              <span style="float:right; margin-left: 10px;"><button class="btn btn-primary goback_btn" id="goback_btn">Go Back</button></span>
              <span style="float:right;"><button class="btn btn-primary save_btn" id="save_btn">Save</button></span>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>                  
                  <th rowspan="2">Modules</th>
                  <th colspan="4">Common Permissions</th>
                  <th rowspan="2">Extra Permissions</th>
                </tr>
                <tr>
                  <th><small>View</small></th>
                  <th><small>Add</small></th>
                  <th><small>Edit</small></th>
                  <th><small>Delete</small></th>                
                </tr>
                
                <?php
                if(sizeof($permission_matrix) > 0)
                {
                    $um = 1;
                    foreach($permission_matrix as $module)
                    {
                        $permissions = $module['permissions'];
                        $module_id = $module['module_id'];
                ?>
                    <tr>
                    
                    <td><?php if(isset($module['module_name'])) echo $module['module_name']; ?></td>
                    
                    <?php               
                    if(array_key_exists("view", $permissions))
                    {                       
                        $checked = ($permissions['view']['permission_val'] == 1) ? "checked" : "";
                        echo "<td><input type='checkbox' class='op_check' data-uniq='".$um."c1' data-mid='".$module_id."' data-optype='common' data-opkey='view' data-opid='". $permissions['view']['id'] ."' data-poldval='". $permissions['view']['permission_val'] ."' ".$checked." value='1' ></td>";                       
                    }
                    else
                    {   echo "<td>  </td>"; }               
                    ?>
                    
                    <?php               
                    if(array_key_exists("add", $permissions))
                    {
                        $checked = ($permissions['add']['permission_val'] == 1) ? "checked" : "";
                        echo "<td><input type='checkbox' class='op_check' data-uniq='".$um."c2' data-mid='".$module_id."' data-optype='common' data-opkey='add' data-opid='". $permissions['add']['id'] ."' data-poldval='". $permissions['add']['permission_val'] ."' ".$checked." value='1' ></td>";                      
                    }
                    else
                    {   echo "<td>  </td>"; }               
                    ?>
                    
                    <?php               
                    if(array_key_exists("edit", $permissions))
                    {
                        $checked = ($permissions['edit']['permission_val'] == 1) ? "checked" : "";
                        echo "<td><input type='checkbox' class='op_check' data-uniq='".$um."c3' data-mid='".$module_id."' data-optype='common' data-opkey='edit' data-opid='". $permissions['edit']['id'] ."' data-poldval='". $permissions['edit']['permission_val'] ."' ".$checked." value='1' ></td>";                       
                    }
                    else
                    {   echo "<td>  </td>"; }               
                    ?>
                    
                    <?php               
                    if(array_key_exists("delete", $permissions))
                    {
                        $checked = ($permissions['delete']['permission_val'] == 1) ? "checked" : "";
                        echo "<td><input type='checkbox' class='op_check' data-uniq='".$um."c4' data-mid='".$module_id."' data-optype='common' data-opkey='delete' data-opid='". $permissions['delete']['id'] ."' data-poldval='". $permissions['delete']['permission_val'] ."' ".$checked." value='1' ></td>";                     
                    }
                    else
                    {   echo "<td>  </td>"; }               
                    ?>
                    
                    <td>                    
                    <?php                   
                    if(array_key_exists("extra", $permissions))
                    {
                        $ume = $um."e"; 
                        $op = 1;
                        foreach($permissions['extra'] as $extmodule)
                        {                       
                        $checked = $extmodule['permission_val'] == 1 ? "checked" : "";
                        echo "<div><input type='checkbox' class='op_check' data-uniq='".$ume.$op."' data-mid='".$module_id."' data-optype='extra' data-opkey='' data-opid='". $extmodule['id'] ."' data-poldval='". $extmodule['permission_val'] ."' ".$checked." value='1' >";
                        echo "&nbsp; &nbsp;".$extmodule['name']."</div>";                       
                                                $op = $op + 1;                      
                        }                   
                    }               
                    ?>              
                    </td>
                    </tr>
                <?php
                    $um = $um+1;                
                    }               
                }               
                ?>             
              
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script src="<?php echo base_url().ASSETS_BACK_THEME; ?>plugins/iCheck/icheck.js"></script>
<script>
$(document).ready(function(){
  $('input').iCheck({
    checkboxClass: 'icheckbox_flat-red',
    radioClass: 'iradio_flat-red'
  });
});

(function($) {
    BASE_URL = '<?php echo base_url(); ?>';
    role_id = '<?php echo $role_info['id']; ?>';
    save_permission = [];
    disableSave = false;
})(jQuery);


$('input').on('ifChanged', function(event){
    var elem = $(this);
    
    var uniq = elem.attr("data-uniq");
    var mid = elem.attr("data-mid");
    var optype = elem.attr("data-optype");
    var opkey = elem.attr("data-opkey");
    var opid = elem.attr("data-opid");
    var poldval = elem.attr("data-poldval");
    
    var pnewval = event.target.checked ? "1" : "0"; 
    //alert('checked = ' + event.target.checked);
    
    console.log(uniq);
    console.log(mid);
    console.log(optype);
    console.log(opkey);
    
    console.log(opid);
    console.log(poldval);
    console.log(pnewval);

    var addFlag = true; 
    if(save_permission.length > 0)
    {
        var len = save_permission.length;
        for(var i=0; i<len; i++)
        {
            if(save_permission[i].uniq == uniq)
            {// elem found
                save_permission[i].pnewval = pnewval;
                addFlag = false;                
            }       
        }
    }
    
    if(addFlag)
    {   // add elem     
        var elemObj = {
        uniq: uniq,
        role_id: role_id,
        mid: mid,
        optype: optype,
        opkey: opkey,
        opid: opid,
        poldval: poldval,
        pnewval: pnewval
        };  
        save_permission.push(elemObj);
    }   
    console.log(save_permission);
});


function save_perm()
{   
    //$(document).off("click","#save_btn", save_perm); 
    var len = save_permission.length;
    var newsavearr = [];
    if(save_permission.length > 0)
    {               
        for(var i=0; i<len; i++)
        {
            if(save_permission[i].pnewval != save_permission[i].poldval)
            {// add element to newsavearr if old value is not equal to new value
                newsavearr.push(save_permission[i]);        
            }       
        } 
    }   
    
    if(newsavearr.length > 0)
    {
        $.ajax({
            url: BASE_URL + 'admin/role/save_permission',
            data: {save_permission: newsavearr,rokad_token:rokad_token},
            async: false,
            type: 'POST',
            success: function (msg) {
                show_alert("alert-success", msg);
                location.reload();
            }
        });
    }
    else
    {
        console.log("No changes found");
        show_alert("alert-info", "No changes found");
    }
}

function show_alert(error_type, error_msg)
{
    var elem = $(".edit_permissions").find(".alertbox");
    elem.html("");
    var alerttmpl = '<div class="alert '+error_type+' alert-dismissible">';
        alerttmpl += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
        alerttmpl += '<h4><i class="icon fa fa-ban"></i> Alert!</h4>';
        alerttmpl += error_msg +'</div>';
    elem.html(alerttmpl);
}

$(document).on("click","#save_btn", save_perm); 

$(document).on("click", "#goback_btn", function(){
    window.location = BASE_URL+"admin/role";    
}); 

</script>