    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3>
            &nbsp;&nbsp;View User Role
             <!--<small>advanced tables</small>-->
        </h3>
       
    </section>
<?php //show($pro_data,1);?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                 <?php
                            $attributes = array("method" => "POST", "id" => "group_create_form", "class" => "group_create_form");
                            echo  form_open(site_url().'/back/user_role/update/'.$group->id, $attributes);
                       ?>
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                       
                        <div class="form-group">

                            <label class="col-lg-3 control-label" for="name">Name:</label>
                            <div class="col-lg-6">
                                <input name="role_id" type="hidden" id="role_id" class="form-control" value="<?php echo $group->id?>">
                                <input name="role_name" type="text" id="role_name" class="form-control" value="<?php echo $group->role_name ?>" >
                               
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                       

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="op_addr_1">Homepage: </label>
                            <div class="col-lg-6">
                                <select name="homepage" class="form-control" id="front_back_access">
                                    <option value=''>Dashboard</option>
                                </select>
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="op_addr_1">Access: </label>
                            <div class="col-lg-6">
                                <select name="access" class="form-control" id="front_back_access">
                                    <option value=''>Back Access</option><!-- 
                                    <option value='front' <?php echo ($group->front_back_access == 'front') ? 'selected': ''?>>Front</option>
                                    <option value="back" <?php echo ($group->front_back_access == 'back') ? 'selected': ''?>> Back</option>
                                    <option value="both" <?php echo ($group->front_back_access == 'both') ? 'selected': ''?>> Both</option> -->
                                </select>
                            </div>
                        </div>


                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="op_addr_1">Status: </label>
                            <div class="col-lg-6">
                              <select name="access" class="form-control" id="front_back_access">
                                    <option value=''>Active</option><!-- 
                                    <option value='front' <?php echo ($group->front_back_access == 'front') ? 'selected': ''?>>Front</option>
                                    <option value="back" <?php echo ($group->front_back_access == 'back') ? 'selected': ''?>> Back</option>
                                    <option value="both" <?php echo ($group->front_back_access == 'both') ? 'selected': ''?>> Both</option> -->
                                </select>
                            </div>
                        </div>
                       
                       
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">

                                <button class="btn btn-primary" id="save_group_data" name="save_group_data" type="submit" style="display: none;">Save</button> 
                                <button class="btn btn-primary back" id="back_data" type="button">Back</button> 
                            </div>
                        </div>

                         
                    </div>
                <!-- /.box-body -->
                </div></form><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->



<script>

    $(document).ready(function() {

    $(document).off('click', '.back').on('click', '.back', function(e)
    {

        window.location.href = base_url + 'back/user_role/';
    });
    });
</script>
