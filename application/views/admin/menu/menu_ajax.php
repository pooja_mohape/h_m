<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Manage Menus</h3>
                <span style="float: right;margin-top: 10px;margin-right: 10px;"><button class="btn btn-primary add_btn">Add Menu</button></span>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="menu_display_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="20px">#</th>
                            <th>Name</th>
                            <th>Parent Name</th>
                            <th>Link</th>
                            <th>Class</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>                                           

                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>


