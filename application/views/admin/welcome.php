 <section class="content">
  <?php if($this->session->userdata('role_id')!=SUPERADMIN) {?>
        <div class="row">
    <div class="modal fade show_waiting" id="show_waiting" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <!-- tab container start--><h3>Hello <?= $this->session->userdata('first_name')?></h3>
            <center>
                <?php $id = $this->session->userdata('id'); ?>
                <div class="search_progress">
                    <span><b>Your Profile update is pending</b></span>
                    <br>
                    <span><h4 class="bold">Please <a href="<?php  echo base_url().'back/registration/edituser/'.$id ?>">Click here <span class="glyphicon glyphicon-hand-left"></span></a>  to update your details</h4></b></span>
                    <br>
                    <h5>Note: If your details is not updated,<br> you wil not get Event details &amp; Notices Update on your email &amp; mobile<br> So update to procede furthur</h5>
                </div>
            </center>
            <!-- tab container end-->
          </div>
        </div>
      </div>
    </div> 
  </div>
  <?php }?>
      <div class="row">
         <?php if($this->session->userdata('role_id') == SUPERADMIN){?>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-orange">
            <div class="inner">
              <h3><?php echo $society_count?></h3>

              <p>Society Count</p>
            </div>
            <div class="icon">
               <img height="80" width="80" src="<?php echo base_url().'assets/images/floor.png'?>">
            </div>
            <a href="<?php echo  base_url().'back/registration/allSociety'?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <?php }?>
        <?php if($this->session->userdata('role_id') == SOCIETY_ADMIN || $this->session->userdata('role_id') == SOCIETY_MEMBER){?>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-orange">
            <div class="inner">
              <h3>
                <i class="fa fa-inr"></i>
                <?php  
               $outstanding_amt = $outstanding_amt[0];
               $out_amt = isset($outstanding_amt->current_outstanding) ? $outstanding_amt->current_outstanding : '0.00';
                echo number_format($out_amt,2); ?></h3>

              <p>Total Outstanding Amount</p>
            </div>
             <div class="icon">
               <img height="80" width="80" src="<?php echo base_url().'assets/images/fair.png'?>">
            </div>
            <a href="<?php echo  base_url().'back/bill_payment/make_payment'?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

          <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h3>
                <i class="fa fa-inr"></i>
                 <?php  
                  $w_amt = isset($outstanding_amt->wallet_balance) ? $outstanding_amt->wallet_balance : '0.00';
                  echo number_format($w_amt,2); ?></h3>
              <p>Wallet Amount</p>
            </div>
             <div class="icon">
               <img height="80" width="80" src="<?php echo base_url().'assets/images/fund.png'?>">
            </div>
            <a href="<?php echo  base_url().'back/bill_payment_report/statement'?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $notices_count?></h3>

              <p>Latest Notice</p>
            </div>
            <div class="icon">
               <img height="80" width="80" src="<?php echo base_url().'assets/images/room.png'?>">
            </div>
            <a href="<?php echo  base_url().'back/notices'?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

         <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $document_count?></h3>

              <p>Documents Collection</p>
            </div>
            <div class="icon">
               <img height="80" width="80" src="<?php echo base_url().'assets/images/report.png'?>">
            </div>
            <a href="<?php echo  base_url().'back/document_collection'?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6" style="display: none;">
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $document_count?></h3>

              <p>Documents Collection</p>
            </div>
            <div class="icon">
               <img height="80" width="80" src="<?php echo base_url().'assets/images/report.png'?>">
            </div>
            <a href="<?php echo  base_url().'back/document_collection'?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo isset($meeting_count) ? $meeting_count : '0';?></h3>

              <p>Upcoming Meetings</p>
            </div>
            <div class="icon">
               <img height="80" width="80" src="<?php echo base_url().'assets/images/comittee.png'?>">
            </div>
            <a href="<?php echo  base_url().'back/meeting/allmeeting'?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-orange">
            <div class="inner">
              <h3><?php echo isset($event_count) ? $event_count : '0';?></h3>

              <p>Upcoming event</p>
            </div>
            <div class="icon">
               <img height="80" width="80" src="<?php echo base_url().'assets/images/report.png'?>">
            </div>
            <a href="<?php echo  base_url().'back/event/eventViewUser'?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <?php }?>

      
       <?php if($this->session->userdata('role_id')==
           SOCIETY_ADMIN ||$this->session->userdata('role_id')==
           SOCIETY_SUPERUSER){?>
        
       <div class="col-lg-3 col-xs-6">
         <div class="small-box bg-orange">          
           <div class="inner">              <h3><?php echo $user_count;?></h3>
             <p>Total Members</p>
           
           </div>
            <div class="icon">
               <img height="80" width="80" src="<?php echo base_url().'assets/images/rented.png'?>">
            </div>
           <a href="<?php echo base_url();?>back/registration/alluser" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
         </div>
       </div>

       <div class="col-lg-3 col-xs-6">
         <div class="small-box bg-green">          
           <div class="inner">              <h3><?php echo $complaint_count;?></h3>
             <p>Complaint</p>
           
           </div>
            <div class="icon">
               <img height="80" width="80" src="<?php echo base_url().'assets/images/comittee.png'?>">
            </div>
           <a href="<?php echo base_url();?>index.php/back/Complaint" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
         </div>
       </div>
      <?php }?>

        <?php if($this->session->userdata('role_id') == SOCIETY_SUPERUSER){?>
         <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $bill_payment_count?></h3>
               <p>Bill Payment Request</p>
            </div>
             <div class="icon">
               <img height="80" width="80" src="<?php echo base_url().'assets/images/fund.png'?>">
            </div>
            <a href="<?php echo base_url().'back/bill_payment'?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <?php }?>


          <?php if($this->session->userdata('role_id') == SOCIETY_SUPERUSER){?>
         <div class="col-lg-3 col-xs-6" style="display: none;">
          <div class="small-box bg-green">
            <div class="inner">
               <h4>Not receive: <?php echo $unread_count?></h4>
              <h4>Received Document : <?php echo $read_count?></h4>
               <p>Document Collection</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?php echo base_url().'back/document_collection/allDocument'?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <?php }?>


      </div>
      <?php if($this->session->userdata('role_id')==SUPERADMIN){?>      
      <div class="row" style="display: none;">
       <div class="col-md-12">
           <div id="chartContainer" style="height: 300px; width: 100%;"></div>
       </div>
     </div>
      <?php } ?>
    </section>
    <script>
<?php
if (isset($map_data)) {
  $map_data;
}
else
{
  $map_data = array();
}if ($map_data && !empty($map_data)) {
  $keys=array_keys($map_data);
  $dataPoints = array(
       array("y"=> $map_data[$keys[0]], "label"=> "Jan"),
       array("y"=> $map_data[$keys[1]], "label"=> "feb"),
       array("y"=> $map_data[$keys[2]], "label"=> "mar"),
       array("y"=> $map_data[$keys[3]], "label"=> "Apr"),
       array("y"=> $map_data[$keys[4]], "label"=> "May"),
       array("y"=> $map_data[$keys[5]], "label"=> "Jun"),
       array("y"=> $map_data[$keys[6]], "label"=> "Jul"),
       array("y"=> $map_data[$keys[7]], "label"=> "Aug"),
       array("y"=> $map_data[$keys[8]], "label"=> "Sep"),
       array("y"=> $map_data[$keys[9]], "label"=> "Oct"),
       array("y"=> $map_data[$keys[10]],"label"=> "Nov"),
       array("y"=> $map_data[$keys[11]],"label"=> "Dec")
 );
}
else
{
 $dataPoints = array();  
}
?>

window.onload = function () {//Better to construct options first and then pass it as a parameter
var options = {
 title: {
   animationEnabled: true,
   exportEnabled: true,
   text: "Society Registration chart"  },
 data: [              
 {
   axisX:{
       title: "society count",
       labelAngle: 45
     },
   // Change type to "doughnut", "line", "splineArea", etc.
   type: "column",
   indexLabelFontColor: "#5A5757",
       indexLabelPlacement: "outside",
   dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
 }
 ]
};$("#chartContainer").CanvasJSChart(options);
}
</script>
