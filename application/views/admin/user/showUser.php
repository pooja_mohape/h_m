<section class="content">
      <div class="row">
         <div class="col-md-6">
                <div class="box box-primary">
                  <div class="box-header with-border">
                  </div>
                  <div class="box-body">
                   <img src="<?= base_url().'upload/profileImages/'.$user[0]->profile_pic; ?>" class="img-responsive center-block" width="300px;" height="300px;"/>
                   <p class="text-center" style="padding:5px;"><strong><?= ucfirst($user[0]->first_name).' '.ucfirst($user[0]->last_name);?></strong></p>
                </div>
              </div>
            </div>
         <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">User Details</h3>
            </div>
              <div class="box-body">
                <table class="table table-responsive table-condensed table-hover table-bordered">
                  <caption>Personal Details</caption>
                  <thead></thead>
                  <tbody>
                    <tr>
                        <td>Name</td>
                        <td><?= ucfirst($user[0]->first_name).' '.ucfirst($user[0]->last_name); ?></td>
                    </tr>
                     <tr>
                          <td>Socitey Name</td>
                          <td><?= $user[0]->society_name; ?></td>
                      </tr>
                      <tr>
                          <td>Building</td>
                          <td><?= $user[0]->building; ?></td>
                      </tr>
                      <tr>
                          <td>Wing</td>
                          <td><?= $user[0]->wing; ?></td>
                      </tr>
                      <tr>
                          <td>Block</td>
                          <td><?= $user[0]->block; ?></td>
                      </tr>
                     <tr>
                        <td>Username</td>
                        <td><?= $user[0]->username; ?></td>
                    </tr>
                     <tr>
                        <td>Account status</td>
                        <?php $status=$user[0]->status; if($status=='Y'){?>
                            <td><?= 'Active'; ?></td>
                        <?php } else {?>
                        <td><?= 'In-Active'; ?></td>
                        <?php }; ?>
                    </tr>
                     <tr>
                        <td>Role</td>
                        <td><?= $user[0]->role_name; ?></td>
                    </tr>
                     <tr>
                        <td>Email</td>
                        <td><?= $user[0]->email; ?></td>
                    </tr>
                     <tr>
                        <td>Phone</td>
                        <td><?= $user[0]->phone; ?></td>
                    </tr>
                     <tr>
                        <td>Birth Date</td>
                        <td><?= $user[0]->birth_date; ?></td>
                    </tr>
                    <tr>
                        <td>Total Family Member</td>
                        <td><?= $user[0]->total_family_member; ?></td>
                    </tr>
                    <tr>
                        <td>Created Date</td>
                        <td><?= $user[0]->created_date; ?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>
                       <div class="form-group">
                                <label class="col-lg-3 control-label" for="commission from"></label>
                                <div class="col-lg-6">
                                     <!--  <button type="submit" href="<?php echo  base_url().'back/parking'?> id="btn btn-danger" class="btn btn-primary pull-left">Back</button> -->
                                      &nbsp;&nbsp;&nbsp;&nbsp;
                                      <a class="btn btn-danger" href="<?php echo  base_url().'back/registration/alluser'?>" type="button">Back</a> 
                                </div>
                            </div>
          </div>
  </div>
</section>