
<script>
 $( function() {
    $( ".starttime" ).datepicker({minDate:0});  
    $( ".starttime" ).change(function(){
     var dt=$( ".starttime" ).val();
     var d = new Date(dt);
     $( ".starttime_db" ).val(dt);
     var c= d.toString().split(' ')[1]+" "+d.toString().split(' ')[2]+", "+ d.toString().split(' ')[3];
     $( ".starttime" ).val(c);
    }) 
  });
 $( function() {
    $( ".endtime" ).datepicker({minDate:0});
      $( ".endtime" ).change(function(){
     var dt=$( ".endtime" ).val();
       $( ".endtime_db" ).val(dt);
     var d = new Date(dt);
     var c= d.toString().split(' ')[1]+" "+d.toString().split(' ')[2]+", "+ d.toString().split(' ')[3];
     $( ".endtime" ).val(c);
    }) 
  });

//   $(function(){
//     $('#time').combodate({
//         firstItem: 'name', //show 'hour' and 'minute' string at first item of dropdown
//         minuteStep: 1
//     });   
// });
//    $(function(){
//     $('#time1').combodate({
//         firstItem: 'name', //show 'hour' and 'minute' string at first item of dropdown
//         minuteStep: 1
//     });   
// });


 $(document).ready(function() {
  $('#containerIntro1').hide();
 $('.add_time').click(function(){
  $('#containerIntro1').show();
 });
   
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  var calendar = $('#calendar').fullCalendar({
   editable: true,
   header: {
    left: 'prev,next today',
    center: 'title',
    right: 'month,agendaWeek,agendaDay'
   },

   events: "<?php echo base_url()?>back/event/allevent",

   eventRender: function(event, element, view) {
    if (event.allDay === 'true') {
     event.allDay = true;
    } else {
     event.allDay = false;
    }
   
   },
   selectable: true,
   selectHelper: true,
   select: function(start, end, allDay) {
      var startdate = $.fullCalendar.formatDate(start, "Y-MM-DD");
   var todayDate = new Date().toISOString().slice(0, 10);
   $(".today_date").val(todayDate);
   $("#error_title").html("");
   if(startdate < todayDate)
   {

   }else{
     $('#myModalHorizontal').modal('show');
      var d = new Date(start);
      var start = $.fullCalendar.formatDate(start, "MM/DD/Y");
      var end = $.fullCalendar.formatDate(end, "MM/DD/Y");
     $(".starttime_db").val(start);
     $(".endtime_db").val(start);
     

      var c= d.toString().split(' ')[1]+" "+d.toString().split(' ')[2]+", "+ d.toString().split(' ')[3];
       var e= d.toString().split(' ')[1]+" "+d.toString().split(' ')[2]+", "+ d.toString().split(' ')[3];
     $(".starttime").val(c);
     $(".endtime").val(e);
     

   $('.save_event').click(function(){
    var title=$('#event_form').serialize();
   var title_name=$("#event_title").val();
    if(title_name){
     var starttime_db=$(".starttime_db").val();
    var endtime_db=$(".endtime_db").val();
    var starttime=$("#time").val();
    var endtime=$("#time1").val();
         if(starttime_db == endtime_db){
      if(starttime <= endtime){
              $.ajax({
               url: '<?php echo base_url()?>back/event/add_event',
               data: title,
               type: "POST",
               success: function(json) {
               alert('Added Successfully');
               location.reload();
               }
             });
            }else{
               $("#error_title").html('Please select proper time slot.');
            }
    }else if(starttime_db > endtime_db){
       $("#error_title").html('Please select proper start date and end date.');
    }else{
          $.ajax({
               url: '<?php echo base_url()?>back/event/add_event',
               data: title,
               type: "POST",
               success: function(json) {
               alert('Added Successfully');
               location.reload();
               }
             });
    }
 }else{
  $("#error_title").html('Please enter event title.');
 }
   });
 }
   calendar.fullCalendar('unselect');
   },
   
   editable: true,
   eventDrop: function(event, delta) {
   var start = new Date(event.start).toISOString().slice(0, 10)+ " "+(new Date(event.start).toISOString().slice(11, 19));
   var end = new Date(event.end).toISOString().slice(0, 10)+ " "+(new Date(event.start).toISOString().slice(11, 19));
    var decision = confirm("Do you really want to update position?"); 
    if (decision) {
   $.ajax({
     url: '<?php echo base_url()?>back/event/update_event',
     data: 'title='+ event.title+'&start='+ start +'&end='+ end +'&id='+ event.eid ,
     type: "POST",
     success: function(json) {
      alert("Updated Successfully");
        }
      });
    }
   },
 
   eventClick: function(event) {
    $('#myModal').modal('show');

    $("#event_id").html(event.eid);
    $("#title").html(event.title);
     var d = new Date(event.start);
    var c= d.toString().split(' ')[0]+","+" "+ d.toString().split(' ')[1]+" "+d.toString().split(' ')[2]+", "+ d.toString().split(' ')[3];
     $("#date").html(c);
     $("#datetime").html(d.toString().split(' ')[4]) ;
    },
  
  }); 
  $(".eventDelete").click(function(){
   var id= $("#event_id").html();
    var decision = confirm("Do you really want to delete?"); 
  if (decision) {
  $.ajax({
    type: "POST",
    url: "<?php echo base_url()?>back/event/delete_event",
    data: "&id=" + id,
     success: function(json) {
       $('#calendar').fullCalendar('removeEvents', event.eid);
       alert("Deleted Successfully");
      location.reload();
      }
  });
  }
  })

 });
</script>

<style>
 body {
  font-size: 14px;
  font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
  }
 #calendar {
  width: 800px;
  margin: 0 auto;
  }
</style>
</head>
<body>

 <h2 style="padding:20px">Events</h2>
 <br/>
 <div id='calendar'></div>
<!--  -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
         <div class="alert alert-success">

          <button style="background:black;margin-left:350px" class="eventDelete"> <i class="fa fa-trash-o"></i></button>
           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       <div id="title" style="font-size: 25px;text-align: center"></div>
      
       <hr>
    
       <span class="glyphicon glyphicon-time" aria-hidden="true" style="font-size: 22px"></span> <div id="date" style="color: white;margin-top:-25px;margin-left:30px"></div><br>
      
      <span class="glyphicon glyphicon-calendar" aria-hidden="true" style="font-size: 18px"></span><div style="color: white;margin-top:-25px;margin-left:30px">Events For Society</div><br>
       <span class="glyphicon glyphicon-lock" aria-hidden="true" style="font-size: 18px"></span><div style="color: white;margin-top:-25px;margin-left:30px">Public</div>
       <div id="event_id" style="display:none"></div>

    </div>
  </div>
</div>
<!--  -->
<!--  -->
 <div class="modal fade" id="myModalHorizontal" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h2 class="modal-title" id="myModalLabel" style="color:white">
                    Add Event
                </h2>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                
                <form class="form-horizontal" role="form" id="event_form">
                  <div class="form-group">
                    <label  class="col-sm-2 control-label"
                              for="inputEmail3">Title</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" 
                        id="event_title" placeholder="Event Title" name="title"/ maxlength="">
                    </div>
                      <div id="error_title" style="margin-left:120px;color:red"></div>
                  </div>
                  <div class="form-group">
                    <span class="glyphicon glyphicon-calendar" aria-hidden="true" style="font-size: 22px;float:left;margin-left:75px"></span>
                    <div id="containerIntro" style="margin-left:120px;margin-right:20px">
                      <input type="hidden" class="starttime_db" name="starttime_db">
                      <input type="hidden" class="endtime_db" name="endtime_db">
                      <input type="hidden" class="today_date" name="today_date">
                    <input type="text" class="starttime" name="startdate" style="width:150px">
                    <input type="text" value="TO" style="width:40px">
                    <input type="text" class="endtime" name="enddate" style="width:150px">
                    <button type="button" class="add_time btn btn-primary" style="margin-top:2px">Add Time </button>
                    </div>

                     <div id="containerIntro1" style="margin-left:120px;margin-right:20px">
                  <!--  <input type="text" id="time" data-format="HH:mm" data-template="HH : mm"   name="starttime"> -->
                  <select name="starttime" size="8" style="padding-left:10px;padding-right: 40px" id="time">
                  <option value="12:00" >12:00 </option>
                  <option value="12:30" >12:30 </option>
                  <option value="1:00">1:00 </option>
                  <option value="1:30" >1:30 </option>
                  <option value="2:00" >2:00 </option>
                  <option value="2:30" >2:30 </option>
                  <option value="3:00" >3:00 </option>
                  <option value="3:30" >3:30 </option>
                  <option value="4:00" >4:00 </option>
                  <option value="4:30" >4:30 </option>
                  <option value="5:00" >5:00 </option>
                  <option value="5:30" >5:30 </option>
                  <option value="6:00" >6:00 </option>
                  <option value="6:30" >6:30 </option>
                  <option value="7:00">7:00 </option>
                  <option value="8:00" >8:00 </option>
                  <option value="8:30" >8:30 </option>
                  <option value="9:00" >9:00 </option>
                  <option value="9:30" >9:30 </option>
                  <option value="10:00" >10:00 </option>
                  <option value="10:30" >10:30 </option>
                  <option value="11:00" >11:00 </option>
                  <option value="11:30" >11:30 </option>
                  <option value="12:00" >12:00 </option>
                  <option value="12:30" >12:30 </option>
                  <option value="13:00" >13:00 </option>
                  <option value="13:30" >13:30 </option>
                  <option value="14:00" >14:00 </option>
                  <option value="14:30" >14:30 </option>
                  <option value="15:00" >15:00 </option>
                  <option value="15:30" >15:30 </option>
                  <option value="16:00" >16:00 </option>
                  <option value="16:30" >16:30 </option>
                  <option value="17:00" >17:00 </option>
                  <option value="17:30" >17:30 </option>
                  <option value="18:00" >18:00 </option>
                  <option value="18:30" >18:30 </option>
                  <option value="19:00" >19:00 </option>
                  <option value="19:30" >19:30 </option>
                  <option value="20:00" >20:00 </option>
                  <option value="20:30" >20:30 </option>
                  <option value="21:00" >21:00 </option>
                  <option value="21:30" >21:30 </option>
                  <option value="22:00" >22:00 </option>
                  <option value="22:30" >22:30 </option>
                  <option value="23:00" >23:00 </option>
                  <option value="23:30" >23:30 </option>
                 </select>
                   <input type="text" id="extra">
                   <!--  <input type="text" id="time1" data-format="HH:mm" data-template="HH : mm" name="endtime"> -->
                    <select name="endtime" size="8" style="padding-left:10px;padding-right: 40px" id="time1">
                  <option value="12:00" >12:00 </option>
                  <option value="12:30" >12:30 </option>
                  <option value="1:00">1:00 </option>
                  <option value="1:30" >1:30 </option>
                  <option value="2:00" >2:00 </option>
                  <option value="2:30" >2:30 </option>
                  <option value="3:00" >3:00 </option>
                  <option value="3:30" >3:30 </option>
                  <option value="4:00" >4:00 </option>
                  <option value="4:30" >4:30 </option>
                  <option value="5:00" >5:00 </option>
                  <option value="5:30" >5:30 </option>
                  <option value="6:00" >6:00 </option>
                  <option value="6:30" >6:30 </option>
                  <option value="7:00">7:00 </option>
                  <option value="8:00" >8:00 </option>
                  <option value="8:30" >8:30 </option>
                  <option value="9:00" >9:00 </option>
                  <option value="9:30" >9:30 </option>
                  <option value="10:00" >10:00 </option>
                  <option value="10:30" >10:30 </option>
                  <option value="11:00" >11:00 </option>
                  <option value="11:30" >11:30 </option>
                  <option value="12:00" >12:00 </option>
                  <option value="12:30" >12:30 </option>
                  <option value="13:00" >13:00 </option>
                  <option value="13:30" >13:30 </option>
                  <option value="14:00" >14:00 </option>
                  <option value="14:30" >14:30 </option>
                  <option value="15:00" >15:00 </option>
                  <option value="15:30" >15:30 </option>
                  <option value="16:00" >16:00 </option>
                  <option value="16:30" >16:30 </option>
                  <option value="17:00" >17:00 </option>
                  <option value="17:30" >17:30 </option>
                  <option value="18:00" >18:00 </option>
                  <option value="18:30" >18:30 </option>
                  <option value="19:00" >19:00 </option>
                  <option value="19:30" >19:30 </option>
                  <option value="20:00" >20:00 </option>
                  <option value="20:30" >20:30 </option>
                  <option value="21:00" >21:00 </option>
                  <option value="21:30" >21:30 </option>
                  <option value="22:00" >22:00 </option>
                  <option value="22:30" >22:30 </option>
                  <option value="23:00" >23:00</option>
                  <option value="23:30" >23:30 </option>
                 </select>
                    </div>
                   
                  </div>
                </form>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                            Close
                </button>
                <button type="button" class="btn btn-primary save_event">
                    Save
                </button>
            </div>
        </div>
    </div>
</div>


<!--  -->
</body>
<style>
.alert-success {
 width:450px !important;
 margin-top:450px;
  }
  .alert-success {
    background-color: #337ab7 !important;
  }
  #containerIntro input,
#containerIntro input,h3 {
    display: inline;
    vertical-align: top;
    font-family: 'Open Sans', sans-serif;
    font-size: 16px;
    line-height: 28px;
    color:black;
    border: hidden;
    width:180px;
      outline: none;
}
#containerIntro1 select option {
  margin-bottom:10px;
  color:black;
}
#extra
{
   border: none;
   width:15px;
}
#time1
{
  margin-left:50px;
}
.modal-header
 {
     padding:9px 15px;
     border-bottom:1px solid #eee;
     background-color: #337ab7;
 }
 body .fc {
    border: 5px solid #337ab7 !important;
}

</style>
</html>