<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                     <div class="form-group">
                                <label class="col-lg-2 control-label" for="provider name"></label>
                                <div class="col-lg-3">
                                    <h2><u>Notice</u></h2>
                                </div>
                            </div>                             
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <form action="" name="prov_create_form">
                            <?php if($this->session->userdata('role_id') == SOCIETY_SUPERUSER || $this->session->userdata('role_id') == SOCIETY_ADMIN){?>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" for="commission from">
                                <b class="float-right">Sender List</b></label>
                                <div class="col-lg-6">
                                   <i class="bt-lg glyphicon glyphicon-eye-open" data-toggle="modal" data-target="#myModal" style="border-radius: 30%;color: #31b0d5;cursor: pointer;"></i>
                                   <i glyphicon-eye-open" data-toggle="modal" data-target="#myModal" style="border-radius: 30%;color: #31b0d5"></i>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <?php }?>
                            <div class="form-group">
                                <label class="col-lg-2 control-label float-right" for="provider name">Notice Title <b class="float-right">:</b></label>
                                <div class="col-lg-6">
                                   <!--  <input name="disc_name" type="text" id="disc_name" value="<?php echo $template_data->title?>" class="form-control"> -->
                                   <?php echo $template_data->title?>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>

                            <div class="form-group">

                                <label class="col-lg-2 control-label" for="provider name">Notice Subject<b class="float-right">:</b></label>
                                <div class="col-lg-6">
                                    <!-- <input name="disc_desc" type="text" id="disc_desc" value="<?php echo $template_data->subject?>" class="form-control"> -->
                                    <?php echo $template_data->subject?>
                                </div>
                            </div>
                            
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                           <div class="form-group">
                                <label class="col-lg-2 control-label" for="commission from">Notice Desciption
                                <b class="float-right">:</b></label>
                                <div class="col-lg-6">

                                   <!-- <textarea placeholder="" id="notice_body" value="<?php echo $template_data->notice_description?>" name="notice_body" class="form-control" required="" style="height: 200px"></textarea>  -->
                                   <?php echo $template_data->notice_description?>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                             <div class="form-group">
                                    <div class="col-lg-offset-3">
                                        <a class="btn btn-primary" href="<?php echo  base_url().'back/notices'?>" type="button">Back</a> 
                                    </div>
                                </div>
                           </form>
                    </div><!-- /.box-body -->
                <!--</div> /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
  <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
       <h4 class="modal-title" id="exampleModalLongTitle"><b>Notice view </b></h4>
        </div>
        <div class="modal-body notice_view">
          <p>
                <?php if($notice_details){?>
                 <?php foreach($notice_details as $key => $value){
                  ?>
                  <div class="col-md-6">
                      <?php echo ucfirst($value['full_name']); 
                  ?>  
                  </div>
                  <div class="col-md-6">
                      <?php echo $value['is_view'];?>
                  </div>
                  <?php echo "<br>";?>
                 <?php }?>
                <?php }?>
            </p>
        </div>
    </div>
  </div>
</div>
  <style type="text/css">
    .notice_view {
      width: 570px;
      height: 230px;
      overflow: scroll;
  }
  </style>
<script type="text/javascript">
       
</script>