<div class="row">
    <h3 style="padding-left: 15px;">Notice List</h3>
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <?php if($this->session->userdata('role_id') == SOCIETY_SUPERUSER || $this->session->userdata('role_id') == SOCIETY_ADMIN) {?> 
                <span style="float: right;margin-top: 10px;margin-right: 10px;"><button class="btn btn-primary add_btn">Send Notice</button></span>
                <?php }?>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="mail_display_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="20px">Sr.No</th>
                            <th>Id</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Title</th>
                            <th>Subject</th>
                            <th width="100px">View</th>
                        </tr>
                    </thead>
                    <tbody>                                           

                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>
<script type="text/javascript">
   
   $(document).off('click', '.view_btn').on('click', '.view_btn', function (e) 
    {
        e.preventDefault();
        var id = $(this).attr('ref');
        var detail = {};
        var div = "";
        var ajax_url = base_url+'back/notices/view_template_member';
        var form = '';

        detail['id'] = id;

        get_data(ajax_url, form, div, detail, function (response)
        {
            if (response.flag == '@#success#@')
            {
                template_data = response.template_data;
                notice_des = template_data.notice_description;
                $("#mail_template_form #id").val(template_data.id);
                $("#mail_template_form #title").val(template_data.title);
                $("#mail_template_form #subject").val(template_data.subject); 
                $("#mail_template_form #notice_body").html(notice_des).text();
                $('#view_notices').modal('show');
            }
            else
            {
                alert(response.msg);  
            }
        }, '', false);
    });
</script>



