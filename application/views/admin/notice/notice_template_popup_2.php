<div class="modal fade popup" id="add_edit_popup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php
            $attributes = array("method" => "POST", "id" => "mail_template_form", "name" => "mail_template_form");
            echo form_open(base_url().'back/notices/add_notice', $attributes);
            ?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="title">Notice Title:</label>
                    <input type="text" placeholder="Notice Title" name="notices_title" id="title" class="form-control" data-rule-required="true" data-rule-fullname="true" data-msg-required="Please enter title" required/>
                </div>
                 <div class="form-group">
                    <label for="title">Notice Subject:</label>
                    <input type="text" placeholder="Notice Subject" name="notices_subject" id="title" class="form-control" data-rule-required="true" data-rule-fullname="true" data-msg-required="Please enter title" required/>
                </div>
                <div class="form-group">
                    <label for="title">Notice To:</label>&nbsp;<!-- 
                    <label class="radio-inline">
                          <input type="radio" name="optradio" id="all_member">All
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="optradio" id="ind_member">Particular
                    </label> -->
                    <select class="form-control" id="notices_to" name="notices_to[]" required="" multiple="" >
                        <?php if($member_data) {?>
                             <?php foreach($member_data as $key => $value) {?>
                               <?php  echo "<option value='" . $value->id . "'> " . $value->first_name ." ".$value->last_name. " </option>"; ?>
                             <?php  } ?>
                        <?php }?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="class">Notice Body:</label>
                    <textarea placeholder="Body" id="mail_body" name="notice_body"  class="form-control" required=""></textarea>
                    <input name="id" type="hidden" id="id" class="form-control"/>
                </div>
            </div>
            <div class="modal-footer clearfix">

                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>Cancel</button>

                <button type="submit" id="submit_btn" class="btn btn-primary pull-left"><i class="fa fa-envelope"></i>Save</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>


<div class="modal fade popup" id="view_notices" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php
            $attributes = array("method" => "POST", "id" => "mail_template_form", "name" => "mail_template_form");
            echo form_open(base_url().'back/notices/add_notice', $attributes);
            ?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="title">Notice Title:</label>
                    <input type="text" placeholder="Notice Title" name="notices_title" id="title" class="form-control" data-rule-required="true" data-rule-fullname="true" data-msg-required="Please enter title" required/>
                </div>
                 <div class="form-group">
                    <label for="title">Notice Subject:</label>
                    <input type="text" placeholder="Notice Subject" name="notices_subject" id="title" class="form-control" data-rule-required="true" data-rule-fullname="true" data-msg-required="Please enter title" required/>
                </div>
                <div class="form-group">
                    <label for="class">Notice Body:</label>
                    <textarea placeholder="Body" id="notice_body" name="notice_body"  class="form-control" required="" style="height: 200px"></textarea>
                    <input name="id" type="hidden" id="id" class="form-control"/>
                </div>
            </div>
            <div class="modal-footer clearfix">

                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>Cancel</button>
            </div>
            <?php echo form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</script>
 <script type="text/javascript">
        $(document).ready(function() {
            $('#notices_to').multiselect({
            includeSelectAllOption: true,
            buttonWidth: 250,
            enableFiltering: true
        });
        });
    </script>
