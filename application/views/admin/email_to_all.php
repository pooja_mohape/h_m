
<div class="" id="add_edit_popup" tabindex="-1" role="" aria-hidden="false">
   
        <div class="modal-content">
            <?php
            $attributes = array("method" => "POST", "id" => "mail_template_form", "name" => "mail_template_form");
            echo form_open(base_url().'back/email/send_mail_all', $attributes);
            ?>
            <center><h3 style="margin-top:10px;">INSTANT MAIL REMAINDER</h3></center>
            <hr>
            <div class="modal-body">
               <div class="form-group">
                    <label for="title">To:</label>
                     <select class="form-control chosen" name="to[]" id="to" required="members" multiple="">
                       <?php if($user_list){?>

                        <?php foreach($user_list as $key => $value){?>
                        <option value="<?php echo $value->id ?>"><?php echo $value->name;?></>
                        <?php }?>
                       <?php }?>
                    </select>
                </div>
                 <div class="form-group">
                    <label for="title">Subject</label>&nbsp;
                    <input type="text" placeholder="subject" name="subject" id="title" class="form-control" data-rule-required="true" data-rule-fullname="true" data-msg-required="Please enter title" required/>
                </div>
                <div class="form-group">
                    <label for="class">Mail:</label>
                    <textarea placeholder="Body" id="mail_body" name="mail"  class="form-control" required=""></textarea>
                </div>
            </div>
            <div class="modal-footer clearfix">

                 <a href="<?php echo base_url().'back/bill_payment_report/outstanding_report'?>">
                     <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>Cancel</button>
                </a>

                <button type="submit" id="submit_btn" class="btn btn-primary pull-left"><i class="fa fa-send-o"></i>Send</button>
            </div>
            <?php echo form_close();?>
        </div>
    
</div>
<script src="<?php echo  base_url()?>assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript"> 
$(document).ready(function() {  
     $('#to').multiselect({
            includeSelectAllOption: true,
            buttonWidth: 250,
            enableFiltering: true
        });

    var myckeditor = CKEDITOR.replace('mail_body',{
        enterMode : CKEDITOR.ENTER_BR,
        entities : false,
        basicEntities : false,
        entities_greek : false,
        entities_latin : false,                             
        htmlDecodeOutput : false
   });

     myckeditor.on('key', function(evt){
        $('#mail_template_form').valid();
    });
});      
</script>
