<?php 
if($this->session->userdata('role_id') == SOCIETY_MEMBER)
{
 $style = "display: none";
}
else
{
 $style = "display: block;";
}
?>

<section class="content">
    <div id="mail_template_wrapper">
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <?php if($hide_button == false && $month){
              
              ?>
            <div class="row">
              <?php 
                if($this->session->userdata('role_id') ==SOCIETY_SUPERUSER){?>
                    <center>
                      <div style="margin:3em;">
                          <button type="button" class="btn btn-success btn-lg " id="generate_bill_btn" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing Order" style="border-radius: 10%">Generate Bill <?php echo date('M',strtotime($month)).'-'.date("Y", strtotime($month))?></button>
                      </div>
                   </center>
            <?php }?>
            </div>
            <?php }?>
            <div class="row">
              <h3 class="box-title" style="margin-left:25px;">Monthly Bill Payment List</h3>
            </div>
            <div class="box-header">
                 <div class="row">
                    
                           <div class="col-md-2" style="<?php echo $style?>">   
                                <div style="position: static;" class="form-group">
                                    <label for="select-1">User List</label>
                                    <select class="form-control chosen" id="user_id" name="user_id">
                                    <option value="">All</option>
                                   <?php if($userdata){?>
                                    <?php foreach($userdata as $key => $value){?>
                                      <option value="<?php echo $value->id ?>"><?php echo $value->first_name ." ".$value->last_name?></option>
                                    <?php }?>
                                   <?php }?>  
                                    </select>
                                </div>
                            </div>
                           <div class="col-md-2">
                                <div style="position: static;" class="form-group">
                                    <label for="select-1">Select Month</label>
                                     <input type="text" class="form-control transaction_date" name="transaction_date" id="transaction_date" value="" required="">
                                </div>
                           </div>
                           
                             <div class="col-md-2">
                                    <div style="position: static;" class="form-group">
                                        <label for="input-text-2" style="visibility: hidden;">test</label>
                                        <button type="submit" class="form-control btn btn-primary" id="generate_btn">Submit</button>
                                    </div>  
                            </div>
                            
                    </div>
                                                                           
               
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="mail_display_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="20px">Sr.No</th>
                            <th>Id</th>
                            <th>Month</th>
                            <th>Full Name</th>
                            <th>Maintenance Charge </th>
                            <th>Parking Charge</th>
                            <th>Facility Charge</th>
                            <th>Other Charge</th>
                            <th>Total Amount</th>
                            <th>Current Outstanding</th>
                            <th>Invoice</th>
                        </tr>
                    </thead>
                    <tbody>                                           

                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>
</section>
<script type="text/javascript">
</script>
<script>
  $( function() {
    $(document).ready(function(){

    $(".transaction_date").datepicker({ 
        dateFormat: 'mm-yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,

        onClose: function(dateText, inst) {  
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val(); 
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val(); 
            $(this).datepicker('setDate', new Date(year, month, 1)); 
        }
    });
    
    $(".transaction_date").focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
              my: "center top",
              at: "center bottom",
              of: $(this)
            });
        
    });
    
});
  } );
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tconfig = {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "back/bill_payment_report/get_bill_data",
            "type": "POST",
            "data": "json",
            data   : function (d) {
                        d.user_id    =  $("#user_id").val();
                        d.transaction_date = $("#transaction_date").val();
                     }
        },
        "iDisplayLength": 10,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        //        "paginate": true,
        "paging": true, 
        "searching": true,
        "order": [[1, "desc"]],
        "aoColumnDefs": [
                    {
                        "targets": [1],
                        "visible": false,
                        "searchable": false

                    }
                ],

         "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();  
            $("td:first", nRow).html(info.start + iDisplayIndex + 1);  
            if (aData[8] == 'Approved' || aData[8] == 'Rejected')
            {
                $("td:eq(8)", nRow).html('<a href='+base_url+'back/bill_payment/bill_payment_view/'+aData[1]+' class="views_wallet_trans margin"><i class="glyphicon glyphicon-eye-open"></i></a>');
            }       
            return nRow;
        },
    };

    var oTable = $('#mail_display_table').dataTable(tconfig); 
    $(document).off('click', '#generate_btn').on('click', '#generate_btn', function (e) { 
       oTable.fnDraw();     
     }); 
     $(document).off('click', '#generate_bill_btn').on('click', '#generate_bill_btn', function (e) { 
         $("#generate_bill_btn").html('Processing');
         $("#generate_bill_btn").attr('disabled',true);
          $.ajax({
          url: '<?php echo base_url();?>back/cron/generate_monthly_bill/'+'<?php echo $month?>',
          success:function(data)
          {
              location.reload();
          }
        }); 
     }); 

});
</script>

