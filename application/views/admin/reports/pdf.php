<?php

// show($society,1);

foreach ($society as $key => $value){
$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf->SetTitle('My Invoice');
$pdf->SetHeaderMargin(30);
$pdf->SetTopMargin(20);
$pdf->setFooterMargin(20);
$pdf->SetAutoPageBreak(true);
$pdf->SetAuthor('Author');
$pdf->SetDisplayMode('real', 'default');

$pdf->AddPage();

//$pdf->SetFont('helvetica', 'B', 7);
$pdf->SetFont('helvetica', '', 7);

$pdf->SetXY(90, 30);
//$pdf->image('style'='display:block; border:none; outline:none; text-decoration:none;')
$pdf->Image('https://hamarisociety.co.in/dashboard/assets/images/hs_4-01-for_site_1024.png', '', '', 30, 20, '', '', 'T', false, 300, '', false, false, 1, false, false, false);
$html =  '< style="display:block;border:none; outline:none; text-decoration:none;width:300;height:200;border:0></style>';
$pdf->writeHTML($html, true, false, true, false, '');


 $pdf->SetXY(40, 60);
 $pdf->Image('upload/dimension.png','', '', 120, 12, '', '', 'T', false, 500, '', false, false, 0, false, false, false);

$pdf->SetXY(90, 65);
$html =  '<h1 style="
    color: #5D6975;
    font-size: 2.4em;
    line-height: 1.4em;
    font-weight: normal;
    text-align: right;
    margin: 0 0 20px 0;">INVOICE</h1>';
$pdf->writeHTML($html, true, false, true, false, '');
// $html .= '<style>'.file_get_contents(base_url().'assets/global/css/stylesheet.css').'</style>';


$pdf->SetXY(100, 75);
$html='

    <div class="invoice-box" style="width: 100%;
      
       padding: 50px;
       border: 1px solid #eee;
       box-shadow: 0 0 10px rgba(0, 0, 0, .15);
       font-size: 10px;
       line-height: 24px;
       color: #555;">
       <table style="margin-top:80px;width:100%;">
          
            <br><br>
           <tr class="information">
               
                           <td style="width:50%;text-align:left;">
                               Name: '.$value->full_name.'<br>
                                '.$value->block.','.$value->wing.','.$value->building.'
                                
                           </td>
                           
                           <td style="width:50%;text-align:right;">
                              
                               Generated at: '.$value->created_at.'<br>

                               Month: '.date('M-Y', strtotime($value->created_at)).'
                           </td>
                       
           </tr>
           <br>
           <tr class="heading" style="background-color: #eee;
       border-bottom: 1px solid #ddd;
       font-weight: bold;">
               <td style="text-align:left">
                   Description
               </td>
               
               <td>
                   Price
               </td>
           </tr>
           
           <tr class="item">
               <td style="text-align:left">
                   Maintance Charges
               </td>
               
               <td >
                   '.$value->maintenance_amt.'
               </td>
           </tr>
           
           <tr class="item" >
               <td style="text-align:left">
                   Parking Charges
               </td>
               
               <td >
                   '.$value->parking_amt.'
               </td>
           </tr>
           
           <tr class="item last">
               <td style="text-align:left">
                   Facility Charges
               </td>
               
               <td >
                  '.$value->facility_charges.'
               </td>
           </tr>
           <tr class="item last">
               <td style="text-align:left">
                   other Charges
               </td>
               
               <td>
                   '.$value->other_charges.'
               </td>
           </tr>
           <hr>
           <tr class="total" style=" background-color:#eee;font-weight:bold">
               <td style="text-align:left;">Total Amount</td>

               
               <td>
                  '.$value->total_amt.'
               </td>
           </tr><br>
            <tr class="total" style=" background-color:#FFFFFF;font-weight:bold">
               <td style="text-align:left;">Remaining Outstanding</td>
               
               
               <td>
                  '.$value->outstanding_amt_before.'
               </td>
           </tr><br>
           <tr class="total" style=" background-color:#eee;font-weight:bold">
               <td style="text-align:left;">Net Payable Amont</td>
               
               
               <td>
                  '.$value->net_payable_amt.'
               </td>
           </tr>
       </table>
   </div>
     ';
//$pdf->Write(5, 'Some sample text');
$pdf->writeHTML($html, true, false, true, false, 'C');
$pdf->Output('Invoice.pdf', 'I');
exit();
}
?>