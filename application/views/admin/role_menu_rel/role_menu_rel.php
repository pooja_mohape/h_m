<section class="content-header">
    <h1> Dashboard <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Menu Permission</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div id="role_wrapper">
        
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Menu Permission</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <!-- body content start -->
                         <?php
                            if (isset($role_id)) {
                                echo "<div class='row'>";
                                echo "<div class='pull-right' style='margin-right: 31px'>";
                                echo "<b> Select All &nbsp;</b>";
                                echo "<input id='select_all_chk' class='select_all_chk' type='checkbox' name='select_all_chk'/>"; 
                                echo "</div>";
                                echo "</div>";
                            }
                             ?>      
                        <div class="row">
                            <div class="col-xs-12 bhoechie-tab-container">
                                <div class="col-xs-2 bhoechie-tab-menu">
                                    <div class="list-group">
                                        <a href="#" class="list-group-item roles_direction text-center">
                                            <h4 class="glyphicon">Roles</h4><br/>
                                        </a>
                                    </div>
                                </div>
                                <div class="permission_type col-xs-10">
                                    <ul class="list-inline">
                                        <li style="width:45%;"> Menu Name</li>
                                        <li style="width: 7%">View</li>
                                        <li style="width: 7%">Add</li>
                                        <li style="width: 7%">Edit</li>
                                        <li style="width: 9%">Delete</li>
                                        <li style="width: 7%">Export</li>
                                        <li style="width: 7%">Pdf</li> 
                                        <li style="width: 8%">Approve</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 bhoechie-tab-container">
                                <div class="col-lg-2 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                                    <div><!--<div class="list-group">-->
                                        <?php
                                        foreach ($rolename as $role_name) {
                                            if (strtolower($role_name->role_name) != "super admin") {
                                                if($role_name->id == $role_id)
                                                 echo "<a href='" . base_url() . "admin/role_menu_rel/get_role_permissions/" . $role_name->id . "' class='list-group-item rolename active'>";
                                                 else                                               
                                                 echo "<a href='" . base_url() . "admin/role_menu_rel/get_role_permissions/" . $role_name->id . "' class='list-group-item rolename'>";
                                                 echo $role_name->role_name;
                                                 echo "</a>";
                                            }
                                        }
                                        ?> 
                                    </div>
                                </div>
                                <div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                                    <div class="bhoechie-tab-content" id="role_menu_rel_permission" style='background:none repeat scroll 0 0 #f5f5f5'>
                                        <?php
                                        if (isset($role_id)) {
                                            $pmenu = array('items' => array(), 'parents' => array());
                                            // Builds the array lists with data from the menu table
                                            foreach ($menu_data as $mkey => $mvalue) {
                                                // Creates entry into items array with current menu item id ie. $menu['items'][1]
                                                $pmenu['items'][$mvalue['id']] = $mvalue;
                                                $pmenu['parents'][$mvalue['parent']][] = $mvalue['id'];
                                            }
                                        
                                            $permission = array();
                                            if ($permission_array != "") {
                                                foreach ($permission_array as $pkey => $pvalue) {
                                                    $permission[$pvalue["id"]] = $pvalue;
                                                }
                                            } else {
                                                $permission = "";
                                            }

                                            echo "<ul>";
                                            echo "<li>";
                                            echo get_child_menus(0, $pmenu, $permission);
                                            echo "</li>";
                                            echo "</ul>";
                                        }
                                        else
                                        {
                                            $role_id = "";
                                        }
                                        ?>
                                        <!-- Menu permission main content end -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- body content end -->
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        
    </div>
</section>

<script>                        
    $(document).ready(function(){                        
        
        $(document).off('click', 'div.bhoechie-tab-menu>div.list-group>a').on('click', 'div.bhoechie-tab-menu>div.list-group>a', function (e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
        });
        
        $('.parent-check').on('ifChecked ifUnchecked', function(e){
            
            var type            = $(this).attr("name");
            var menu_data_attr  = $(this).closest("li").attr( "data-category" );

            if(type == "view")
            {
                if(e.type == 'ifChecked')
                {
                    $("li[parent-category=" + menu_data_attr + "]").show(1500);
                    $(".sub_menu_"+type+"_"+menu_data_attr).closest("div").find('input').attr('checked',true);
                    
                }
                else if(e.type == 'ifUnchecked')
                {
                    $("li[data-category=" + menu_data_attr + "]").find('ul li').hide(1500);      
                    $(this).closest( "li" ).find('input').attr('checked', false)
                }
                else
                {
                    $("li[data-category=" + menu_data_attr + "]").find('ul li').hide(1500);
                }
            }//end of view event
            
            var checkboxvalue = "N";
            if(e.type == 'ifChecked')
            {
                checkboxvalue = "Y";
            }
            else if(e.type == 'ifUnchecked')
            {
                checkboxvalue = "N";      
            }
            
            // update menu permission
            $.post("<?php echo base_url(); ?>admin/role_menu_rel/set_role_permissions",{type: type, role:<?php echo $role_id; ?>, menu: menu_data_attr, checkboxvalue:checkboxvalue},function(response, status){
                if(status == "success")
                {
                    showLoader(response.msg_type,response.msg);
                }
            });

            $('input[type="checkbox"]').iCheck('update');
        });
        
});// end of document ready
</script>
<script> 
        $(".show_sub").click(function() {
          var menu_data_attr  = $(this).closest("li").attr("data-category" );  
             $("li[parent-category=" + menu_data_attr + "]").slideToggle( "slow", function() 
           {
                 
           });
         });
     $('.select_all_chk').on('ifChecked ifUnchecked', function(e){
      
        var isChecked = $(this).is(':checked');
        if (isChecked)
        {
            $('.icheckbox_minimal').addClass('checked');
            $.post("<?php echo base_url(); ?>admin/role_menu_rel/select_all_menu",{role:<?php echo $role_id; ?>},function(response, status){
                if(status == "success")
                {
                    showLoader(response.msg_type,response.msg);
                }
              });
         }
        else
            {
                $('.icheckbox_minimal').removeClass('checked');
                 $.post("<?php echo base_url(); ?>admin/role_menu_rel/remove_all_permission",{role:<?php echo $role_id; ?>},function(response, status){
                    if(status == "success")
                    {
                        showLoader(response.msg_type,response.msg);
                    }
                });
            }

 });
      $('.all_per').on('ifChecked ifUnchecked', function(e){ 
            var type            = $(this).attr("name");
            var menu_data_attr  = $(this).closest("li").attr("data-category" );  
                $("li[parent-category=" + menu_data_attr + "]").slideToggle( "slow", function() 
               {
                     
               });
             var isChecked = $(this).is(':checked');
             if (isChecked)
             {
               $(".sub_menu_"+type+"_"+menu_data_attr).closest("div").find('input').parent().addClass('checked');
               $(".sub_menu_"+"add"+"_"+menu_data_attr).closest("div").find('input').parent().addClass('checked');
               $(".sub_menu_"+"edit"+"_"+menu_data_attr).closest("div").find('input').parent().addClass('checked');
               $(".sub_menu_"+"delete"+"_"+menu_data_attr).closest("div").find('input').parent().addClass('checked');
               $(".sub_menu_"+"export"+"_"+menu_data_attr).closest("div").find('input').parent().addClass('checked');
               $(".sub_menu_"+"pdf"+"_"+menu_data_attr).closest("div").find('input').parent().addClass('checked');
               
               $(".menu_"+"view"+"_"+menu_data_attr).closest("div").find('input').parent().addClass('checked');
               $(".menu_"+"add"+"_"+menu_data_attr).closest("div").find('input').parent().addClass('checked');
               $(".menu_"+"edit"+"_"+menu_data_attr).closest("div").find('input').parent().addClass('checked');
               $(".menu_"+"delete"+"_"+menu_data_attr).closest("div").find('input').parent().addClass('checked');
               $(".menu_"+"export"+"_"+menu_data_attr).closest("div").find('input').parent().addClass('checked');
               $(".menu_"+"pdf"+"_"+menu_data_attr).closest("div").find('input').parent().addClass('checked');

               $.post("<?php echo base_url(); ?>admin/role_menu_rel/all_per_per_menu",{role:<?php echo $role_id; ?>, menu: menu_data_attr},function(response, status){
                if(status == "success")
                {
                    showLoader(response.msg_type,response.msg);
                }
              });       
             }
             else
             {
               $(".sub_menu_"+type+"_"+menu_data_attr).closest("div").find('input').parent().removeClass('checked');
               $(".sub_menu_"+"add"+"_"+menu_data_attr).closest("div").find('input').parent().removeClass('checked');
               $(".sub_menu_"+"edit"+"_"+menu_data_attr).closest("div").find('input').parent().removeClass('checked');
               $(".sub_menu_"+"delete"+"_"+menu_data_attr).closest("div").find('input').parent().removeClass('checked');
               $(".sub_menu_"+"export"+"_"+menu_data_attr).closest("div").find('input').parent().removeClass('checked');
               $(".sub_menu_"+"pdf"+"_"+menu_data_attr).closest("div").find('input').parent().removeClass('checked');
               
               $(".menu_"+"view"+"_"+menu_data_attr).closest("div").find('input').parent().removeClass('checked');
               $(".menu_"+"add"+"_"+menu_data_attr).closest("div").find('input').parent().removeClass('checked');
               $(".menu_"+"edit"+"_"+menu_data_attr).closest("div").find('input').parent().removeClass('checked');
               $(".menu_"+"delete"+"_"+menu_data_attr).closest("div").find('input').parent().removeClass('checked');
               $(".menu_"+"export"+"_"+menu_data_attr).closest("div").find('input').parent().removeClass('checked');
               $(".menu_"+"pdf"+"_"+menu_data_attr).closest("div").find('input').parent().removeClass('checked');

               $.post("<?php echo base_url(); ?>admin/role_menu_rel/remove_per_per_menu",{role:<?php echo $role_id;?>, menu: menu_data_attr},function(response, status){
                if(status == "success")
                {
                    showLoader(response.msg_type,response.msg);
                }
              });    
             }   
 });    
 </script> 