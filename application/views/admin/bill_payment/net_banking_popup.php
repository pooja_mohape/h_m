<div class="modal fade popup" id="net_banking_popup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php
            $attributes = array("method" => "POST", "id" => "net_banking_form", "name" => "net_banking_form");
            echo form_open(base_url().'back/parking/add_parking', $attributes);
            ?>
            <div class="modal-body">
                 <div class="form-group">
                    <label for="title">Users:</label>
                    <div class="form-group">
                    <label for="title">Notice Title:</label>
                    <input type="text" placeholder="Notice Title" name="notices_title" id="title" class="form-control" data-rule-required="true" data-rule-fullname="true" data-msg-required="Please enter title" required/>
                </div>
                </div>
                 <div class="form-group">
                    <label for="title">Parking Slot:</label>
                     <select class="form-control" name="slot_no" required="slot_no">
                      <option value="">Parking Slot</option>
                       <?php if($park_slot){?>
                        <?php foreach($park_slot as $key => $value){?>
                        <option value="<?php echo $value->id;?>"><?php echo $value->slot_no;?></option>
                        <?php }?>
                       <?php }?>
                    </select>
                </div>
               
            </div>
            <div class="modal-footer clearfix">

                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>Cancel</button>

                <button type="submit" id="submit_btn" class="btn btn-primary pull-left"><i class="fa fa-envelope"></i>Save</button>
            </div>
            <?php echo form_close();?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    $(function() {
        
        CKEDITOR.replace('mail_body_class',{
            enterMode : CKEDITOR.ENTER_BR,
            entities : false,
            basicEntities : false,
            entities_greek : false,
            entities_latin : false, 
            htmlDecodeOutput : false
        });
        
    });
</script>
