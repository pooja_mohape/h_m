<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="row">
              <h3 class="box-title" style="margin-left:25px;">Payment List</h3>
            </div>
            <div class="box-header">
                 <div class="row">
                            <div class="col-md-2">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-1">From Date</label>
                                    <input class="form-control" id="from_date" name="from_date" placeholder="From Date" type="text" value="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2">Till Date</label>
                                    <input class="form-control " id="till_date" name="till_date" placeholder="Till Date" type="text" value=""/>
                                </div>
                            </div>
                            <div class="col-md-2">    
                                <div style="position: static;" class="form-group">
                                    <label for="select-1">Transaction status</label>
                                    <select class="form-control chosen" id="trans_type" name="trans_type">
                                    <option value="">All</option>
                                    <option value="pending" selected>Pending</option>
                                    <option value="approved">Approved</option>
                                    <option value="rejected">Rejected</option>
                                    </select>

                                </div>
                            </div>
                             <div class="col-md-2">
                                    <div style="position: static;" class="form-group">
                                        <label for="input-text-2" style="visibility: hidden;">test</label>
                                        <button type="submit" class="form-control btn btn-primary" id="generate_btn">Submit</button>
                                    </div>  
                             </div>
                            
                            <div class="col-md-2"></div>
                            <div class="col-md-3" style="float: right">
                                 <?php if($this->session->userdata('role_id') == SOCIETY_MEMBER || $this->session->userdata('role_id') == SOCIETY_ADMIN){?>
                <a href="<?php echo base_url().'back/bill_payment/make_payment'?>">
                <span style="float: right;margin-top: 10px;margin-right: 10px;"><button class="btn btn-primary add_btn">Make Payment Request</button></span></a>
                <?php }?>
                            </div>
                    </div>
                                                                           
               
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="mail_display_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="20px">Sr.No</th>
                            <th>Id</th>
                            <th>userid</th>
                            <th>Full Name</th>
                            <th>Amount</th>
                            <th>Requested_date</th>
                            <th>Transaction type</th>
                            <th>Payment_date</th>
                            <th>Status</th>
                            <th>Is_approved</th>
                            <th width="100px">View</th>
                        </tr>
                    </thead>
                    <tbody>                                           

                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

<script type="text/javascript">
   
   $(document).off('click', '.view_btn').on('click', '.view_btn', function (e) 
    {
        e.preventDefault();
        var id = $(this).attr('ref');
        var detail = {};
        var div = "";
        var ajax_url = base_url+'back/notices/view_template_member';
        var form = '';

        detail['id'] = id;
        get_data(ajax_url, form, div, detail, function (response)
        {
            if (response.flag == '@#success#@')
            {
                template_data = response.template_data;
                notice_des = template_data.notice_description;
                $("#mail_template_form #id").val(template_data.id);
                $("#mail_template_form #title").val(template_data.title);
                $("#mail_template_form #subject").val(template_data.subject); 
                $("#mail_template_form #notice_body").html(notice_des).text();
                $('#view_notices').modal('show');
            }
            else
            {
                alert(response.msg);  
            }
        }, '', false);
    });
</script>
<script>
  /*$( function() {
       $( "#from_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
       $( "#till_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
  } );*/
   $(document).ready(function(){
    $("#from_date").datepicker({
        numberOfMonths: 1,
        onSelect: function(selected) {
          $("#till_date").datepicker("option","minDate", selected)
        }
    });
    $("#till_date").datepicker({ 
        numberOfMonths: 1,
        onSelect: function(selected) {
           $("#from_date").datepicker("option","maxDate", selected)
        }
    });  
     $("#from_date").keydown(function(e) { 
        if(e.keyCode == 8)
        {
          return true;
        }else{
          return false;
        }
    });
    $("#till_date").keydown(function(e) { 
        if(e.keyCode == 8)
        {
          return true;
        }else{
          return false;
        }
    });
});
</script>



