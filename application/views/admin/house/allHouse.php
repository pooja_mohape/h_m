<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="col-md-8">
           <h3 style="">View House Details</h3>
        </div>
         <div class="col-md-4">
	  	<?php $ses_id = $this->session->userdata('role_id'); if($ses_id==SOCIETY_SUPERUSER || $ses_id==SOCIETY_ADMIN){?>
	  	 <a href="<?php echo base_url()?>back/registration/houseView" class="btn btn-primary">Add House</a>
	  	 <?php }?>&nbsp;&nbsp;<a href="javascript:window.history.go(-1);" class="btn btn-danger"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</a>
        </div>
 </div>
      <div class="row">
 			<div class="col-md-12">
 				 <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="box box-primary" id="box-primary">
          <div class="box-body table-responsive">
 				<table id="allhouse" class="table table-condensed table-hover table-responsive">
 					<thead>
 						<tr>
 							<th>Sr. No</th>
 							<th>Society Name</th>
 							<th>Building</th>
 							<th>Wing</th>
 							<th>Block No</th>
 							<th>House Type</th>
 							<th>Created Date</th>
 							<th>Action</th>
 						</tr>
 					</thead>
 					<tbody>
 						<?php $count ='0'; foreach($house as $house){
 							$id = $house->id;
 							$count++;?>
 						<tr>
 							<td><?= $count; ?></td>
 							<td><?= $house->society_name;?></td>
 							<td><?= $house->building;?></td>
 							<td><?= $house->wing;?></td>
 							<td><?= $house->block;?></td>
 							<td><?= $house->house_type;?></td>
 							<td><?= $house->created_date;?></td>
 							<?php $role_id = $this->session->userdata('role_id'); if($role_id==SUPERADMIN){ ?>
 							<td><a href="<?= base_url().'back/registration/showhouse/'.$id; ?>" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="bottom" title="View" id="show"><i class="fa fa-eye"></i></a> | <a href= "<?= base_url().'back/registration/deletehouse/'.$id; ?>" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete" id="delete"><i class="fa fa-trash"></i></a></td>
 								<?php } else if($role_id==SOCIETY_SUPERUSER){?>
                  <td><a href="<?= base_url().'back/registration/edithouse/'.$id; ?>" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit" id="edit"><i class="fa fa-pencil"></i></a> | <a href="<?= base_url().'back/registration/showhouse/'.$id; ?>" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="bottom" title="View" id="show"><i class="fa fa-eye"></i></a> | <a href= "<?= base_url().'back/registration/deletehouse/'.$id; ?>" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete" id="delete"><i class="fa fa-trash"></i></a></td>
                <?php } else {?>
 								<td><a href="<?= base_url().'back/registration/showhouse/'.$id; ?>" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="bottom" title="View" id="show"><i class="fa fa-eye"></i></a></td>
 								<?php } ?>
 						</tr>
 						<?php } ?>
 					</tbody>
 				</table>
 			</div>
 			   </div>
 			</div>
      </div>
 </section>
 <script type="text/javascript">
 	$(document).ready(function(){
 		$('#allhouse').DataTable();
 		 $('#delete').click(function() {
 		var choice = confirm('Are You Sure, To Delete this House');
 		if(choice!=true){
 			return false;
 		}
 	});
 	});
 </script>