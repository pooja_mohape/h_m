<section class="content">
      <div class="row">
        <!-- left column -->
      <form role="form" method="post" action="<?= base_url();?>back/registration/updateHouse" enctype="multipart/form-data" id="house_form">
         <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit House</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              <div class="box-body">
                <?php $role = $this->session->userdata('role_id'); if($role == SUPERADMIN){?>
               <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="society_name">Society Name *</label>
                  <div class="col-sm-5">
                    <select name="society_name" class="form-control" id="society_name">
                      <option value="<?= $house[0]->society_id; ?>"><?= $house[0]->society_name; ?></option>
                    </select>
                  </div>
                </div>
                <?php } ?>
                 <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="building">Building Name Or No.</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="building" id="building" value="<?= $house[0]->building ?>">
                  </div>
                </div>
                 <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="block">Society Wing</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="wing" id="block" value="<?= $house[0]->wing ?>">
                    <p class="help-block">For example: A</p>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="block">Block / Flat Number *</label>
                    <div class="col-sm-5">
                    <input type="text" class="form-control" name="block" id="block" value="<?= $house[0]->block ?>">
                    <input type="hidden" name="hid" value="<?= $house[0]->id; ?>" />
                  </div>
                </div>
               <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="house">House Type *</label>
                    <div class="col-sm-5">
                      <select name="house_type" class="form-control" id="city">
              <option value="<?= $house[0]->shc_id; ?>"><?= $house[0]->house_type; ?></option>
                        <?php if($houseType){
                         foreach($houseType as $h_type){
                            if($house[0]->shc_id==$h_type->hid){} else {?>
                            <option value="<?= $h_type->hid;?>"><?= $h_type->house_type; ?></option>
                        <?php }}} ?>
                      </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="on_lease">House On Lease *</label>
                    <div class="col-sm-5">
                      Yes&nbsp;<input type="radio" value="Y" name="on_lease" id="yes">&nbsp;&nbsp;
                      No&nbsp;<input type="radio" value="N" name="on_lease" id="no">
                    </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="house">Details</label>
                  <div class="col-sm-5">
                    <input type="text" name="details" class="form-control" value="<?= $house[0]->details;?>" />
                  </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="profile_pic">House Image</label>
                    <div class="col-sm-5">
                    <input type="file" name="house_pic" id="house_pic" value="<?= $house[0]->house_picture; ?>" />
                    <img src="<?= base_url().'upload/houseImages/'.$house[0]->house_picture; ?>" width="150px" alt="House Image"/>
                    <input type="hidden" name="house_img" value="<?= $house[0]->house_picture;?>">
                    <p class="help-block">Your House Picture.(Only png,jpg,jpeg and gif allowed)</p>
                  </div>
               </div>
               <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="submit"></label>
                    <div class="col-sm-5">
                        <button type="submit" class="btn btn-primary">Update House</button>&nbsp;&nbsp;&nbsp;
                        <a href="<?= base_url()?>back/registration/allhouse" class="btn btn-danger"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</a>
                      </div>
                  </div>
              </div>
            </div>
          </div>
      </form>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function(){
    if("<?= $house[0]->on_lease?>"=='N') {
      $('#no').attr('checked','checked');
    }
    else {
      $('#yes').attr('checked','checked');
    }
  });
</script>