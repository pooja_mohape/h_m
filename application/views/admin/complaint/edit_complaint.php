<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                     <div class="form-group">
                                <label class="col-lg-3 control-label" for="provider name"></label>
                                <div class="col-lg-6">
                                    <h2>Complaint</h2>
                                </div>
                            </div>                             
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <form action ="<?php echo base_url();?>back/complaint/insert_complaint" id="prov_create_form" method="post" name="prov_create_form">
                            <div class="clearfix" style="height: 10px;clear: both;"></div>

                            <div class="form-group">

                                <label class="col-lg-3 control-label" for="provider name">Complaint Subject<b float="right">:</b></label>
                                <div class="col-lg-6">
                                   <!--  <input name="disc_desc" type="text" id="disc_desc" value="<?php echo $template_data->complaint_type?>" class="form-control"> -->
                                    <?php echo $template_data->complaint_type?>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                           <div class="form-group">
                                <label class="col-lg-3 control-label" for="commission from">Complaint discription
                                <b float="right">:</b></label>
                                <div class="col-lg-6">

                                      <!-- textarea placeholder="" id="notice_body" value="<?php echo $template_data->complaint_details?>" name="notice_body" class="form-control" required="" style="height: 50px"></textarea>  -->
                                   <!--  <input name="disc_desc" type="text" id="disc_desc" value="<?php echo $template_data->complaint_details?>" class="form-control"> -->

                                  <?php echo $template_data->complaint_details?>
                                  <input type="hidden" name="id" value="<?php echo $template_data->id?>">
                                </div>
                            </div>
                              <div class="clearfix" style="height: 10px;clear: both;"></div>
                           <div class="form-group">
                                <label class="col-lg-3 control-label" for="commission from">Complaint Remark
                                <b float="right">:</b></label>
                                <div class="col-lg-6">

                                      <textarea placeholder="" id="notice_body" name="complaint_remark" class="form-control" required="" style="height: 78px; width: 299px;"></textarea> 
                                </div>
                            </div>

                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="commission from"></label>
                                <div class="col-lg-6">
                                      <button type="submit" id="submit_btn" class="btn btn-primary pull-left">Save</button>
                                      &nbsp;&nbsp;
                                      <a class="btn btn-default btn-danger" href="<?php echo  base_url().'back/complaint'?>" type="button">Back</a> 
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                         </form>
                    </div><!-- /.box-body -->
                <!--</div> /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
