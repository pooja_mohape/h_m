<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends My_Controller {

		public function __construct() {
        parent::__construct();
        is_logged_in();
        $this->load->model(array('users_model','user_role_model','society_master_model'));
        }
    public function userReportView(){
        $ses_role = $this->session->userdata('role_id');
        $ses_scid = $this->session->userdata('society_id');
        if($ses_role ==SUPERADMIN) {
             
             $data['society'] = $this->society_master_model->find_all();
        } else {
            $data['society'] = $this->society_master_model->where('id',$ses_scid)->find_all();
        }
         $data['role'] = $this->user_role_model->getRoleUsers();
    	load_back_view('admin/reports/userReport',$data);
    }
    public function userReport() {

        $filter_by = $this->input->post('filter_by');
    	$res = $this->users_model->userReport($filter_by);

        $data='';
        // $c=1;
        foreach ($res as $dd) {
            $fullname   = $dd->first_name." ".$dd->last_name;
            $birth_date = date("d-m-Y",strtotime($dd->birth_date));
            $created_date = date("d-m-Y",strtotime($dd->created_date));
            $status='';
            if($dd->status=='Y')
            {
                $status ='Active';
            }
            else {
                $status ='In-Active';
            }
            $data .="<tr>
                        
                         <td>$fullname</td>
                         <td>$dd->username</td>
                         <td>$dd->society_name</td>
                         <td>$dd->building $dd->wing $dd->block</td>
                         <td>$dd->email</td>
                         <td>$dd->phone</td>
                         <td>$dd->birth_date</td>
                         <td>$dd->total_family_member</td>
                         <td>$status</td>
                         <td>$created_date</td>
                    </tr>";
    // $c++;  
}
        echo ($data);
        
    }
    public function userReportByBoth()
    {
        $filter_by = $this->input->post('filter_by');
        $filter_list = $this->input->post('filter_list');

        $res = $this->users_model->userReportByBoth($filter_by,$filter_list);
         $data='';
        foreach ($res as $dd) {
            $fullname   = $dd->first_name." ".$dd->last_name;
            $birth_date = date("d-m-Y",strtotime($dd->birth_date));
            $created_date = date("d-m-Y",strtotime($dd->created_date));
            $status='';
            if($dd->status=='Y')
            {
                $status ='Active';
            }
            else {
                $status ='In-Active';
            }
            $data .="<tr><td>$fullname</td>
                         <td>$dd->username</td>
                         <td>$dd->society_name</td>
                         <td>$dd->building $dd->wing $dd->block</td>
                         <td>$dd->email</td>
                         <td>$dd->phone</td>
                         <td>$dd->birth_date</td>
                         <td>$dd->total_family_member</td>
                         <td>$status</td>
                         <td>$created_date</td>
                    </tr>";
        }
        echo ($data);
    }
    public function search_byDesignation()
    {
        $role_id=$this->input->post('data');
        $details=$this->user_role_model->searchByRoleId($role_id);
        echo json_encode($details);
    }

}