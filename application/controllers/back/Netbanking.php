<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Netbanking extends CI_Controller {

	public function __construct() {
        parent::__construct();

        is_logged_in();

        // $this->load->model('facility_model');
        $this->load->model('net_banking_model');
    }

	public function index()
	{
		load_back_view('admin/net_banking');
	}

	public function add_net_banking(){
		$input = $this->input->post();

		if($input){
			$society_id = $this->session->userdata('society_id');

			$acc_name = $input['acc_name'];
			$acc_no = $input['acc_no'];
			$bank_name = $input['bank_name'];
			$ifsc_code = $input['ifsc_code'];
			$branch = $input['branch'];


			$this->net_banking_model->society_id = $society_id;

    		$this->net_banking_model->acc_name=$acc_name;
    		$this->net_banking_model->acc_no=$acc_no;
    		$this->net_banking_model->bank_name=$bank_name;
    		$this->net_banking_model->ifsc_code=$ifsc_code;
    		$this->net_banking_model->branch=$branch;    		
    		$net_banking=$this->net_banking_model->save();
    		if($net_banking)
    		{
    			$this->session->set_flashdata('msg_type', 'success');
			    $this->session->set_flashdata('msg', 'Netbanking details save successfully');
			    redirect(base_url().'back/Netbanking');
    		}else
    		{
    			$this->session->set_flashdata('msg_type', 'danger');
			    $this->session->set_flashdata('msg', 'failed');
    		}
    		redirect(base_url().'back/Netbanking');
		}
	}

	public function get_netbanking_data(){
		$session_data = $this->session->userdata();
		$society_id = $session_data['society_id'];
		$role_id = $session_data['role_id'];
		$this->datatables->select('1,id,acc_name,acc_no,bank_name,ifsc_code,branch');
		if($role_id ==SOCIETY_SUPERUSER || $role_id == SOCIETY_ADMIN)
        {
        	
        	$this->datatables->add_column('action', '<a href='.base_url() . 'back/netbanking/view_netbanking/$1 title="View netbanking details" class="views_wallet_trans margin" ref="$1"><i  class="glyphicon glyphicon-eye-open"></i> </a>|
        		<a href='.base_url() .'back/netbanking/edit_netbanking/$1 title="edit netbanking" class="views_wallet_trans margin" ref="$1"><i  class="glyphicon glyphicon glyphicon-pencil"></i> </a> |
        		 <a href='.base_url() .'back/netbanking/delete_netbanking/$1 title="delete netbanking" onClick="return doconfirm();" class="views_wallet_trans margin" ref="$1"><i  class="fa fa-trash-o"></i></a>', 'id');
        }
		$this->datatables->from('net-banking-details');	
		$res=$this->datatables->where('society_id',$society_id);	
		$cdata = $this->datatables->generate();	
		echo $cdata;
	}

	public function view_netbanking($id){
		$this->net_banking_model->id = $id;
		$ndata['net_details']= $this->net_banking_model->select();
		load_back_view('admin/netbanking/view_netbanking',$ndata);
	}

	public function edit_netbanking($id){
		$data['editnetbanking'] = $this->net_banking_model->editNetbanking($id);
		load_back_view('admin/netbanking/edit_netbanking',$data);
	}

	public function update(){
		$input=$this->input->post();
		if($input){
			$society_id = $this->session->userdata('society_id');

			$id = $input['id'];
			$acc_name = $input['acc_name'];
			$acc_no = $input['acc_no'];
			$bank_name = $input['bank_name'];
			$ifsc_code = $input['ifsc_code'];
			$branch = $input['branch'];

			$ndata=array(
				'id'=>$id,
				'society_id'=>$society_id,
				'acc_name'=>$acc_name,
				'acc_no'=>$acc_no,
				'bank_name'=>$bank_name,
				'ifsc_code'=>$ifsc_code,
				'branch'=>$branch
			);
			$update_data = $this->net_banking_model->updateNetbanking($id,$ndata);
			if($update_data){
				 $this->session->set_flashdata('msg', 'Net Banking details edit successully');
			   $this->session->set_flashdata('msg_type', 'success');
			   redirect(base_url().'back/netbanking');
			}
			else{
				 $this->session->set_flashdata('msg', 'Updatation error');
			     $this->session->set_flashdata('msg_type', 'danger');
			     redirect(base_url().'back/netbanking');
			}
		}
	}

	public function delete_netbanking($id)
	{
		$delete = $this->net_banking_model->delete_netbanking($id);
		if($delete)
		{
			 $this->session->set_flashdata('msg', 'delete successfully');
			   $this->session->set_flashdata('msg_type', 'success');
			   redirect(base_url().'back/netbanking');
		}else
		{
			 $this->session->set_flashdata('msg', 'deletion error');
			    $this->session->set_flashdata('msg_type', 'danger');
			    redirect(base_url().'back/netbanking');
		}

	}
}
