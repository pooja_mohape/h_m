<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of menu
 * 
 * @author jay butere<jayvant@stzsoft.com>
 * 
 * Revision History
 * 
 * 
 */
class Societycharges extends MY_Controller {

    public function __construct() {
        parent::__construct();
        is_logged_in();
        $this->load->model(array('society_house_charges_model','parking_charges_model','facility_master_model','facility_model'));    
      }
    public function index()
    {
        $data = array();
        $data['house_type'] = $this->society_house_charges_model->where('society_id',$this->session->userdata('society_id'))->as_array()->find_all();
        $data['parking_charges'] = $this->parking_charges_model->where('society_id',$this->session->userdata('society_id'))->as_array()->find_all();
        $data['facilities']= $this->facility_model->all_facility();
        
        load_back_view('admin/society_charges/add_charges',$data);
    }
  /*  public function add_charges()
    {
        $input = $this->input->post();
        if($input)
        {
             if(isset($input['house_type_check']) || isset($input['parking_check']))
            {
                if(isset($input['house_type_check']) && isset($input['parking_check']))
                {
                       $house_type = $input['house_type'];
                       
                       for($i = 0;$i < count($input['house_type']); $i++) {
                        $insert_house_details = array();
                       
                        $insert_house_details['society_id'] = $this->session->userdata('society_id');
                        $insert_house_details['house_type'] = $input['house_type'][$i];
                        $insert_house_details['maintenance_charges'] = $input['main_amt'][$i];
                        $insert_house_details['lease_amt'] = $input['lease_amt'][$i];
                        $insert_house_details['created_by'] = $this->session->userdata('user_id');

                         $check_housetype = $this->society_house_charges_model->where('society_id',$this->session->userdata('society_id'))->where('house_type',$input['house_type'][$i])->find_all();
                        
                        if(empty($check_housetype))
                        {
                            $inserted_id = $this->society_house_charges_model->insert($insert_house_details);
                        }
                      } 
                      for($i = 0;$i < count($input['parking_type']); $i++) {
                        $insert_park_details = array();
                       
                        $insert_park_details['society_id'] = $this->session->userdata('society_id');
                        $insert_park_details['vehicle_type'] = $input['parking_type'][$i];
                        $insert_park_details['parking_amt'] = $input['park_amt'][$i];

                        $check_parktype = $this->parking_charges_model->where('society_id',$this->session->userdata('society_id'))->where('vehicle_type',$input['parking_type'][$i])->find_all();
                        
                        if(empty($check_parktype))
                        {
                            $inserted_id = $this->parking_charges_model->insert($insert_park_details);
                        }
                      } 
                        $this->session->set_flashdata('msg', 'Thank You..Information submitted successfully');
                        $this->session->set_flashdata('msg_type', 'success');
                        redirect(base_url().'back/Societycharges');


                }else if (isset($input['house_type_check'])) {
                   
                   for($i = 0;$i < count($input['house_type']); $i++) {
                        $insert_house_details = array();
                       
                        $insert_house_details['society_id'] = $this->session->userdata('society_id');
                        $insert_house_details['house_type'] = $input['house_type'][$i];
                        $insert_house_details['maintenance_charges'] = $input['main_amt'][$i];
                        $insert_house_details['lease_amt'] = $input['lease_amt'][$i];
                        $insert_house_details['created_by'] = $this->session->userdata('user_id');
                        
                        $check_housetype = $this->society_house_charges_model->where('society_id',$this->session->userdata('society_id'))->where('house_type',$input['house_type'][$i])->find_all();

                       if(empty($check_housetype))
                        {
                            $inserted_id = $this->society_house_charges_model->insert($insert_house_details);
                            $msg = 'House type added successfully';
                        }
                        else
                        {
                             $msg = 'House type already exist';
                        }
                      } 
                       $this->session->set_flashdata('msg', $msg);
                        $this->session->set_flashdata('msg_type', 'success');
                        redirect(base_url().'back/Societycharges');

                }elseif ($input['parking_check']) {
                  
                   for($i = 0;$i < count($input['parking_type']); $i++) {
                        $insert_park_details = array();
                       
                        $insert_park_details['society_id'] = $this->session->userdata('society_id');
                        $insert_park_details['vehicle_type'] = $input['parking_type'][$i];
                        $insert_park_details['parking_amt'] = $input['park_amt'][$i];
                        
                         $check_parktype = $this->parking_charges_model->where('society_id',$this->session->userdata('society_id'))->where('vehicle_type',$input['parking_type'][$i])->find_all();

                         if(empty($check_parktype))
                        {
                            $inserted_id = $this->parking_charges_model->insert($insert_park_details);
                            $msg = 'Thank You..Information submitted successfully';
                        }
                        else
                        {
                          $msg = 'Vehicle type already exist';
                        }
                      } 
                        $this->session->set_flashdata('msg',$msg);
                        $this->session->set_flashdata('msg_type', 'success');
                        redirect(base_url().'back/Societycharges');

                }else
                {
                  
                }
            }
            else
            {
               $this->session->set_flashdata('msg', 'select atleast one checkbox');
                $this->session->set_flashdata('msg_type', 'success');
                redirect(base_url().'back/Societycharges');
            }
        }
        else
        {
            $this->session->set_flashdata('msg', 'Please provide valid input');
            $this->session->set_flashdata('msg_type', 'success');
            redirect(base_url().'back/Societycharges');
        }

    }*/
     public function add_charges()
    {
        $input = $this->input->post();
        if($input)
        {
             if(isset($input['house_type_check']) || isset($input['parking_check']) || isset($input['facility_check']))
            {
                if(isset($input['house_type_check']) && isset($input['parking_check']) && isset($input['facility_check']))
                {
                       $house_type = $input['house_type'];
                       
                       for($i = 0;$i < count($input['house_type']); $i++) {
                        $insert_house_details = array();
                       
                        $insert_house_details['society_id'] = $this->session->userdata('society_id');
                        $insert_house_details['house_type'] = $input['house_type'][$i];
                        $insert_house_details['maintenance_charges'] = $input['main_amt'][$i];
                        $insert_house_details['lease_amt'] = $input['lease_amt'][$i];
                        $insert_house_details['created_by'] = $this->session->userdata('user_id');

                         $check_housetype = $this->society_house_charges_model->where('society_id',$this->session->userdata('society_id'))->where('house_type',$input['house_type'][$i])->find_all();
                        
                        if(empty($check_housetype))
                        {
                            $inserted_id = $this->society_house_charges_model->insert($insert_house_details);
                        }
                      } 
                      for($i = 0;$i < count($input['parking_type']); $i++) {
                        $insert_park_details = array();
                       
                        $insert_park_details['society_id'] = $this->session->userdata('society_id');
                        $insert_park_details['vehicle_type'] = $input['parking_type'][$i];
                        $insert_park_details['parking_amt'] = $input['park_amt'][$i];

                        $check_parktype = $this->parking_charges_model->where('society_id',$this->session->userdata('society_id'))->where('vehicle_type',$input['parking_type'][$i])->find_all();
                        
                        if(empty($check_parktype))
                        {
                            $inserted_id = $this->parking_charges_model->insert($insert_park_details);
                        }
                      } 
                      for($i = 0;$i < count($input['facility_name']); $i++) {
                        $insert_fac_details = array();
                       
                        $insert_fac_details['society_id'] = $this->session->userdata('society_id');
                         $insert_fac_details['facility_name'] = $input['facility_name'][$i];
                        if(empty($input['adult_charges'][$i]))
                        {
                           $input['adult_charges'][$i] = 0; 
                        }
                        if(empty($input['child_charges'][$i]))
                        {
                           $input['child_charges'][$i] = 0; 
                        }
                        $insert_fac_details['adult_charges'] = $input['adult_charges'][$i];
                        $insert_fac_details['child_charges'] = $input['child_charges'][$i];
                        $check_factype = $this->facility_master_model->where('society_id',$this->session->userdata('society_id'))->where('facility_name',$input['facility_name'][$i])->find_all();
                        
                        if(empty($check_factype))
                        {
                            $inserted_id = $this->facility_master_model->insert($insert_fac_details);
                        }
                      } 

                      $this->session->set_flashdata('msg', 'Thank You..Information submitted successfully');
                      $this->session->set_flashdata('msg_type', 'success');
                      redirect(base_url().'back/Societycharges');
                }
                elseif (isset($input['house_type_check']) && isset($input['parking_check'])) 
                {
                       $house_type = $input['house_type'];

                       for($i = 0;$i < count($input['house_type']); $i++) {
                        $insert_house_details = array();
                       
                        $insert_house_details['society_id'] = $this->session->userdata('society_id');
                        $insert_house_details['house_type'] = $input['house_type'][$i];
                        $insert_house_details['maintenance_charges'] = $input['main_amt'][$i];
                        $insert_house_details['lease_amt'] = $input['lease_amt'][$i];
                        $insert_house_details['created_by'] = $this->session->userdata('user_id');

                         $check_housetype = $this->society_house_charges_model->where('society_id',$this->session->userdata('society_id'))->where('house_type',$input['house_type'][$i])->find_all();
                        
                        if(empty($check_housetype))
                        {
                            $inserted_id = $this->society_house_charges_model->insert($insert_house_details);
                        }
                      } 
                      for($i = 0;$i < count($input['parking_type']); $i++) {
                        $insert_park_details = array();
                       
                        $insert_park_details['society_id'] = $this->session->userdata('society_id');
                        $insert_park_details['vehicle_type'] = $input['parking_type'][$i];
                        $insert_park_details['parking_amt'] = $input['park_amt'][$i];

                        $check_parktype = $this->parking_charges_model->where('society_id',$this->session->userdata('society_id'))->where('vehicle_type',$input['parking_type'][$i])->find_all();
                        
                        if(empty($check_parktype))
                        {
                            $inserted_id = $this->parking_charges_model->insert($insert_park_details);
                        }
                      }
                      $this->session->set_flashdata('msg', 'Thank You..Information submitted successfully');
                      $this->session->set_flashdata('msg_type', 'success');
                      redirect(base_url().'back/Societycharges');
                }
                elseif (isset($input['house_type_check']) && isset($input['facility_check'])) 
                {
                       $house_type = $input['house_type'];
                       
                       for($i = 0;$i < count($input['house_type']); $i++)
                       {
                        $insert_house_details = array();
                       
                        $insert_house_details['society_id'] = $this->session->userdata('society_id');
                        $insert_house_details['house_type'] = $input['house_type'][$i];
                        $insert_house_details['maintenance_charges'] = $input['main_amt'][$i];
                        $insert_house_details['lease_amt'] = $input['lease_amt'][$i];
                        $insert_house_details['created_by'] = $this->session->userdata('user_id');

                         $check_housetype = $this->society_house_charges_model->where('society_id',$this->session->userdata('society_id'))->where('house_type',$input['house_type'][$i])->find_all();
                        
                        if(empty($check_housetype))
                        {
                            $inserted_id = $this->society_house_charges_model->insert($insert_house_details);
                        }
                      } 
                      for($i = 0;$i < count($input['facility_name']); $i++) {
                        $insert_fac_details = array();
                       
                        $insert_fac_details['society_id'] = $this->session->userdata('society_id');
                         $insert_fac_details['facility_name'] = $input['facility_name'][$i];
                        if(empty($input['adult_charges'][$i]))
                        {
                           $input['adult_charges'][$i] = 0; 
                        }
                        if(empty($input['child_charges'][$i]))
                        {
                           $input['child_charges'][$i] = 0; 
                        }
                        $insert_fac_details['adult_charges'] = $input['adult_charges'][$i];
                        $insert_fac_details['child_charges'] = $input['child_charges'][$i];
                        $check_factype = $this->facility_master_model->where('society_id',$this->session->userdata('society_id'))->where('facility_name',$input['facility_name'][$i])->find_all();
                        
                        if(empty($check_factype))
                        {
                            $inserted_id = $this->facility_master_model->insert($insert_fac_details);
                        }
                      } 
                      $this->session->set_flashdata('msg', 'Thank You..Information submitted successfully');
                      $this->session->set_flashdata('msg_type', 'success');
                      redirect(base_url().'back/Societycharges');
                }
                elseif (isset($input['parking_check']) && isset($input['facility_check'])) {

                    for($i = 0;$i < count($input['parking_type']); $i++) {
                        $insert_park_details = array();
                       
                        $insert_park_details['society_id'] = $this->session->userdata('society_id');
                        $insert_park_details['vehicle_type'] = $input['parking_type'][$i];
                        $insert_park_details['parking_amt'] = $input['park_amt'][$i];

                        $check_parktype = $this->parking_charges_model->where('society_id',$this->session->userdata('society_id'))->where('vehicle_type',$input['parking_type'][$i])->find_all();
                        
                        if(empty($check_parktype))
                        {
                            $inserted_id = $this->parking_charges_model->insert($insert_park_details);
                        }
                      } 
                      for($i = 0;$i < count($input['facility_name']); $i++) {
                        $insert_fac_details = array();
                       
                        $insert_fac_details['society_id'] = $this->session->userdata('society_id');
                         $insert_fac_details['facility_name'] = $input['facility_name'][$i];
                        if(empty($input['adult_charges'][$i]))
                        {
                           $input['adult_charges'][$i] = 0; 
                        }
                        if(empty($input['child_charges'][$i]))
                        {
                           $input['child_charges'][$i] = 0; 
                        }
                        $insert_fac_details['adult_charges'] = $input['adult_charges'][$i];
                        $insert_fac_details['child_charges'] = $input['child_charges'][$i];
                        $check_factype = $this->facility_master_model->where('society_id',$this->session->userdata('society_id'))->where('facility_name',$input['facility_name'][$i])->where('is_deleted','N')->find_all();
                        
                          if(empty($check_factype))
                          {
                            $inserted_fac = $this->facility_master_model->insert($insert_fac_details);
                          }
                        } 
                        $this->session->set_flashdata('msg', 'Thank You..Information submitted successfully');
                        $this->session->set_flashdata('msg_type', 'success');
                        redirect(base_url().'back/Societycharges');
                }
                else if (isset($input['house_type_check'])) {
                   
                   for($i = 0;$i < count($input['house_type']); $i++) {
                        $insert_house_details = array();
                       
                        $insert_house_details['society_id'] = $this->session->userdata('society_id');
                        $insert_house_details['house_type'] = $input['house_type'][$i];
                        $insert_house_details['maintenance_charges'] = $input['main_amt'][$i];
                        $insert_house_details['lease_amt'] = $input['lease_amt'][$i];
                        $insert_house_details['created_by'] = $this->session->userdata('user_id');
                        
                        $check_housetype = $this->society_house_charges_model->where('society_id',$this->session->userdata('society_id'))->where('house_type',$input['house_type'][$i])->find_all();

                       if(empty($check_housetype))
                        {
                            $inserted_id = $this->society_house_charges_model->insert($insert_house_details);
                            $msg = 'House type added successfully';
                        }
                        else
                        {
                             $msg = 'House type already exist';
                        }
                      } 
                       $this->session->set_flashdata('msg', $msg);
                        $this->session->set_flashdata('msg_type', 'success');
                        redirect(base_url().'back/Societycharges');

                }elseif (isset($input['parking_check'])) {
                  
                   for($i = 0;$i < count($input['parking_type']); $i++) {
                        $insert_park_details = array();
                       
                        $insert_park_details['society_id'] = $this->session->userdata('society_id');
                        $insert_park_details['vehicle_type'] = $input['parking_type'][$i];
                        $insert_park_details['parking_amt'] = $input['park_amt'][$i];
                        
                         $check_parktype = $this->parking_charges_model->where('society_id',$this->session->userdata('society_id'))->where('vehicle_type',$input['parking_type'][$i])->find_all();

                         if(empty($check_parktype))
                        {
                            $inserted_id = $this->parking_charges_model->insert($insert_park_details);
                            $msg = 'Thank You..Information submitted successfully';
                        }
                        else
                        {
                          $msg = 'Vehicle type already exist';
                        }
                      } 
                        $this->session->set_flashdata('msg',$msg);
                        $this->session->set_flashdata('msg_type', 'success');
                        redirect(base_url().'back/Societycharges');

                }
                elseif (isset($input['facility_check'])) {
                       
                        for($i = 0;$i < count($input['facility_name']); $i++) {
                        $insert_fac_details = array();
                       
                        $insert_fac_details['society_id'] = $this->session->userdata('society_id');
                        $insert_fac_details['facility_name'] = $input['facility_name'][$i];
                        if(empty($input['adult_charges'][$i]))
                        {
                           $input['adult_charges'][$i] = 0; 
                        }
                        if(empty($input['child_charges'][$i]))
                        {
                           $input['child_charges'][$i] = 0; 
                        }
                        $insert_fac_details['adult_charges'] = $input['adult_charges'][$i];
                        $insert_fac_details['child_charges'] = $input['child_charges'][$i];
                        $check_factype = $this->facility_master_model->where('society_id',$this->session->userdata('society_id'))->where('facility_name',$input['facility_name'][$i])->where('is_deleted','N')->find_all();
                        
                        if(empty($check_factype))
                        {
                            $inserted_id = $this->facility_master_model->insert($insert_fac_details);
                            $msg = "facility details added successfully";
                        }
                        else{
                            $msg = "facility details already exist";
                        }
                      } 
                      $this->session->set_flashdata('msg',$msg);
                      $this->session->set_flashdata('msg_type', 'success');
                      redirect(base_url().'back/Societycharges');
                }
                else
                {
                  
                }
            }
            else
            {
               $this->session->set_flashdata('msg', 'select atleast one checkbox');
                $this->session->set_flashdata('msg_type', 'success');
                redirect(base_url().'back/Societycharges');
            }
        }
        else
        {
            $this->session->set_flashdata('msg', 'Please provide valid input');
            $this->session->set_flashdata('msg_type', 'success');
            redirect(base_url().'back/Societycharges');
        }

    }
     public function edit_house_charge($id="")
    {
      if($id)
      {
          $data['house_type'] = $this->society_house_charges_model->where('id',$id)->as_array()->find_all();
  
          load_back_view('admin/society_charges/edit_house_charge',$data);
      }
      else
      {
        redirect(base_url().'back/societycharges');
      }
    }
    public function update(){
        $input=$this->input->post();       
        if($input){
            $id=$input['id'];
             $maintenence_amount=$input['maintenence_amount'];
             $lease_amount=$input['lease_amount'];
             $array = array('maintenance_charges'=>$maintenence_amount,
                            'lease_amt'=>$lease_amount);
        $update=$this->society_house_charges_model->update($id,$array);
        }
       $this->session->set_flashdata('msg', 'House type Updated successfully');
       $this->session->set_flashdata('msg_type', 'success');
       redirect(base_url().'back/Societycharges');
        
    }
    public function delete_charge($id)
    {      
      $delete=$this->society_house_charges_model->delete_data($id);
      $this->session->set_flashdata('msg', 'House type Deleted successfully');
      $this->session->set_flashdata('msg_type', 'success');
      redirect(base_url().'back/Societycharges');
    }
    public function edit_park_charges($id)
    {
        $data=array();
       $data['park_charge'] = $this->parking_charges_model->where('id',$id)->as_array()->find_all();
       if($data['park_charge']){
        load_back_view('admin/society_charges/edit_park',$data);
        }
    }
    public function update_park()
    {
        $input=$this->input->post();
        
        if($input){
            $id=$input['id'];
             $vehicle_type=$input['vehicle_type'];
            $parking_amount=$input['parking_amount'];

            $array=array('parking_amt'=>$parking_amount); 
            $this->parking_charges_model->update($id,$array);
            $this->session->set_flashdata('msg', 'Parking Details Update successfully');
            $this->session->set_flashdata('msg_type', 'success');
            redirect(base_url().'back/societycharges');
        }
        
    }
    public function delete_park($id)
    {
      $delete=$this->parking_charges_model->delete_park($id);
       $this->session->set_flashdata('msg', 'Parking Vehicle delete successfully');
            $this->session->set_flashdata('msg_type', 'success');
      redirect(base_url().'back/Societycharges');
    }
}

//end of menu class