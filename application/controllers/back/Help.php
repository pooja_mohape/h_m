<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Help extends MY_Controller {

	public function __construct() {
		parent::__construct();
		is_logged_in();

	}
	public function index() {
		$data = array();
		load_back_view('admin/help/help_view', $data);
	}
}
?>