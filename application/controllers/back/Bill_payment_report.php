<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bill_payment_report extends CI_Controller {

	public function __construct() {
		parent::__construct();
		is_logged_in();
	}

	public function index() {
		$data = array();
		$this->load->model('my_bill_model');
		$this->load->model('society_master_model');

		$data['userdata'] = $this->users_model->where('society_id', $this->session->userdata('society_id'))->where_not_in('role_id', SOCIETY_SUPERUSER)->where('status','Y')->find_all();
		$month_details    = $this->my_bill_model->where('society_id', $this->session->userdata('society_id'))->order_by("id", "desc")->limit(1)->find_all();

		if (empty($month_details)) {
			$society_id          = $this->session->userdata('society_id');
			$society_Details     = $this->society_master_model->where('id', $society_id)->find_all();
			$time                = strtotime($society_Details[0]->created_date);
			$month               = date("Y", $time).'-'.date("m", $time);
			$data['hide_button'] = false;
			$data['month']       = $month;
		} else if ($month_details[0]->month == date('Y-m')) {
			$data['hide_button'] = true;
			$data['month']       = false;
		} else if ($month_details[0]->month && $month_details[0]->month != date('Y-m')) {
			$time      = strtotime($month_details[0]->month);
			$mon       = date("m", $time);
			$plusmonth = $mon+01;
			if (strlen($plusmonth) == '1') {
				$plusmonth = '0'.$plusmonth;
			}
			$month               = date("Y", $time).'-'.$plusmonth;
			$data['hide_button'] = false;
			$data['month']       = $month;
		}
		//show($data,1);
		load_back_view('admin/reports/bill_generation_report', $data);
	}
	public function get_bill_data() {
		$data         = $this->input->post();
		$session_data = $this->session->userdata();
		$role_id      = $session_data['role_id'];
		$society_id   = $session_data['society_id'];
		$user_id      = $session_data['id'];
		$this->datatables->select('1,mb.id,mb.month as transaction_date,concat(u.first_name," ",u.last_name) as full_name,mb.maintenance_amt,mb.parking_amt,mb.facility_charges,mb.other_charges,mb.total_amt,mb.outstanding_amt_after');
		$this->datatables->from('my_bill mb');
		$this->datatables->join('users u', 'u.id=mb.user_id', 'left');
		$this->datatables->join('wallet w', 'w.user_id=mb.user_id', 'left');
		$this->datatables->where('mb.society_id', $society_id);
		if (isset($data['user_id']) && !empty($data['user_id'])) {
			$this->datatables->where('mb.user_id', $data['user_id']);
		}
		if (isset($data['transaction_date']) && !empty($data['transaction_date'])) {
			$exp   = explode("-", $data['transaction_date']);
			$month = $exp[1].'-'.$exp[0];
			$this->datatables->where("mb.month = ", $month);
		}
		if ($this->session->userdata('role_id') == SOCIETY_MEMBER) {
			$this->datatables->where('mb.user_id', $user_id);
		}
		$this->datatables->add_column('action', '<a href='.base_url().'back/bill_payment/pdf/$1 title="Genrate pdf" title="Pdf" class="views_wallet_trans margin" ref="$1">'.'<img style="padding-right: 10px;max-height: 42px;" src='.base_url().'assets/images/pdffile.png></a>', 'id');
		$data = $this->datatables->generate();
		echo $data;
	}
	public function statement() {
		$data = array();
		$id   = $this->uri->segment(4);
		if ($id) {
			$data['transactions_by_id'] = $id;
		} else {
			$data['transactions_by_id'] = '';
		}
		$data['userdata'] = $this->users_model->where('society_id', $this->session->userdata('society_id'))->where_not_in('role_id', SOCIETY_SUPERUSER)->find_all();
		load_back_view('admin/reports/transaction_statement', $data);
	}
	public function get_transaction_statement() {
		$data         = $this->input->post();
		$session_data = $this->session->userdata();
		$role_id      = $session_data['role_id'];
		$society_id   = $session_data['society_id'];
		$user_id      = $session_data['id'];
		$this->datatables->select('1,wt.id,DATE_FORMAT(wt.created_at,"%d-%m-%Y") as transaction_date,concat(u.first_name," ",u.last_name) as full_name,bp.transaction_type,wt.outstanding_amount_before,wt.amount,wt.outstanding_amount_after,wt.wallet_balance_before,wt.wallet_balance_after');
		$this->datatables->from('wallet_transaction wt');
		$this->datatables->join('users u', 'u.id=wt.user_id', 'left');
		$this->datatables->join('bill_payment bp', 'wt.bill_payment_id=bp.id', 'left');
		if (isset($data['user_id']) && !empty($data['user_id'])) {
			$this->datatables->where('wt.user_id', $data['user_id']);
		}
		if (isset($data['from_date']) && $data['from_date'] != '') {
			$this->datatables->where("DATE_FORMAT(wt.created_at, '%Y-%m-%d') >=", date('Y-m-d', strtotime($data['from_date'])));
		}
		if ($data['transaction_by_id']) {
			$this->datatables->where('u.id', $data['transaction_by_id']);
		}
		if (isset($data['to_date']) && $data['to_date'] != '') {
			$this->datatables->where("DATE_FORMAT(wt.created_at, '%Y-%m-%d')<=", date('Y-m-d', strtotime($data['to_date'])));
		}
		if ($this->session->userdata('role_id') == SOCIETY_MEMBER) {
			$this->datatables->where('wt.user_id', $user_id);
		}
		$this->datatables->where('u.society_id', $society_id);
		$data = $this->datatables->generate();
		echo $data;
	}
	public function outstanding_report() {
		$data             = array();
		$data['userdata'] = $this->users_model->where('society_id', $this->session->userdata('society_id'))->where_not_in('role_id', SOCIETY_SUPERUSER)->find_all();
		load_back_view('admin/reports/bill_outstanding_report', $data);
	}
	public function get_outstanding() {
		$data         = $this->input->post();
		$session_data = $this->session->userdata();
		$role_id      = $session_data['role_id'];
		$society_id   = $session_data['society_id'];
		$user_id      = $session_data['id'];

		$this->datatables->select('1,u.id,concat(u.first_name," ",u.last_name) as full_name,w.current_outstanding,w.wallet_balance');
		$this->datatables->from('wallet w');
		$this->datatables->join('users u', 'u.id=w.user_id', 'left');

		if ($this->session->userdata('role_id') == SOCIETY_SUPERUSER || $this->session->userdata('role_id') == SOCIETY_ADMIN) {
			$this->datatables->add_column('action', '<a href='.base_url().'back/Bill_payment_report/statement/$1 title="View Transacations" title="view transactions" class="views_wallet_trans margin" ref="$1"><i  class="glyphicon glyphicon-th-list"></i> </a> | <a href='.base_url().'back/email/index/$1 title="Send Mail" title="view transactions" class="views_wallet_trans margin" ref="$1"><i  class="glyphicon glyphicon-envelope"></i> </a> | <a href='.base_url().'back/sms/index/$1 title="Send Sms" title="view transactions" class="views_wallet_trans margin" ref="$1"><i class=" fa fa-comment"></i> </a>', 'id');
		} else {
			$this->datatables->add_column('action', '<a href='.base_url().'back/Bill_payment_report/statement/$1 title="View Transacations" title="view transactions" class="views_wallet_trans margin" ref="$1"><i  class="glyphicon glyphicon-th-list"></i> </a>', 'id');
		}

		if (isset($data['user_id']) && !empty($data['user_id'])) {
			$this->datatables->where('w.user_id', $data['user_id']);
		}
		if ($this->session->userdata('role_id') == SOCIETY_MEMBER) {
			$this->datatables->where('w.user_id', $user_id);
		}
		$this->datatables->where('u.society_id', $society_id);
		$data = $this->datatables->generate();
		echo $data;
	}
}
