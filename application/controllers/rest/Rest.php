<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rest extends CI_Controller {

  public function __construct() {
    parent::__construct();
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Headers: Content-Type');
    header('Content-Type: text/html; charset=utf-8');
    header('Content-Type: application/json; charset=utf-8');
    $this->load->model(array('facility_model','users_model','house_master_model','society_house_charges_model','my_bill_model','parking_model','wallet_model','parking_charges_model'));
        
    $data = array();
    $data['url'] = current_url();
    $data['Request_type'] = $_SERVER['REQUEST_METHOD'];
    $data['post'] = json_decode(file_get_contents('php://input'));
    $data['ip'] = $this->input->ip_address();
       /* log_data('Api/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $data, 'api');*/
  }

  public function user_data() {
    
    $msg = '';$status =false;
    $response = array('status' => false, 'message' => '');

    if ($_SERVER["REQUEST_METHOD"] == "POST") 
    {
      $input = json_decode(file_get_contents('php://input'));
      $user_id = isset($input->user_id) ? $input->user_id : '';
      if(empty($user_id)) {
        $response['message'] = 'user id is required.';
        $response['code'] = false;
      }else 
      {
         $valid_user = $this->users_model->column('id,role_id,first_name,last_name,username,email,phone,birth_date,total_family_member,society_id,house_id,status,profile_pic')->where('id',$user_id)->where('role_id !=',SOCIETY_SUPERUSER)->find_all();

         if($valid_user)
         {
            $user_data = $valid_user[0];
            $response['user_data'] = $user_data;
            if(!empty($user_data->profile_pic))
            {
              $url = str_replace(' ', '%20', base_url().'upload/profileImages/'.$user_data->profile_pic);
              $response['user_data']->profile_pic = $url;
            }
            $wallet_details = $this->wallet_model->where('user_id',$user_id)->find_all();
            $current_outstanding = '0.00';
            if($wallet_details)
            {
                $current_outstanding = $wallet_details[0]->current_outstanding;  
            }
            $response['current_outstanding'] = $current_outstanding;
            /* House Details start */
            $house_details = $this->house_master_model->column('wing,block,house_type,on_lease,allocated')->where('id',$user_data->house_id)->find_all();
            $response['house_details'] = $house_details[0];
            $house_type_data = $this->society_house_charges_model->where('id',$house_details[0]->house_type)->find_all();
            if($house_type_data)
            {
                  $house_type_data = $house_type_data[0];
                  $response['house_details']->house_type = $house_type_data->house_type;
                  $response['house_details']->maintenance_charges = $house_type_data->maintenance_charges;
                  $response['house_details']->lease_amt = $house_type_data->lease_amt;
            }
            /* House Details end */
            /* parking & facility details start */
            $parking_details = $this->parking_model->where('user_id',$user_id)->find_all();
            if($parking_details)
            {
                 if(sizeof($parking_details) > 1)
                 {
                    foreach ($parking_details as $key => $value) {
                      $pm_id = $value->pm_id;
                      $this->db->select('*');
                      $this->db->from('parking_master');
                      $this->db->where('id',$pm_id);
                      $res = $this->db->get()->result();
                      if($res)
                      {
                        $response['parking_details'][$key]['slot_no'] = $res[0]->slot_no;
                        $parking_charge = $this->parking_charges_model->where('id',$res[0]->parking_charge_id)->find_all();
                        if($parking_charge)
                        {
                            $response['parking_details'][$key]['vehicle_type'] = $parking_charge[0]->vehicle_type;
                            $response['parking_details'][$key]['parking_amt'] = $parking_charge[0]->parking_amt;
                        }
                      }
                    }
                 }
                 else
                 {
                    $pm_id = $parking_details[0]->pm_id;
                    $this->db->select('*');
                    $this->db->from('parking_master');
                    $this->db->where('id',$pm_id);
                    $res = $this->db->get()->result();
                    if($res)
                    {
                      $response['parking_details'][0]['slot_no'] = $res[0]->slot_no;
                      $parking_charge = $this->parking_charges_model->where('id',$res[0]->parking_charge_id)->find_all();
                      $response['parking_details'][0]['vehicle_type'] = $parking_charge[0]->vehicle_type;
                      $response['parking_details'][0]['parking_amt'] = $parking_charge[0]->parking_amt;
                    }
                 }
            }
            $facility_details = $this->facility_model->where('user_id',$user_id)->find_all();
            if($facility_details)
            {
               if(sizeof($facility_details) > 1)
               {
                  foreach ($facility_details as $key => $value) {
                    $this->db->select('f.*,fm.*');
                    $this->db->from('facilty f');
                    $this->db->join('facility_master fm','f.facilty_id = fm.id');
                    $this->db->where('f.id',$value->id);
                    $result = $this->db->get()->result();
                    if($result)
                    {
                      $response['facility_details'][$key]['facility_name'] = $result[0]->facility_name;
                      $response['facility_details'][$key]['adult_count'] = $result[0]->adult_count;
                      $response['facility_details'][$key]['child_count'] = $result[0]->child_count;
                      $response['facility_details'][$key]['total_fees'] = $result[0]->total_fees;
                      $response['facility_details'][$key]['facility_name'] = $result[0]->facility_name;
                    }
                  }
               }
               else
               {
                    $this->db->select('f.*,fm.*');
                    $this->db->from('facilty f');
                    $this->db->join('facility_master fm','f.facilty_id = fm.id');
                    $this->db->where('f.id',$facility_details[0]->id);
                    $result = $this->db->get()->result();
                    if($result)
                    {
                      $response['facility_details'][0]['facility_name'] = $result[0]->facility_name;
                      $response['facility_details'][0]['adult_count'] = $result[0]->adult_count;
                      $response['facility_details'][0]['child_count'] = $result[0]->child_count;
                      $response['facility_details'][0]['total_fees'] = $result[0]->total_fees;
                      $response['facility_details'][0]['facility_name'] = $result[0]->facility_name;
                    }
               }
            }
            /* parking & facility details end */
            /* Monthly Bill Genratation start*/
            $current_month_bill = $this->my_bill_model->where('user_id',$user_id)->where('DATE_FORMAT(created_at, "%Y-%m")=',date('Y-m'))->find_all();
            if($current_month_bill)
            {
              $response['current_month_bill'] = $current_month_bill;
            }
            else
            {
             $response['current_month_bill'] = 'Monthly Bill Not generated yet'; 
            }
            /* Monthly Bill Genratation end */
            
            $msg = 'data retrived successfully';
            $status = true;
         }
         else
         {
            $msg = 'invalid user_id , please provide valid user id.';
            $status = false;
         }
      }
    } 
    else 
    {
       $msg = 'No direct script is allowed.';
       $status = false;
    }
    $response['message'] = $msg;
    $response['status'] = $status;
    echo json_encode($response);
  }
  public function get_member_authority()
  {
     $data['authority'] = '';
     $authority = $this->db->select('*')->from('member_authority')->get()->result();
     if($authority)
     {
      $data['authority'] = $authority;
     }
     data2json($data);
  }
}

?>
