<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct() {
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Allow-Headers: Content-Type');
		header('Content-Type: text/html; charset=utf-8');
		header('Content-Type: application/json; charset=utf-8');
		$this->load->model(array('notices_model', 'users_model', 'house_master_model', 'complaint_model', 'meetings_model', 'parking_model', 'bill_payment_model', 'document_collection_model', 'society_house_charges_model'));

		$data                 = array();
		$data['url']          = current_url();
		$data['Request_type'] = $_SERVER['REQUEST_METHOD'];
		$data['post']         = json_decode(file_get_contents('php://input'));
		$data['ip']           = $this->input->ip_address();
		log_data('Api/'.date('M').'/'.date('d-m-Y').'/request'.'.log', $data, 'api');
	}

	public function notice_list() {
		$response = array('status' => false, 'message' => '');
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input     = json_decode(file_get_contents('php://input'));
			$notice_to = $input->user_id;
			if (empty($notice_to)) {
				$response['message'] = 'user id is required.';
				$response['code']    = 201;
			} else {
				$notice_to   = trim($notice_to);
				$notice_data = "";
				$notice_data = $this->notices_model->where('notice_to', $notice_to)->group_by('notice_id')->order_by('created_at', 'desc')->where('notice_view', 'N')->find_all();
				if ($notice_data) {
					foreach ($notice_data as $key => $value) {
						$notice_data[$key]->notice_description = html_entity_decode($value->notice_description);
					}
				}
				$response['status']      = 'success';
				$response['notice_to']   = $notice_to;
				$response['notice_data'] = $notice_data;
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}
	public function login() {
		$response = array('status' => false, 'message' => '');
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input    = json_decode(file_get_contents('php://input'));
			$username = $input->email;
			$password = $input->password;

			if (empty($username)) {
				$msg    = 'Email is required.';
				$status = false;
			} else if (empty($password)) {
				$msg    = 'Password is required.';
				$status = fasle;
			} else {
				$udata = $this->users_model->where('username', $username)->or_where('email', $username)->find_all();
				if ($udata) {
					$user_array = $udata[0];
					if ($user_array->status == "Y") {
						$salt          = $user_array->salt;
						$user_password = $user_array->password;
						$password      = md5($salt.$password);
						if ($password == $user_password) {
							$role_name = '';
							$role_name = $this->users_model->getRoleName($user_array->role_id);
							$pic       = $user_array->profile_pic;
							if ($pic) {
								$pic = str_replace(' ', '%20', base_url().'upload/profileImages/'.$pic);
							}
							$user_data = array(
								"id"          => $user_array->id,
								"user_id"     => $user_array->id,
								"role_id"     => $user_array->role_id,
								"role_name"   => $role_name,
								"username"    => $user_array->username,
								"email"       => $user_array->email,
								"first_name"  => $user_array->first_name,
								"last_name"   => $user_array->last_name,
								"mobile_no"   => $user_array->phone,
								"profile_pic" => $pic,
								"society_id"  => $user_array->society_id,
								"house_id"    => $user_array->house_id
							);
							$status                    = true;
							$msg                       = "login done successfully";
							$response['login_details'] = $user_data;
						} else {
							$msg    = "password is incorrect";
							$status = false;
						}
					} else {
						$msg    = "user account is disabled";
						$status = false;
					}
				} else {
					$msg    = "username is incorrect";
					$status = false;
				}
			}
		} else {
			$msg    = 'No direct script is allowed.';
			$status = false;
		}
		$response['message'] = $msg;
		$response['status']  = $status;
		echo json_encode($response);
	}
	public function getUserList() {
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input      = json_decode(file_get_contents('php://input'));
			$society_id = isset($input->society_id)?$input->society_id:'';
			if (empty($society_id)) {
				$response['message'] = 'society_id is required.';
				$response['code']    = 201;
			} else {
				$society_id = trim($society_id);
				$user_data  = "";
				$user_data  = $this->users_model->where('society_id', $society_id)->where('role_id !=', SOCIETY_SUPERUSER)->where('is_deleted', 'N')->find_all();
				if ($user_data) {
					foreach ($user_data as $key => $value) {
						$profile_pic = file_exists(base_url().'upload/profileImages/'.$value->profile_pic);
						if ($profile_pic) {
							$profile_pic = base_url().'upload/profileImages/'.$value->profile_pic;
						} else {
							$profile_pic = "";
						}
						$profile_pic                  = base_url().'upload/profileImages/'.$value->profile_pic;
						$user_data[$key]->profile_pic = $profile_pic;
					}
				}
				$response['status']     = 'success';
				$response['society_id'] = $society_id;
				$response['user_data']  = $user_data;
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}
	public function listHouse() {
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input      = json_decode(file_get_contents('php://input'));
			$society_id = $input->society_id;
			if (empty($society_id)) {
				$response['message'] = 'society id is required.';
				$response['code']    = 201;
			} else {
				$society_id = trim($society_id);
				$house_data = "";
				$this->db->select('u.id as user_id,ht.*');
				$this->db->from('house_master ht');
				$this->db->where('ht.society_id', $society_id);
				$this->db->group_by('ht.id');
				$this->db->join('users u', 'u.house_id=ht.id', 'left');
				$result = $this->db->get()->result();

				foreach ($result as $key => $value) {
					$url                         = str_replace(' ', '%20', base_url().'upload/houseImages/'.$value->house_picture);
					$result[$key]->house_picture = $url;
				}
				$response['status']     = 'success';
				$response['society_id'] = $society_id;
				$response['house_data'] = $result;
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}
	public function ComplaintList() {
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input      = json_decode(file_get_contents('php://input'));
			$society_id = $input->society_id;
			if (empty($society_id)) {
				$response['message'] = 'society id is required.';
				$response['code']    = 201;
			} else {
				$society_id = trim($society_id);
				$house_data = "";
				$house_data = $this->complaint_model->where('society_id', $society_id)->order_by('created_date', 'desc')->find_all();

				$response['status']         = 'success';
				$response['society_id']     = $society_id;
				$response['complaint_data'] = $house_data;
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}
	public function InsertComplaints() {
		$response = array();
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input = json_decode(file_get_contents('php://input'));

			$society_id        = isset($input->society_id)?$input->society_id:'';
			$user_id           = isset($input->user_id)?$input->user_id:'';
			$complaint_type    = isset($input->complaint_type)?$input->complaint_type:'';
			$complaint_details = isset($input->complaint_details)?$input->complaint_details:'';

			if (empty($society_id) || empty($user_id) || empty($complaint_type) || empty($complaint_details)) {
				$msg    = "please provide valid input";
				$status = false;
			} else {
				$this->complaint_model->user_id           = $user_id;
				$this->complaint_model->society_id        = $society_id;
				$this->complaint_model->complaint_type    = $complaint_type;
				$this->complaint_model->complaint_details = $complaint_details;

				$InsertComplaints_id = $this->complaint_model->save();
				if ($InsertComplaints_id) {
					$response['complaint_id'] = $InsertComplaints_id;
					$msg                      = "Complaint send successfully";
					$status                   = true;
				} else {
					$msg    = "failed to raise complaint";
					$status = false;
				}
			}
		} else {
			$msg    = 'No direct script is allowed.';
			$status = false;
		}
		$response['msg']    = $msg;
		$response['status'] = $status;
		data2json($response);
	}
	public function updateComplaints() {
		$response = array();
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input = json_decode(file_get_contents('php://input'));

			$complaint_id = isset($input->complaint_id)?$input->complaint_id:'';
			$society_id   = isset($input->society_id)?$input->society_id:'';
			$remark       = isset($input->remark)?$input->remark:'';
			if (empty($society_id) || empty($complaint_id) || empty($remark)) {
				$msg    = "please provide valid input";
				$status = false;
			} else {
				$update_data = array(
					'admin_remark' => $remark,
					'is_solved'    => 'Y',
				);

				$this->db->where('id', $complaint_id);
				$update = $this->db->update('complaints', $update_data);

				if ($update) {
					$msg    = "Complaint update successfully";
					$status = true;
				} else {
					$msg    = "failed to update complaint";
					$status = false;
				}
			}
		} else {
			$msg    = 'No direct script is allowed.';
			$status = false;
		}
		$response['msg']    = $msg;
		$response['status'] = $status;
		data2json($response);
	}
	public function document_collection() {

		$response = array();
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input      = json_decode(file_get_contents('php://input'));
			$address_to = isset($input->user_id)?$input->user_id:'';
			if (empty($address_to)) {
				$response['message'] = 'user id is required.';
				$response['code']    = 201;
			} else {
				$table               = 'document_collection';
				$document_collection = $this->meetings_model->getdata($address_to, $table);

				if ($document_collection) {

					foreach ($document_collection as $key => $value) {
						$address_to = $value['address_to'];
						$address_to = $this->users_model->column('first_name,last_name')->where('id', $address_to)->find_all();
						if ($address_to) {
							$full_name = $address_to[0]->first_name.' '.$address_to[0]->last_name;

							$document_collection[$key]['address_to'] = $full_name;
						}
					}
					$response['document_collection'] = $document_collection;
				} else {
					$response['document_collection'] = "";
				}
				$response['status']     = 'success';
				$response['address_to'] = $address_to;
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		data2json($response);
	}

	public function meeting_list() {

		$response = array();
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input      = json_decode(file_get_contents('php://input'));
			$society_id = isset($input->society_id)?$input->society_id:'';
			$user_id    = isset($input->user_id)?$input->user_id:'';
			if (empty($society_id)) {
				$response['message'] = 'society id is required';
				$response['code']    = 201;
			} else {

				$this->db->select('*');
				$this->db->from('meetings');
				$this->db->where('society_id', $society_id);
				$this->db->group_by('meeting_id');
				if ($user_id) {
					$this->db->where('user_id', $user_id);
				}
				$meetings = $this->db->get()->result_array();

				if ($meetings) {
					foreach ($meetings as $key => $value) {
						$meetings[$key]['stime']    = $value['start_time'];
						$meetings[$key]['etime']    = $value['end_time'];
						$meetings[$key]['ondate']   = $value['meeting_date'];
						$meetings[$key]['location'] = $value['meeting_location'];
						unset($meetings[$key]['start_time']);
						unset($meetings[$key]['end_time']);
						unset($meetings[$key]['meeting_location']);
						unset($meetings[$key]['meeting_date']);
					}
				}
				$response['society_id'] = $society_id;
				$response['meetings']   = $meetings;
				if ($response['meetings']) {
					$response['message'] = 'Meeting List';
					$response['status']  = 'success';
				} else {
					$response['message'] = 'No record for Meeting List';
				}
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		data2json($response);
	}

	public function sign_up() {
		$response = array('status' => false, 'message' => '');
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input = json_decode(file_get_contents('php://input'));

			$role_id             = isset($input->role_id)?$input->role_id:'';
			$first_name          = isset($input->first_name)?$input->first_name:'';
			$last_name           = isset($input->last_name)?$input->last_name:'';
			$username            = isset($input->username)?$input->username:'';
			$email               = isset($input->email)?$input->email:'';
			$phone               = isset($input->phone)?$input->phone:'';
			$birth_date          = isset($input->birth_date)?$input->birth_date:'';
			$total_family_member = isset($input->total_family_member)?$input->total_family_member:'';
			$password            = isset($input->password)?$input->password:'';
			$salt                = getRandomId(8);
			$society_id          = isset($input->society_id)?$input->society_id:'';
			$house_id            = isset($input->house_id)?$input->house_id:'';
			$created_by          = isset($input->created_by)?$input->created_by:'';
			$created_date        = date("Y-m-d h:i:s");

			if (empty($society_id)) {
				$response['message'] = 'society_id is required.';
				$response['code']    = 201;
			} else if (empty($role_id)) {
				$response['message'] = 'Role Id is required.';
				$response['code']    = 201;
			} else if (empty($first_name)) {
				$response['message'] = 'First Name is required.';
				$response['code']    = 201;
			} else if (empty($last_name)) {
				$response['message'] = 'Last name is required.';
				$response['code']    = 201;
			} else if (empty($username)) {
				$response['message'] = 'Username is required.';
				$response['code']    = 201;
			} else if (empty($password)) {
				$response['message'] = 'Password is required.';
				$response['code']    = 201;
			} else if (empty($society_id)) {
				$response['message'] = 'Society Id is required.';
				$response['code']    = 201;
			} else if (empty($house_id)) {
				$response['message'] = 'House Id is required.';
				$response['code']    = 201;
			} else if (empty($email)) {
				$response['message'] = 'Email is required.';
				$response['code']    = 201;
			} else if (empty($phone)) {
				$response['message'] = 'phone number is required.';
				$response['code']    = 201;
			} else {

				$details = array(
					'first_name'          => $first_name,
					'last_name'           => $last_name,
					'username'            => $username,
					'email'               => $email,
					'phone'               => $phone,
					'birth_date'          => $birth_date,
					'total_family_member' => $total_family_member,
					'password'            => md5($salt.$password),
					'salt'                => $salt,
					'society_id'          => $society_id,
					'house_id'            => $house_id,
					'created_date'        => $created_date,
					'role_id'             => $role_id,
					'created_by'          => $created_by,
				);

				$res = $this->db->insert('users', $details);

				$response['code']    = 200;
				$response['status']  = true;
				$response['uid']     = $this->db->insert_id();
				$response['message'] = 'Registration Done !!';

				if ($res) {
					$this->db->set('allocated', 'Y');
					$this->db->where('id', $house_id);
					$this->db->update('house_master');
				} else {
					$response['message'] = 'Failed to register';
					$response['code']    = 203;
				}
			}

		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}
	public function house_type() {
		$response = array();
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input      = json_decode(file_get_contents('php://input'));
			$society_id = isset($input->society_id)?$input->society_id:'';
			if (empty($society_id)) {
				$response['message'] = 'society id is required';
				$response['code']    = 201;
			} else {
				$house_type = $this->society_house_charges_model->where('society_id', $society_id)->find_all();
				;
				$response['status']     = 'success';
				$response['house_type'] = $house_type;
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		data2json($response);

	}

	public function add_house() {
		$response = array('status' => false, 'message' => '');
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input = json_decode(file_get_contents('php://input'));

			$society_id = isset($input->society_id)?$input->society_id:'';
			$wing       = isset($input->wing)?$input->wing:'';
			$block      = isset($input->block)?$input->block:'';
			$house_type = isset($input->house_type)?$input->house_type:'';
			$details    = isset($input->details)?$input->details:'';
			$created_by = isset($input->created_by)?$input->created_by:'';

			if (empty($society_id)) {
				$response['message'] = 'society_id is required.';
				$response['code']    = 201;
			} else if (empty($wing)) {
				$response['message'] = 'Wing is required.';
				$response['code']    = 201;
			} else if (empty($block)) {
				$response['message'] = 'Block Number is required.';
				$response['code']    = 201;
			} else if (empty($house_type)) {
				$response['message'] = 'House Type is required.';
				$response['code']    = 201;
			} else if (empty($created_by)) {
				$response['message'] = 'Created By is required.';
				$response['code']    = 201;
			} else {

				$house_data = array(
					'society_id'   => $society_id,
					'wing'         => $wing,
					'block'        => $block,
					'house_type'   => $house_type,
					'details'      => $details,
					'is_deleted'   => 'N',
					'created_by'   => $created_by,
					'created_date' => date('Y-m-d H:m:i'),
					'updated_by'   => $created_by,
					'updated_date' => date('Y-m-d H:m:i')
				);

				$res = $this->db->insert('house_master', $house_data);
				if ($res) {
					$response['code']     = 200;
					$response['status']   = true;
					$response['house_id'] = $this->db->insert_id();
					$response['message']  = 'House add Successfully !!';

				} else {
					$response['message'] = 'Failed to Add House';
					$response['code']    = 203;
				}
			}

		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}
	public function notices() {
		$response = array();
		$msg      = '';
		$status   = '';
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input     = json_decode(file_get_contents('php://input'));
			$notice_id = isset($input->notice_id)?$input->notice_id:'';

			if (empty($notice_id)) {
				$msg    = "please provide notice id";
				$status = false;

			} else {
				$check_notice_id = $this->notices_model->where('notice_id', $notice_id)->find_all();

				if ($check_notice_id) {
					$notices_details = $this->notices_model->notices_seen($notice_id);

					if ($notices_details) {
						$response['notices_details'] = $notices_details;
						$msg                         = "notice data retrive successfully";
						$status                      = true;
					} else {
						$msg    = "unable to fetch notice details";
						$status = false;
					}
				} else {
					$msg    = "invalid notice id";
					$status = false;
				}
			}
		} else {

		}
		$response['message'] = $msg;
		$response['status']  = $status;

		data2json($response);
	}

	public function facility_list() {

		$response = array('status' => false, 'message' => '');
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input      = json_decode(file_get_contents('php://input'));
			$society_id = $input->society_id;
			// $society_id=$this->input->post('society_id');            Form data
			if (empty($society_id)) {
				$response['message'] = 'society_id is required.';
				$response['code']    = 201;
			} else {
				$res = $this->db->get_where('facility_master', array('society_id' => $society_id, 'is_deleted' => 'N'));
				if ($res->num_rows() == 0) {
					$response['message'] = "No Records";
				} else {
					$response['status']           = 200;
					$result                       = $res->result();
					$length                       = count($result);
					$response['facility_details'] = array();
					for ($i = 0; $i < $length; $i++) {
						$facility_details['id']               = $result[$i]->id;
						$facility_details['facility_name']    = $result[$i]->facility_name;
						$facility_details['adult_charges']    = $result[$i]->adult_charges?$result[$i]->adult_charges:0;
						$facility_details['child_charges']    = $result[$i]->child_charges?$result[$i]->child_charges:0;
						$facility_details['maintenance_cost'] = $result[$i]->maintenance_cost;
						$facility_details['created_at']       = $result[$i]->created_at;
						array_push($response['facility_details'], $facility_details);
					}
				}
			}

		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}

	public function fac_allocation_list() {
		$response = array('status' => false, 'message' => '');

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$input      = json_decode(file_get_contents('php://input'));
			$society_id = $input->society_id;

			if (empty($society_id)) {
				$response['message'] = "Society Id is required.";
				$response['code']    = 201;
			} else {
				$this->db->select('concat(u.first_name," ",u.last_name) as username,fm.facility_name,f.*');
				$this->db->from('facilty f');
				$this->db->join('facility_master fm', 'fm.id = f.facilty_id', 'left');
				$this->db->join('users u', 'u.id = f.user_id', 'left');
				$this->db->where('f.society_id', $society_id);

				$res = $this->db->get()->result();
				if ($res == 0) {
					$response['message'] = 'No record Found.';
				} else {
					$response['status']              = 200;
					$result                          = $res;
					$response['fac_allocation_list'] = $res;
				}
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}
	public function allocate_facility() {
		$response = array('status' => false, 'message' => '');

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

			$input = json_decode(file_get_contents('php://input'));

			$society_id   = isset($input->society_id)?$input->society_id:'';
			$facility_id  = isset($input->facility_id)?$input->facility_id:'';
			$user_id      = isset($input->user_id)?$input->user_id:'';
			$adult_count  = isset($input->adult_count)?$input->adult_count:'';
			$child_count  = isset($input->child_count)?$input->child_count:'';
			$total_member = isset($input->total_member)?$input->total_member:'';

			if (empty($user_id)) {
				$response['message'] = "User Id is required.";
				$response['code']    = 201;
			} else if (empty($facility_id)) {
				$response['message'] = "Facility Id is required.";
				$response['code']    = 201;
			}
			// else if ($society_id) {
			// 	$response['message'] = "Society Id is required.";
			// 	$response['code']    = 201;
			// }
			 else {
				$data = array(
					'society_id'   => $society_id,
					'facilty_id'   => $facility_id,
					'user_id'      => $user_id,
					'adult_count'  => $adult_count,
					'child_count'  => $child_count,
					'total_member' => $total_member,
				);
				// $this->db->where('society_id', $society_id);
				$allocate_user = $this->db->insert('facilty', $data);
				if ($allocate_user == 0) {
					$response['message'] = 'No record Found';
				} else {
					$response['message'] = 'Facility allocated successfully';
					$response['status']  = true;
				}
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}
	public function get_parking_slot() {
		$response = array();
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input = json_decode(file_get_contents('php://input'));

			$society_id = isset($input->society_id)?$input->society_id:'';

			if (empty($society_id)) {
				$msg    = "please provide society_id";
				$status = false;
			} else {
				$parking_slot = $this->db->query("SELECT id,slot_no FROM parking_master WHERE id NOT IN (SELECT pm_id FROM parking) and society_id=$society_id")->result();
				if ($parking_slot) {
					$response['slot_details'] = $parking_slot;
					$msg                      = "Slot details fetch successfully";
					$status                   = true;
				} else {
					$msg    = "Slot Details is empty";
					$status = false;
				}
			}
		} else {
			$msg    = 'No direct script is allowed.';
			$status = false;
		}
		$response['msg']    = $msg;
		$response['status'] = $status;
		data2json($response);
	}
	public function allocate_parking_slot() {
		$response = array();
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input = json_decode(file_get_contents('php://input'));

			$slot_id    = isset($input->slot_id)?$input->slot_id:'';
			$user_id    = isset($input->user_id)?$input->user_id:'';
			$society_id = isset($input->society_id)?$input->society_id:'';

			if (empty($slot_id) || empty($user_id) || empty($society_id)) {
				$msg    = "please provide valid input";
				$status = false;
			} else {
				$check_valid_slot = $this->db->query('SELECT id,slot_no FROM parking_master WHERE id = '.$slot_id.'')->result();

				if ($check_valid_slot) {
					$this->parking_model->user_id    = $user_id;
					$this->parking_model->society_id = $society_id;
					$this->parking_model->pm_id      = $slot_id;

					$assign_slot = $this->parking_model->save();
					if ($assign_slot) {

						$msg    = "Facility Allocate Successfully";
						$status = true;
					} else {
						$msg    = "Error occured in allocate facility";
						$status = false;
					}
				} else {
					$msg    = "please provide valid slot no";
					$status = false;
				}
			}
		} else {
			$msg    = 'No direct script is allowed.';
			$status = false;
		}
		$response['msg']    = $msg;
		$response['status'] = $status;
		data2json($response);
	}
	public function parking_list() {
		$input = json_decode(file_get_contents('php://input'));

		$user_id    = isset($input->user_id)?$input->user_id:'';
		$society_id = isset($input->society_id)?$input->society_id:'';

		if (empty($society_id)) {
			$msg    = "please provide society id";
			$status = false;
		} else {
			$this->db->select('p.id,p.user_id,u.id as user_id,u.first_name,u.last_name,p.pm_id as slot_id,pm.slot_no');
			$this->db->from('parking p');
			$this->db->where('p.society_id', $society_id);
			$this->db->join('parking_master pm', 'pm.id = p.pm_id', 'left');
			$this->db->join('users u', 'u.id=p.user_id', 'left');
			$this->db->order_by('id', 'desc');
			if ($user_id) {
				$this->db->where('p.user_id', $user_id);
			}

			$parking_list = $this->db->get()->result();

			if ($parking_list) {
				$response['parking_list'] = $parking_list;
				$msg                      = "Parking details fetch successfully";
				$status                   = true;
			} else {
				$msg    = "unable to fetch parking details";
				$status = " false";
			}
		}
		$response['msg']    = $msg;
		$response['status'] = $status;
		data2json($response);
	}
	/*public function meeting_add() {
	$response = array();
	$msg      = '';
	$status   = '';
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$input        = json_decode(file_get_contents('php://input'));
	$society_id   = isset($input->society_id)?$input->society_id:'';
	$ondate       = isset($input->ondate)?$input->ondate:'';
	$stime        = isset($input->stime)?$input->stime:'';
	$etime        = isset($input->etime)?$input->etime:'';
	$subject      = isset($input->subject)?$input->subject:'';
	$description  = isset($input->society_id)?$input->description:'';
	$location     = isset($input->society_id)?$input->location:'';
	$authority_id = isset($input->authority_id)?$input->authority_id:'';

	if (empty($society_id)) {
	$response['message'] = 'society_id is required.';
	$response['status']  = false;
	} elseif (empty($ondate)) {
	$response['message'] = 'ondate is required.';
	$response['status']  = false;
	} elseif (empty($stime)) {
	$response['message'] = 'stime is required.';
	$response['status']  = false;
	} elseif (empty($etime)) {
	$response['message'] = 'etime is required.';
	$response['status']  = false;
	} elseif (empty($subject)) {
	$response['message'] = 'subject is required.';
	$response['status']  = false;
	} elseif (empty($description)) {
	$response['message'] = 'description is required.';
	$response['status']  = false;
	} elseif (empty($location)) {
	$response['message'] = 'location is required.';
	$response['status']  = false;
	} elseif (empty($authority_id)) {
	$response['message'] = 'authority_id is required.';
	$response['status']  = false;
	} else {
	show($input, 1);
	$house_data = array(
	'society_id'       => $society_id,
	'meeting_date'     => $ondate,
	'start_time'       => $stime,
	'end_time'         => $etime,
	'subject'          => $subject,
	'description'      => $description,
	'meeting_location' => $location,
	);

	$res = $this->db->insert('meetings', $house_data);
	if ($res) {
	$response['message'] = 'meeting request send successfully.';
	$response['status']  = true;
	} else {
	$response['message'] = 'Error occured.';
	$response['status']  = false;
	}
	}
	data2json($response);
	}
	}*/
	public function meeting_add() {
		$response = array('status' => false, 'message' => '');
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input = json_decode(file_get_contents('php://input'));

			$society_id = isset($input->society_id)?$input->society_id:'';

			$ondate        = isset($input->ondate)?$input->ondate:'';
			$stime         = isset($input->stime)?$input->stime:'';
			$etime         = isset($input->etime)?$input->etime:'';
			$subject       = isset($input->subject)?$input->subject:'';
			$description   = isset($input->description)?$input->description:'';
			$location      = isset($input->location)?$input->location:'';
			$authority     = isset($input->authority)?$input->authority:'';
			$notifications = isset($input->notifications)?$input->notifications:'';
			if (empty($society_id)) {
				$response['message'] = 'Society Id is required.';
				$response['status']  = false;
			} elseif (empty($ondate)) {

				$response['message'] = 'ondate is required.';
				$response['status']  = false;

			} elseif (empty($stime)) {

				$response['message'] = 'stime is required.';
				$response['status']  = false;

			} elseif (empty($etime)) {

				$response['message'] = 'etime is required.';
				$response['status']  = false;

			} elseif (empty($subject)) {

				$response['message'] = 'subject is required.';
				$response['status']  = false;

			} elseif (empty($description)) {

				$response['message'] = 'description is required.';
				$response['status']  = false;

			} elseif (empty($location)) {

				$response['message'] = 'location is required.';
				$response['status']  = false;

			} elseif (empty($authority)) {

				$response['message'] = 'authority is required.';
				$response['status']  = false;

			} else {

				$sms_send   = false;
				$email_send = false;
				$both       = false;

				if (isset($notifications)) {
					if ($notifications->email == "true") {
						$mail_send = true;
					} else if ($notifications->sms == "true") {
						$sms_send = true;
					} elseif ($notifications->email == "true" && $notifications->sms == "true") {
						$both = true;
					}
				}
				$society_data = $this->db->where('id', $society_id)->get('society_master')->result();
				$society_name = $society_data[0]->name;
				if ($input) {
					$meeting_id   = getRandomId(5, 'numeric');
					$authority_id = explode(",", $authority);
					$res          = $this->users_model->where_in('authority_id', $authority_id)->where('society_id', $society_id)->find_all();
					foreach ($res as $key => $value) {
						$email      = $value->email;
						$phone      = $value->phone;
						$username   = $value->username;
						$first_name = $value->first_name;
						$last_name  = $value->last_name;
						$created_by = $value->created_by;
						$email_user = $this->db->where('id', $created_by)->get('users')->result();
						$from_email = $email_user[0]->email;
						$params     = array(
							'to'      => $email,
							'subject' => $subject,
							'html'    => $description,
							'from'    => $from_email,

						);
						$msg = "meeting request from: ".$society_name.", ".$subject.":Meeting Date: ".$ondate." meeting time : ".$stime." login link thank you";

						if ($both) {
							$response = sendMail($params);
							$smsres   = sendSms($phone, $msg);
						} elseif ($email_send) {
							$response = sendMail($params);
						} else if (isset($sms_send)) {
							$smsres = sendSms($phone, $msg);
						} else {
						}

						$meeting_data = array(
							'society_id'       => $society_id,
							'meeting_date'     => $ondate,
							'meeting_id'       => $meeting_id,
							'user_id'          => $value->id,
							'start_time'       => $stime,
							'end_time'         => $etime,
							'subject'          => $subject,
							'description'      => $description,
							'meeting_location' => $location,

						);

						$insert_meeting = $this->db->insert('meetings', $meeting_data);
					}
				} else {
					$response['message'] = 'Invalid Parameters passed';
				}
				if ($insert_meeting) {
					$response['message'] = 'meeting request send successfully.';
					$response['status']  = true;
					$response['code']    = 200;
				} else {
					$response['message'] = 'Error occured.';
					$response['status']  = false;
				}
			}
		} else {
			$msg    = "no direct script access allowed";
			$status = false;
		}
		data2json($response);
	}
	public function addEvent() {
		$response = array();
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input      = json_decode(file_get_contents('php://input'));
			$society_id = isset($input->society_id)?$input->society_id:'';
			$title      = isset($input->title)?$input->title:'';
			$start      = isset($input->start)?$input->start:'';
			$end        = isset($input->end)?$input->end:'';
			if (empty($society_id)) {
				$response['message'] = 'society id is required';
				$response['code']    = 201;
			} else if (empty($title)) {
				$response['message'] = 'society id is required';
				$response['code']    = 201;
			} else if (empty($start)) {
				$response['message'] = 'Start Date required';
				$response['code']    = 201;
			} else if (empty($end)) {
				$response['message'] = 'End Date is required';
				$response['code']    = 201;
			} else {
				$details = array(
					'title'      => $title,
					'start'      => $start,
					'end'        => $end,
					'society_id' => $society_id,
				);
				$res       = $this->db->insert('event_master', $details);
				$insert_id = $this->db->insert_id();
				if ($res) {
					$response['code']    = 200;
					$response['status']  = true;
					$response['eid']     = $insert_id;
					$response['message'] = "Event Add Successfully";
				}
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}

	public function allEvent() {
		$response = array();
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input      = json_decode(file_get_contents('php://input'));
			$society_id = isset($input->society_id)?$input->society_id:'';

			if (empty($society_id)) {
				$response['message'] = 'society id is required';
				$response['code']    = 201;
			} else {

				$res    = $this->db->get_where('event_master', array('society_id' => $society_id));
				$result = $res->result_array();
				if ($res->num_rows() < 1) {
					$response['code']    = 204;
					$response['message'] = "No records";
				} else {
					$response['code']          = 200;
					$response['status']        = true;
					$response['event_details'] = array();
					foreach ($result as $res) {
						array_push($response['event_details'], $res);
					}
				}
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}

	public function send_notice() {
		$response = array();
		$msg      = '';
		$status   = '';
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input          = json_decode(file_get_contents('php://input'));
			$society_id     = isset($input->society_id)?$input->society_id:'';
			$notice_title   = isset($input->notice_title)?$input->notice_title:'';
			$notice_subject = isset($input->notice_subject)?$input->notice_subject:'';
			$notice_to      = isset($input->notice_to)?$input->notice_to:'';
			$notice_from    = isset($input->notice_from)?$input->notice_from:'';
			$notice_body    = isset($input->notice_body)?$input->notice_body:'';

			if (empty($notice_title)) {
				$msg    = "please provide notice title";
				$status = false;
			} else if (empty($notice_subject)) {
				$msg    = "please provide notice subject";
				$status = false;
			} else if (empty($notice_to)) {
				$msg    = "please provide notice to";
				$status = false;
			} else if (empty($notice_from)) {
				$msg    = "please provide notice from";
				$status = false;
			} else if (empty($notice_body)) {
				$msg    = "please provide notice body";
				$status = false;
			} else {
				if ($notice_to == 'all') {
					$send_to_details = $this->users_model->column('id')->where('society_id', $society_id)->where('role_id !=', SOCIETY_SUPERUSER)->as_array()->find_all();
					$notice_to       = array();
					if ($send_to_details) {
						foreach ($send_to_details as $key => $value) {
							array_push($notice_to, $value['id']);
						}
					}
				} else {
					$notice_to = substr($notice_to, 1, -1);
					$notice_to = explode(",", $notice_to);
				}
				$notice_id = getRandomId(4, "numeric");
				foreach ($notice_to as $notice_number) {

					$this->notices_model->notice_id          = $notice_id;
					$this->notices_model->society_id         = $society_id;
					$this->notices_model->notice_from        = $notice_from;
					$this->notices_model->notice_to          = $notice_number;
					$this->notices_model->title              = $notice_title;
					$this->notices_model->subject            = $notice_subject;
					$this->notices_model->notice_description = $notice_body;
					$this->notices_model->created_by         = $notice_from;

					$send_notice = $this->notices_model->save();
				}
				$msg    = "notice send Successfully";
				$status = true;
			}
		} else {
			$msg    = "no direct script access allowed";
			$status = false;
		}
		$response['message'] = $msg;
		$response['status']  = $status;

		data2json($response);
	}
	public function view_notice() {
		$response = array();
		$msg      = '';
		$status   = '';
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input = json_decode(file_get_contents('php://input'));

			$user_id   = isset($input->user_id)?$input->user_id:'';
			$notice_id = isset($input->notice_id)?$input->notice_id:'';

			if (empty($user_id)) {
				$msg    = "please provide user id";
				$status = false;
			} else if (empty($notice_id)) {
				$msg    = "please provide notice id";
				$status = false;
			} else {
				$this->db->set('notice_view', 'Y');
				$this->db->where('notice_id', $notice_id);
				$this->db->where('notice_to', $user_id);
				$update_data = $this->db->update('notices');

				if ($update_data) {
					$msg    = "success";
					$status = false;
				} else {
					$msg    = "Error occured";
					$status = false;
				}
			}
		} else {
			$msg    = "no direct script access allowed";
			$status = false;
		}
		$response['message'] = $msg;
		$response['status']  = $status;

		data2json($response);
	}
	public function view_document() {
		$response = array();
		$msg      = '';
		$status   = '';
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input = json_decode(file_get_contents('php://input'));

			$document_id = isset($input->document_id)?$input->document_id:'';
			$user_id     = isset($input->user_id)?$input->user_id:'';

			if (empty($document_id)) {
				$msg    = "please provide document id";
				$status = false;
			} else if (empty($user_id)) {
				$msg    = "please provide user id";
				$status = false;
			} else {
				$this->db->set('document_view', 'Y');
				$this->db->where('id', $document_id);
				$this->db->where('address_to', $user_id);
				$update_data = $this->db->update('document_collection');

				if ($update_data) {
					$msg    = "success";
					$status = false;
				} else {
					$msg    = "Error occured";
					$status = false;
				}
			}
		} else {
			$msg    = "no direct script access allowed";
			$status = false;
		}
		$response['message'] = $msg;
		$response['status']  = $status;

		data2json($response);
	}
	public function get_updated_count() {
		$response = array();
		$msg      = '';
		$status   = '';
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input   = json_decode(file_get_contents('php://input'));
			$user_id = isset($input->user_id)?$input->user_id:'';

			if (empty($user_id)) {
				$msg    = "please provide user id";
				$status = false;
			} else {
				$check_userid = $this->users_model->where('id', $user_id)->find_all();

				$society_id = $check_userid[0]->society_id;
				if ($check_userid) {
					$notice_count = $this->notices_model->get_notice_id($user_id);

					$latest_document_count = $this->document_collection_model->get_document_id($user_id);

					$time = date("Y-m-d");

					$latest_meeting_count = $this->meetings_model->where('society_id', $society_id)->where('meeting_date', $time)->find_all();

					$response['latest_meeting_count']  = $latest_meeting_count;
					$response['latest_document_count'] = $latest_document_count;
					$response['notice_count']          = $notice_count;
					$msg                               = "success";
					$status                            = true;
				} else {
					$msg    = "please provide valid user id";
					$status = false;
				}
			}
		} else {
			$msg    = "no direct script access allowed";
			$status = false;
		}
		$response['message'] = $msg;
		$response['status']  = $status;

		data2json($response);
	}
	public function latestUpdatedCount() {
		$response = array();
		$msg      = '';
		$status   = '';
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input      = json_decode(file_get_contents('php://input'));
			$society_id = isset($input->society_id)?$input->society_id:'';

			if (empty($society_id)) {
				$msg    = "please provide society id";
				$status = false;
			} else {
				$complaint_count = $this->complaint_model->column('count("id") as count')->where('society_id', $society_id)->where('is_solved', 'N')->find_all();
				if ($complaint_count) {
					$complaint_count = $complaint_count[0]->count;
				} else {
					$complaint_count = 0;
				}
				$bill_payment_count = $this->bill_payment_model->column('count("id") as count')->where('society_id', $society_id)->where('is_approved', 'Pending')->find_all();
				if ($bill_payment_count) {
					$bill_payment_count = $bill_payment_count[0]->count;
				} else {
					$bill_payment_count = 0;
				}
				$status                         = true;
				$response['bill_payment_count'] = $bill_payment_count;
				$response['complaint_count']    = $complaint_count;
				$msg                            = "success";
			}
		} else {
			$msg    = "no direct script access allowed";
			$status = false;
		}
		$response['message'] = $msg;
		$response['status']  = $status;

		data2json($response);
	}
	public function add_document() {
		$response = array('status' => false, 'message' => '');
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input         = json_decode(file_get_contents('php://input'));
			$society_id    = isset($input->society_id)?$input->society_id:'';
			$coming_from   = isset($input->coming_from)?$input->coming_from:'';
			$document_name = isset($input->document_name)?$input->document_name:'';
			$address_to    = isset($input->address_to)?$input->address_to:'';
			$date          = isset($input->date)?$input->date:'';
			$handover_to   = isset($input->handover_to)?$input->handover_to:'';
			if (empty($society_id)) {
				$response['message'] = 'society_id is required.';
				$response['code']    = 201;
			} else if (empty($coming_from)) {
				$response['message'] = 'Coming From is required.';
				$response['code']    = 201;
			} else if (empty($date)) {
				$response['message'] = 'Date is required.';
				$response['code']    = 201;
			} else if (empty($handover_to)) {
				$response['message'] = 'Handover to is required.';
				$response['code']    = 201;
			} else if (empty($address_to)) {
				$response['message'] = 'Address to is required.';
				$response['code']    = 201;
			} else {
				$details = array(
					'coming_from'   => $coming_from,
					'address_to'    => $address_to,
					'handover_to'   => $handover_to,
					'document_name' => $document_name,
					'date'          => $date,
					'society_id'    => $society_id,
				);
				$res = $this->db->insert('document_collection', $details);
				$id  = $this->db->insert_id();
				if ($res) {
					$response['code']    = 200;
					$response['status']  = true;
					$response['id']      = $id;
					$response['message'] = "Document add successfully";
				} else {
					$response['code']    = 204;
					$response['message'] = "Failed To insert";
				}

			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}
	public function remaining_parking() {
		$response = array();
		$msg      = '';
		$status   = '';
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input      = json_decode(file_get_contents('php://input'));
			$society_id = isset($input->society_id)?$input->society_id:'';

			if (empty($society_id)) {
				$msg    = "please provide society id";
				$status = false;
			} else {
				$remaining_parking             = $this->db->query('SELECT id,slot_no FROM parking_master WHERE id NOT IN (SELECT pm_id FROM parking) and society_id = '.$society_id.'')->result();
				$remaining_parking             = $remaining_parking?$remaining_parking:'parking full';
				$response['remaining_parking'] = $remaining_parking;
				$msg                           = "success";
				$status                        = true;
			}
		} else {
			$msg    = "no direct script access allowed";
			$status = false;
		}
		$response['message'] = $msg;
		$response['status']  = $status;

		data2json($response);
	}

	public function allReadDocument() {
		$response = array('status' => false, 'message' => '');
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input      = json_decode(file_get_contents('php://input'));
			$society_id = isset($input->society_id)?$input->society_id:'';

			if (empty($society_id)) {
				$response['message'] = 'society_id is required.';
				$response['code']    = 201;
			} else {
				$res = $this->db->get_where('document_collection', array('society_id' => $society_id, 'document_view' => 'Y'));

				if ($res->num_rows() > 0) {

					$response['code']          = 200;
					$response['status']        = true;
					$response['document_view'] = array();
					foreach ($res->result() as $document) {
						$document_view['coming_from']   = $document->coming_from;
						$document_view['address_to']    = $document->address_to;
						$document_view['document_name'] = $document->document_name;
						$document_view['date']          = $document->date;
						$document_view['handover_to']   = $document->handover_to;
						array_push($response['document_view'], $document_view);
					}

				} else {
					$response['message'] = 'No data';
					$response['code']    = 202;
				}

			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}

	public function allUnreadDocument() {
		$response = array('status' => false, 'message' => '');
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input      = json_decode(file_get_contents('php://input'));
			$society_id = isset($input->society_id)?$input->society_id:'';

			if (empty($society_id)) {
				$response['message'] = 'society_id is required.';
				$response['code']    = 201;
			} else {
				$res = $this->db->get_where('document_collection', array('society_id' => $society_id, 'document_view' => 'N'));

				if ($res->num_rows() > 0) {

					$response['code']            = 200;
					$response['status']          = true;
					$response['document_unread'] = array();
					foreach ($res->result() as $document) {
						$document_unread['coming_from']   = $document->coming_from;
						$document_unread['address_to']    = $document->address_to;
						$document_unread['document_name'] = $document->document_name;
						$document_unread['date']          = $document->date;
						$document_unread['handover_to']   = $document->handover_to;
						array_push($response['document_unread'], $document_unread);
					}

				} else {
					$response['message'] = 'No data';
					$response['code']    = 202;
				}

			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}

	public function viewDocumentByuser() {
		$response = array('status' => false, 'message' => '');
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input = json_decode(file_get_contents('php://input'));
			$id    = isset($input->id)?$input->id:'';

			if (empty($id)) {
				$response['message'] = 'id is required.';
				$response['code']    = 201;
			} else {
				$res = $this->db->update('document_collection', array('document_view' => 'Y'), array('id' => $id));

				if ($res) {

					$response['code']    = 200;
					$response['status']  = true;
					$response['message'] = "Document Seen";

				} else {
					$response['message'] = 'No data';
					$response['code']    = 202;
				}

			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}
	public function addDocument() {
		$response = array('status' => false, 'message' => '');
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input         = json_decode(file_get_contents('php://input'));
			$society_id    = isset($input->society_id)?$input->society_id:'';
			$coming_from   = isset($input->coming_from)?$input->coming_from:'';
			$document_name = isset($input->document_name)?$input->document_name:'';
			$address_to    = isset($input->address_to)?$input->address_to:'';
			$date          = isset($input->date)?$input->date:'';
			$handover_to   = isset($input->handover_to)?$input->handover_to:'';
			if (empty($society_id)) {
				$response['message'] = 'society_id is required.';
				$response['code']    = 201;
			} else if (empty($coming_from)) {
				$response['message'] = 'Coming From is required.';
				$response['code']    = 201;
			} else if (empty($date)) {
				$response['message'] = 'Date is required.';
				$response['code']    = 201;
			} else if (empty($handover_to)) {
				$response['message'] = 'Handover to is required.';
				$response['code']    = 201;
			} else if (empty($address_to)) {
				$response['message'] = 'Address to is required.';
				$response['code']    = 201;
			} else {
				$details = array(
					'coming_from'   => $coming_from,
					'address_to'    => $address_to,
					'handover_to'   => $handover_to,
					'document_name' => $document_name,
					'date'          => $date,
					'society_id'    => $society_id,
				);
				$res = $this->db->insert('document_collection', $details);
				$id  = $this->db->insert_id();
				if ($res) {
					$response['code']    = 200;
					$response['status']  = true;
					$response['id']      = $id;
					$response['message'] = "Document add successfully";
				} else {
					$response['code']    = 204;
					$response['message'] = "Failed To insert";
				}

			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}

	public function deleteDocument() {
		$response = array('status' => false, 'message' => '');
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input = json_decode(file_get_contents('php://input'));
			$id    = isset($input->id)?$input->id:'';

			if (empty($id)) {
				$response['message'] = 'id is required.';
				$response['code']    = 201;
			} else {
				$res = $this->db->delete('document_collection', array('id' => $id));

				if ($res) {

					$response['code']    = 200;
					$response['status']  = true;
					$response['message'] = "Document Deleted Successfully";

				} else {
					$response['message'] = 'No data';
					$response['code']    = 202;
				}

			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}

	public function updateDocument() {
		$response = array('status' => false, 'message' => '');
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input         = json_decode(file_get_contents('php://input'));
			$society_id    = isset($input->society_id)?$input->society_id:'';
			$id            = isset($input->doc_id)?$input->doc_id:'';
			$coming_from   = isset($input->coming_from)?$input->coming_from:'';
			$document_name = isset($input->document_name)?$input->document_name:'';
			$address_to    = isset($input->address_to)?$input->address_to:'';
			$date          = isset($input->date)?$input->date:'';
			$handover_to   = isset($input->handover_to)?$input->handover_to:'';
			if (empty($id)) {
				$response['message'] = 'Id is required.';
				$response['code']    = 201;
			} else if (empty($society_id)) {
				$response['message'] = 'society_id is required.';
				$response['code']    = 201;
			} else if (empty($coming_from)) {
				$response['message'] = 'Coming From is required.';
				$response['code']    = 201;
			} else if (empty($date)) {
				$response['message'] = 'Date is required.';
				$response['code']    = 201;
			} else if (empty($handover_to)) {
				$response['message'] = 'Handover to is required.';
				$response['code']    = 201;
			} else if (empty($address_to)) {
				$response['message'] = 'Address to is required.';
				$response['code']    = 201;
			} else {
				$details = array(
					'coming_from'   => $coming_from,
					'address_to'    => $address_to,
					'handover_to'   => $handover_to,
					'document_name' => $document_name,
					'date'          => $date,
				);
				$res = $this->db->update('document_collection', $details, array('id' => $id));
				if ($res) {
					$response['code']    = 200;
					$response['status']  = true;
					$response['message'] = "Document Update successfully";
				} else {
					$response['code']    = 204;
					$response['message'] = "Failed To update";
				}

			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}

	public function deleteEvent() {
		$response = array('status' => false, 'message' => '');
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input = json_decode(file_get_contents('php://input'));
			$id    = isset($input->id)?$input->id:'';

			if (empty($id)) {
				$response['message'] = 'id is required.';
				$response['code']    = 201;
			} else {
				$res = $this->db->delete('event_master', array('eid' => $id));

				if ($res) {

					$response['code']    = 200;
					$response['status']  = true;
					$response['message'] = "Event Deleted Successfully";

				} else {
					$response['message'] = 'Failed to delete';
					$response['code']    = 202;
				}

			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}
	public function parking_delete() {
		$response = array();
		$msg      = '';
		$status   = '';
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input = json_decode(file_get_contents('php://input'));
			$id    = isset($input->id)?$input->id:'';
			if ($id) {
				$this->db->where('id', $id);
				$res    = $this->db->delete('parking');
				$msg    = "Deleted successfully";
				$status = true;

			} else {
				$msg = "parking not deleted";
			}

		} else {
			$msg    = "no direct script access allowed";
			$status = false;
		}
		$response['message'] = $msg;
		$response['status']  = $status;

		data2json($response);
	}
	public function parking_edit() {
		$response = array();
		$msg      = '';
		$status   = '';
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input = json_decode(file_get_contents('php://input'));
			if ($input) {
				$pid       = isset($input->p_id)?$input->p_id:'';
				$uid       = isset($input->user_id)?$input->user_id:'';
				$slot_name = isset($input->slot_id)?$input->slot_id:'';
				$pdata     = array(
					'pm_id' => $slot_name,
				);
				$this->db->where('id', $pid);
				$res = $this->db->update('parking', $pdata);
				if ($res) {
					$msg    = "Updated successfully";
					$status = true;
				} else {
					$msg    = "Error occured";
					$status = false;
				}

			} else {
				$msg    = "empty input fields";
				$status = false;
			}

		} else {
			$msg    = "no direct script access allowed";
			$status = false;
		}
		$response['message'] = $msg;
		$response['status']  = $status;

		data2json($response);
	}

	public function document_received() {
		$response = array();
		$msg      = '';
		$status   = '';
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input       = json_decode(file_get_contents('php://input'));
			$document_id = isset($input->document_id)?$input->document_id:'';

			if (empty($document_id)) {
				$msg    = "please provide document_id";
				$status = false;
			} else {
				$this->db->set('document_view', 'Y');
				$this->db->where('id', $document_id);
				$this->db->update('document_collection');

				$msg    = "document received successfully";
				$status = true;
			}
		} else {
			$msg    = "no direct script access allowed";
			$status = false;
		}
		$response['message'] = $msg;
		$response['status']  = $status;

		data2json($response);
	}

	//
	public function edit_facility() {
		$response = array('status' => false, 'message' => '');

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$input = json_decode(file_get_contents('php://input'));

			$society_id   = $input->society_id;
			$facility_id  = $input->facility_id;
			$user_id      = $input->user_id;
			$adult_count  = $input->adult_count;
			$child_count  = $input->child_count;
			$total_member = $input->total_member;

			if (empty($user_id)) {
				$response['message'] = "User Id is required.";
				$response['code']    = 201;
			} else if (empty($facility_id)) {
				$response['message'] = "Facility Id is required.";
				$response['code']    = 201;
			} else if (empty($adult_count)) {
				$response['message'] = "Adult Count is required.";
				$response['code']    = 201;
			} else if (empty($child_count)) {
				$response['message'] = "Child Count is required.";
				$response['code']    = 201;
			} else if (empty($total_member)) {
				$response['message'] = "Total Member is required.";
				$response['code']    = 201;
			} else {
				$update_data = array(
					'facilty_id'   => $facility_id,
					'user_id'      => $user_id,
					'adult_count'  => $adult_count,
					'child_count'  => $child_count,
					'total_member' => $total_member,
				);
				$update_facility = $this->db->update('facilty', $update_data, array('id' => $society_id));

				if ($update_data) {
					$response['message'] = "Facility Update Successfully.";
					$response['code']    = 200;
					$response['status']  = true;
				} else {
					$response['message'] = "Failed to Update Facility.";
					$response['code']    = 201;
				}
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}

	public function delete_facility() {
		$response = array('status' => false, 'message' => '');

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$input = json_decode(file_get_contents('php://input'));

			$society_id = $input->society_id;

			if (empty($society_id)) {
				$response['message'] = 'Society Id is required';
				$response['code']    = 201;
			} else {
				$this->db->where('id', $society_id);
				$delete_facility = $this->db->delete('facilty');

				if ($delete_facility) {
					$response['message'] = 'Facility Deleted Successfully';
					$response['code']    = 200;
					$response['status']  = true;
				} else {
					$response['message'] = 'Facility Not Deleted';
					$response['code']    = 201;
				}
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}

	public function edit_meeting() {
		$response = array('status' => false, 'message' => '');

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$input = json_decode(file_get_contents('php://input'));

			$id          = isset($input->id)?$input->id:'';
			$society_id  = isset($input->society_id)?$input->society_id:'';
			$ondate      = isset($input->ondate)?$input->ondate:'';
			$stime       = isset($input->stime)?$input->stime:'';
			$etime       = isset($input->etime)?$input->etime:'';
			$subject     = isset($input->subject)?$input->subject:'';
			$description = isset($input->description)?$input->description:'';
			$location    = isset($input->location)?$input->location:'';

			if (empty($society_id)) {
				$response['message'] = "Society Id is required.";
				$response['code']    = 201;
			} else if (empty($id)) {
				$response['message'] = "Id is required.";
				$response['code']    = 201;
			} else if (empty($ondate)) {
				$response['message'] = "Meeting Date is required.";
				$response['code']    = 201;
			} else if (empty($stime)) {
				$response['message'] = "Start Time is required.";
				$response['code']    = 201;
			} else if (empty($etime)) {
				$response['message'] = "End Time is required.";
				$response['code']    = 201;
			} else if (empty($subject)) {
				$response['message'] = "Subject is required.";
				$response['code']    = 201;
			} else if (empty($description)) {
				$response['message'] = "Description is required.";
				$response['code']    = 201;
			} else if (empty($location)) {
				$response['message'] = "Meeting Location is required.";
				$response['code']    = 201;
			} else {
				$update_meeting = array(
					'id'               => $id,
					'society_id'       => $society_id,
					'meeting_date'     => $ondate,
					'start_time'       => $stime,
					'end_time'         => $etime,
					'subject'          => $subject,
					'description'      => $description,
					'meeting_location' => $location,
				);

				$update_meeting_data = $this->db->update('meetings', $update_meeting, array('id' => $id));

				if ($update_meeting_data) {
					$response['message'] = "Meeting Update Successfully.";
					$response['code']    = 200;
					$response['status']  = true;
				} else {
					$response['message'] = "Failed to Update Meeting.";
					$response['code']    = 201;
				}
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}

	public function delete_meeting() {
		$response = array('status' => false, 'message' => '');

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$input = json_decode(file_get_contents('php://input'));

			$id = isset($input->id)?$input->id:'';

			if (empty($id)) {
				$response['message'] = 'Id is required';
				$response['code']    = 201;
			} else {
				$data = array(
					'is_deleted' => 'N',
				);

				$delete_meeting = $this->db->update('meetings', $data, array('id' => $id));

				if ($delete_meeting) {
					$response['message'] = 'Meeting Deleted Successfully';
					$response['code']    = 200;
					$response['status']  = true;
				} else {
					$response['message'] = 'Meeting Not Deleted';
					$response['code']    = 201;
				}
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}

	public function edit_user() {
		$response = array('status' => false, 'message' => '');

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

			$input = json_decode(file_get_contents('php://input'));

			$first_name          = isset($input->first_name)?$input->first_name:'';
			$last_name           = isset($input->last_name)?$input->last_name:'';
			$role_id             = isset($input->role_id)?$input->role_id:'';
			$username            = isset($input->username)?$input->username:'';
			$house_id            = isset($input->house_id)?$input->house_id:'';
			$email               = isset($input->email)?$input->email:'';
			$phone               = isset($input->phone)?$input->phone:'';
			$birth_date          = isset($input->birth_date)?$input->birth_date:'';
			$total_family_member = isset($input->total_family_member)?$input->total_family_member:'';
			$status              = isset($input->status)?$input->status:'';

			if (empty($first_name)) {
				$response['message'] = 'first name is required';
				$response['code']    = 201;
			} else if (empty($last_name)) {
				$response['message'] = 'last name is required';
				$response['code']    = 201;
			} else if (empty($role_id)) {
				$response['message'] = 'role id is required';
				$response['code']    = 201;
			} else if (empty($username)) {
				$response['message'] = 'user id is required';
				$response['code']    = 201;
			} else if (empty($house_id)) {
				$response['message'] = 'house id is required';
				$response['code']    = 201;
			} else if (empty($email)) {
				$response['message'] = 'email is required';
				$response['code']    = 201;
			} else if (empty($phone)) {
				$response['message'] = 'phone is required';
				$response['code']    = 201;
			} else if (empty($birth_date)) {
				$response['message'] = 'birth date is required';
				$response['code']    = 201;
			} else if (empty($total_family_member)) {
				$response['message'] = 'total family member is required';
				$response['code']    = 201;
			} else if ($status) {
				$response['message'] = 'account status is required';
				$response['code']    = 201;
			} else {

				$update_user = array(
					'first_name'          => $first_name,
					'last_name'           => $last_name,
					'role_id'             => $role_id,
					'username'            => $username,
					'house_id'            => $house_id,
					'email'               => $email,
					'phone'               => $phone,
					'birth_date'          => $birth_date,
					'total_family_member' => $total_family_member,
					'status'              => $status,
				);
				$update_user_data = $this->db->update('users', $update_user, array('house_id' => $house_id));

				if ($update_user_data) {
					$response['message'] = 'User update successfully';
					$response['code']    = 200;
					$response['status']  = true;
				} else {
					$response['message'] = 'User not update';
					$response['code']    = 201;
				}
			}
		} else {
			$response['message'] = 'No direct script is allowed';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}

	public function delete_user() {
		$response = array('status' => false, 'message' => '');

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$input = json_decode(file_get_contents('php://input'));

			$id = isset($input->id)?$input->id:'';

			if (empty($id)) {
				$response['message'] = 'Id is required';
				$response['code']    = 201;
			} else {
				$data = array(
					'is_deleted' => 'Y',
					'status'     => 'N',
				);

				$delete_meeting = $this->db->update('users', $data, array('id' => $id));
				$delete_house   = $this->house_master_model->deAllocat_house($id);
				if ($delete_meeting) {
					$response['message'] = 'User Deleted Successfully';
					$response['code']    = 200;
					$response['status']  = true;
				} else {
					$response['message'] = 'User Not Deleted';
					$response['code']    = 201;
				}
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}

	public function delete_house() {

		$response = array('status' => false, 'message' => '');

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$input = json_decode(file_get_contents('php://input'));

			$id = isset($input->id)?$input->id:'';

			if (empty($id)) {
				$response['message'] = 'Id is required';
				$response['code']    = 201;
			} else {
				$data = array(
					'is_deleted' => 'N',
				);

				$delete_meeting = $this->db->update('house_master', $data, array('id' => $id));

				if ($delete_meeting) {
					$response['message'] = 'House Deleted Successfully';
					$response['code']    = 200;
					$response['status']  = true;
				} else {
					$response['message'] = 'House Not Deleted';
					$response['code']    = 201;
				}
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}

	public function user_outstanding_list() {
		$response = array('status' => false, 'message' => '');

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$input = json_decode(file_get_contents('php://input'));

			$society_id = isset($input->society_id)?$input->society_id:'';

			if (empty($society_id)) {
				$response['message'] = 'Society Id is required';
				$response['code']    = 201;
			} else {
				$this->db->select('u.id,concat(u.first_name," ",u.last_name) as full_name,w.current_outstanding,w.wallet_balance');
				$this->db->from('wallet w');
				$this->db->where('w.current_outstanding >', 0);
				$this->db->where('u.id!=', null);
				$this->db->join('users u', 'u.id=w.user_id', 'left');
				$outstanding_report             = $this->db->get()->result();
				$response['outstanding_report'] = $outstanding_report;

				if (!empty($outstanding_report)) {
					$response['message'] = 'User Outstanding List successfully';
					$response['code']    = 200;
					$response['status']  = true;
				} else {
					$response['message'] = 'No Record Of User Outstanding List';
					$response['code']    = 201;
					$response['status']  = false;
				}
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}

	public function user_exist() {
		$response = array('status' => false, 'message' => '');

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$input = json_decode(file_get_contents('php://input'));

			$username = isset($input->username)?$input->username:'';

			if (empty($username)) {
				$response['message'] = 'User Name is required';
				$response['code']    = 201;
			} else {
				$user_name = $this->db->select('username')->from('users')->get()->result();
				if ($username == $user_name) {
					$response['message'] = 'Username Already Exists';
					$response['code']    = 201;
				} else {
					$response['message'] = 'This Username not match with user exists';
					$response['status']  = true;
					$response['code']    = 200;
				}
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}

	public function profile_pic() {
		$response = array('status' => false, 'message' => '');

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$input   = json_decode(file_get_contents('php://input'));
			$user_id = isset($input->user_id)?$input->user_id:'';

			show($_FILES['file'], 1);

			if (empty($user_id)) {
				$response['message'] = 'User Id is required';
				$response['code']    = 201;
			} else if (!empty($_FILES['profile_pic']['tmp_name'])) {
				$config['upload_path']   = '.upload/profileImages';
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$new_name                = $user_id.'_'.mt_rand().'_'.$_FILES["profile_pic"]['name'];
				$config['file_name']     = trim($new_name);
				$config['max_size']      = 2000000;
				$this->load->library('upload', $config);

				if ($this->upload->do_upload('profile_pic')) {
					$result     = $this->db->get_where('users', array('id' => $user_id));
					$updateData = array('id'                               => $user_id);
					if ($result->num_rows() === 1) {
						$this->db->update('users', $updateData, array('id' => $user_id));
					} else {
						$this->db->insert('users', $updateData);
					}
					$response['message'] = 'Profile pic updated.';
					$response['code']    = 200;
					$response['status']  = true;
				} else {
					$response['message'] = 'No Profile pic uploaded.';
					$response['code']    = 203;
				}
			} else {
				$response['message'] = 'Profile pic uploaded.';
				$response['code']    = 203;
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}
	//Do not delete this
	public function unsubscribers() {
		header('Content-Type: text/html; charset=utf-8');

		$response = array();
		$msg      = '';
		$status   = '';
		if (!isset($_GET['email'])) {

			$msg    = "Please Provoid Valid Parameter";
			$status = false;
		} else if (empty($_GET['email'])) {
			$msg    = "Please Provoid an Email";
			$status = false;
		} else {
			$email  = $_GET['email'];
			$old_dt = $this->db->get_where('unsubscribers', array('email' => $email));

			if ($old_dt->num_rows() > 0) {

				$reason_id = isset($_GET['reason'])?$_GET['reason']:'';
				if ($reason_id) {
					$this->db->set('subscribe', 'N');
					$this->db->set('reason_id', $reason_id);
					$this->db->where('email', $email);
					$res = $this->db->update('unsubscribers');
					if ($res) {
						$response['dt'] = <<<HTML
<html>
		    <head>
		    <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">

		      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
              <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

		    </head>
		    <body><center><div class='col-md-12'><div class ='col-md-2'></div><div class='col-md-8'><br><br><br><i class='glyphicon glyphicon-ok
' style='font-size:25px;
color:green'></i>&nbsp;<b>Thank You ..You have been successfully unsubscribed, so you will not receive further e-mail messages from our organization</b></div></div></center>
		    </body>
		    </html>
HTML

						;

						$status = true;
					}
				} else {
					$replStr = "<form action='https://hamarisociety.co.in/dashboard/rest/api/unsubscribers?' id='form' method='get' name='form'>";
					$email   = (string) $email;
					$this->db->select('*');
					$this->db->from('reason');
					$result = $this->db->get()->result();

					$option = "<option value=''>select Reason</option>";
					if ($result) {
						foreach ($result as $key => $value) {
							$option .= "<option value='".$value->id."'>".$value->reason."</option>";
						}
					}
					$replStr .= "<input type='hidden' name='email' value='".htmlspecialchars($email)."'></input>";
					$replStr .= "<select name='reason' class='form-control' required>".$option."</select>"."</br><input value='Unsubscribe' type='submit' class='btn btn-danger' name='unscubscribe'></form>";
					echo <<<HTML
		    <html>
		    <head>
		    <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">

		      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
              <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

		    </head>
		    <body><center><div class='col-md-12'><div class='col-md-4'></div><div class='col-md-4'><br><p><h3>Unsubscribed Form</h3><hr style='border: 1px sold;'><h4>Hello : {$email} <br><br>You have been unsubcribed from the email list.</p></h4><br>{$replStr}</div><div class='col-md-4'></div></div></center>
		    </body>
		    </html>
HTML

					;

				}

			} else {
				$dd  = array('email' => $email, 'subscribe' => 'Y', 'created_at' => date('Y-m-d H:i:s'));
				$res = $this->db->insert('unsubscribers', $dd);

				if ($res) {
					$response['dt'] = $response['dt'] = <<<HTML
<html>
		    <head>
		    <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">

		      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
              <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

		    </head>
		    <body><center><div class='col-md-12'><div class ='col-md-2'></div><div class='col-md-8'><br><br><br><i class='glyphicon glyphicon-ok
' style='font-size:25px;
color:green'></i>&nbsp;<b>Thank You ..You have been successfully subscribed to our organization</b></div></div></center>
		    </body>
		    </html>
HTML

					;

					$msg    = "success";
					$status = true;
				}
			}
		}
		$response['message'] = $msg;
		$response['status']  = $status;
		if ($status == true) {

			echo ($response['dt']);
		} else {
			echo ($response['message']);
		}
	}

}
?>
